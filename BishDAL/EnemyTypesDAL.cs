﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BishDAL
{
    /// <summary>
    /// Provides access to the enemy types table.
    /// </summary>
    public static class EnemyTypesDAL
    {
        internal const string TBL_NAME = "EnemyTypesTbl";

        /// <summary>
        /// Creates a new enemy type with the given properties
        /// </summary>
        public static int AddEnemyType(int lvl, int health, int atk, int def, string name, char displayChar)
        {
            return OleDbHelper.InsertAndGetId(TBL_NAME, "[level], startingHealth, attack, defense, enemyName, displayCharacter", lvl, health, atk, def, name, displayChar.ToString());
        }

        /// <summary>
        /// Gets the full details of the enemy-type with the given ID.
        /// </summary>
        public static DataRow GetEnemyType(int id)
        {
            return OleDbHelper.GetDataRow("SELECT * FROM " + TBL_NAME + " WHERE enemyTypeId=" + id);
        }

        /// <summary>
        /// Returns all enemy types whose levels are closest to the specified level.
        /// </summary>
        public static DataTable GetAllEnemies(int level)
        {
            if (level < PlayersDAL.STARTING_LEVEL)
                throw new ArgumentOutOfRangeException("level", level, "Level cannot be less than starting level");

            return OleDbHelper.GetDataTable("SELECT * FROM {0} WHERE Abs([level] - {1}) = "
                + "(SELECT Min(Abs([level] - {1})) FROM {0})",
                TBL_NAME, level);
        }

        /// <summary>
        /// Returns a <see cref="DataTable"/> containing the details of all the enemy-types that exist.
        /// </summary>
        public static DataTable GetAllEnemies()
        {
            return OleDbHelper.GetDataTable("SELECT * FROM " + TBL_NAME);
        }

        /// <summary>
        /// Updates the properties of an enemy-type in the database.
        /// </summary>
        public static void Update(int id, int lvl, int health, int atk, int def, string name, char displayChar)
        {
            OleDbHelper.Update(TBL_NAME, "enemyTypeId=" + id, "[level], startingHealth, attack, defense, enemyName, displayCharacter", lvl, health, atk, def, name, displayChar.ToString());
        }

        /// <summary>
        /// Deletes the enemy-type with the given ID.
        /// </summary>
        public static void DeleteEnemyType(int id)
        {
            OleDbHelper.Delete(TBL_NAME, "enemyTypeId", id);
        }
    }
}
