﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BishDAL
{
    /// <summary>
    /// Provides methods for accessing the rooms and doors tables.
    /// </summary>
    public static class RoomsDAL
    {
        internal const string TBL_NAME = "RoomsTbl";
        internal const string DOORS_TBL_NAME = "DoorsTbl";

        /// <summary>
        /// Creates a new room, and returns its id
        /// </summary>
        /// <param name="width">The x-size of the room</param>
        /// <param name="length">The y-size of the room</param>
        /// <param name="difficulty">The average level of monsters to be spawned in the room</param>
        public static int CreateRoom(int width, int length, int difficulty)
        {
            return OleDbHelper.InsertAndGetId(TBL_NAME, "width, length, difficulty", width, length, difficulty);
        }

        /// <summary>
        /// Returns a <see cref="DataRow"/> containing the details for the room with the given id.
        /// </summary>
        public static DataRow GetRoom(int roomId)
        {
            return OleDbHelper.GetDataRow("SELECT * FROM {0} WHERE roomId={1}", TBL_NAME, roomId);
        }

        /// <summary>
        /// Returns a single room with difficulty 1, or null if none exists
        /// </summary>
        public static DataRow GetStartingRoom()
        {
            return OleDbHelper.GetDataRow("SELECT TOP 1 * FROM {0} WHERE [difficulty]=1", TBL_NAME);
        }

        /// <summary>
        /// Creates a new door, and returns its id
        /// </summary>
        /// <param name="firstRoomId">The room initially connected to the door</param>
        /// <param name="xPos">The x-position of the door in <paramref name="firstRoomId"/></param>
        /// <param name="yPos">The y-position of the door in <paramref name="firstRoomId"/></param>
        /// <returns>The new doors's id</returns>
        public static int CreateDoor(int firstRoomId, int xPos, int yPos)
        {
            return OleDbHelper.InsertAndGetId(DOORS_TBL_NAME, "firstRoomId, firstXPos, firstYPos", firstRoomId, xPos, yPos);
        }

        /// <summary>
        /// Connect the specified room to the specified door from the specified side
        /// </summary>
        /// <param name="doorId">The door to connect</param>
        /// <param name="roomId">The room to connect</param>
        /// <param name="xPos">The x-position of the door in the connected room</param>
        /// <param name="yPos">The y-position of the door in the connected room</param>
        /// <param name="doorDir">The direction of connection, 0 if connected as the first room and 1 if second</param>
        public static void ConnectDoor(int doorId, int roomId, int xPos, int yPos, int doorDir)
        {
            if (doorDir == 0)
                OleDbHelper.Update(DOORS_TBL_NAME, "doorId=" + doorId, "firstRoomId, firstXPos, firstYPos", roomId, xPos, yPos);
            else
                OleDbHelper.Update(DOORS_TBL_NAME, "doorId=" + doorId, "secondRoomId, secondXPos, secondYPos", roomId, xPos, yPos);
        }

        /// <summary>
        /// Deletes the specified room and any enemies in it, and disconnect any doors to it.
        /// </summary>
        public static void DeleteRoom(int roomId)
        {
            //disconnect the doors first
            OleDbHelper.Update(DOORS_TBL_NAME, "firstRoomId=" + roomId, "firstRoomId", new object[] { null });
            OleDbHelper.Update(DOORS_TBL_NAME, "secondRoomId=" + roomId, "secondRoomId", new object[] { null });
            //delete all the enemies and items in the room
            PhysicalEnemiesDAL.DeleteEnemiesInRoom(roomId);
            ItemsDAL.DeleteItemsInRoom(roomId);
            //delete the room
            OleDbHelper.Delete(TBL_NAME, "roomId", roomId);
        }

        /// <summary>
        /// Deletes all the doors that are not connected to any room
        /// </summary>
        public static void RemoveDisconnectedDoors()
        {
            OleDbHelper.Delete(DOORS_TBL_NAME, "firstRoomId, secondRoomId", null, null);
        }

        /// <summary>
        /// Returns a DataTable with a single column containing the IDs of all rooms that have no players
        /// </summary>
        public static DataTable GetEmptyRoomsIds()
        {
            return OleDbHelper.GetDataTable(
                "SELECT RoomsTbl.roomId FROM RoomsTbl "
                + "LEFT JOIN PlayersTbl ON RoomsTbl.roomId = PlayersTbl.roomId "
                + "GROUP BY RoomsTbl.roomId "
                + "HAVING Count(PlayersTbl.roomId) = 0;"
            );
        }

        /// <summary>
        /// Returns a door's details by its room and position, or null if a door doesn't exist there
        /// </summary>
        public static DataRow GetDoorByPosition(int roomId, int xPos, int yPos)
        {
            return OleDbHelper.GetDataRow(
                "SELECT * FROM {0} "
                + "WHERE (firstRoomId={1} AND firstXPos={2} AND firstYPos={3}) "
                + "OR (secondRoomId={1} AND secondXPos={2} AND secondYPos={3})",
                DOORS_TBL_NAME, roomId, xPos, yPos);
        }

        /// <summary>
        /// Returns the complete list of doors, with their details, in a specified room.
        /// </summary>
        public static DataTable GetDoorsInRoom(int roomId)
        {
            return OleDbHelper.GetDataTable(
                "SELECT * FROM {0} "
                + "WHERE firstRoomId={1} OR secondRoomId={1}",
                DOORS_TBL_NAME, roomId);
        }

        /// <summary>
        /// Returns whether or not a room with the given id exists.
        /// </summary>
        public static bool DoesRoomExist(int roomId)
        {
            return OleDbHelper.ExecuteScalar("SELECT roomId FROM {0} WHERE roomId={1}", TBL_NAME, roomId) != null;
        }
    }
}
