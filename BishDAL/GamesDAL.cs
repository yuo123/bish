﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;

namespace BishDAL
{
    /// <summary>
    /// Provides access to the games table.
    /// </summary>
    public static class GamesDAL
    {
        internal const string TBL_NAME = "GamesTbl";
        internal const string ENEMIES_TBL = "GamesEnemiesTbl";

        /// <summary>
        /// Creates a new game for the given user.
        /// </summary>
        /// <param name="playerUsername">The username for the user who started the new game</param>
        public static void NewGame(string playerUsername)
        {
            OleDbHelper.ExecuteNonQuery(
                        "INSERT INTO {0} (playerUsername, levelStarted, timeStarted) "
                        + "SELECT '{1}', [level], NOW() FROM {2} "
                        + "WHERE {2}.username='{1}'",
                        TBL_NAME, playerUsername, PlayersDAL.TBL_NAME);
        }

        /// <summary>
        /// Adds the specified amount of Experience Points to the gained XP count of a game.
        /// </summary>
        public static void AddXp(int gameId, int amount)
        {
            OleDbHelper.ChangeNumeric(TBL_NAME, "xpGained", amount, "gameId=" + gameId);
        }

        /// <summary>
        /// Returns a <see cref="DataRow"/> containing the details of the game with the given id.
        /// </summary>
        public static DataRow GetGameDetails(int gameId)
        {
            return OleDbHelper.GetDataRow("SELECT * FROM {0} WHERE gameId={1}", TBL_NAME, gameId);
        }

        /// <summary>
        /// Sets the level-finished and time-finished properties of a game based on the current 
        /// properties of its player.
        /// </summary>
        /// <param name="gameId"></param>
        public static void FinishGame(int gameId)
        {
            OleDbHelper.ExecuteNonQuery(
                "UPDATE {0} "
                + "INNER JOIN {1} ON {1}.username={0}.playerUsername "
                + "SET {0}.levelFinished={1}.[level] , {0}.timeFinished=NOW() "
                + "WHERE {0}.gameId={2}",
                TBL_NAME, PlayersDAL.TBL_NAME, gameId);
        }

        /// <summary>
        /// Returns a DataRow representing the unfinished game of the given player, or null if there is none
        /// </summary>
        public static DataRow GetUnfinishedGame(string username)
        {
            return OleDbHelper.GetDataRow(string.Format("SELECT * FROM {0} WHERE {1}", TBL_NAME, OleDbHelper.MakeAndClause("playerUsername, timeFinished", username, null)));
        }

        /// <summary>
        /// Returns a DataTable containing all games of all users, including a kill count for each
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllGames()
        {
            return GetAllGames(null);
        }

        /// <summary>
        /// Returns a DataTable containing all the games of the given user, including a kill count for each
        /// </summary>
        /// <param name="username">The username of the user to retrieve games for, or null for all</param>
        public static DataTable GetAllGames(string username)
        {
            string sql = "SELECT {0}.gameId, playerUsername, levelStarted, levelFinished, xpGained, timeStarted, timeFinished, Sum({1}.enemyCount) AS enemiesKilled "
                + "FROM {0} LEFT JOIN {1} ON {0}.gameId = {1}.gameId ";
            if (username != null)
                sql += " WHERE playerUsername='{2}' ";
            sql += "GROUP BY {0}.gameId, playerUsername, levelStarted, levelFinished, xpGained, timeStarted, timeFinished";

            return OleDbHelper.GetDataTable(sql, TBL_NAME, ENEMIES_TBL, username);
        }

        /// <summary>
        /// Registers that an enemy of the given type was killed in a game.
        /// If an enemy of that type was already killed in the game, its count will be incremented;
        /// otherwise, it will be added to the killed enemies list.
        /// </summary>
        public static void AddKilledEnemy(int gameId, int enemyTypeId)
        {
            string where = OleDbHelper.MakeAndClause("gameId, enemyTypeId", gameId, enemyTypeId);
            //if the game-enemy combination doesn't exist yet, create it (default is enemyCount=1). else, increment enemyCount
            if (OleDbHelper.GetDataRow("SELECT gameId FROM " + ENEMIES_TBL + " WHERE " + where) == null)
            {
                try
                {
                    OleDbHelper.Insert(ENEMIES_TBL, "enemyTypeId, gameId", enemyTypeId, gameId);
                }
                catch (OleDbException ex)
                {
                    throw new ArgumentException("Non-existent enemyTypeId or gameId", ex);
                }
            }
            else
            {
                OleDbHelper.ChangeNumeric(ENEMIES_TBL, "enemyCount", 1, where);
            }
        }

        /// <summary>
        /// Gets a DataTable containing all the details about all the enemies killed in the specified game
        /// </summary>
        public static DataTable GetEnemiesKilled(int gameId)
        {
            return OleDbHelper.GetDataTable("SELECT {0}.enemyTypeId, enemyName, [level], attack, defense, startingHealth, displayCharacter, enemyCount FROM {0} "
                + "INNER JOIN {1} ON {0}.enemyTypeId = {1}.enemyTypeId "
                + "WHERE {0}.gameId={2}",
                ENEMIES_TBL, EnemyTypesDAL.TBL_NAME, gameId);
        }
    }
}
