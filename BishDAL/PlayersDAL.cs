﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BishDAL
{
    /// <summary>
    /// Provides access to the players table
    /// </summary>
    public static class PlayersDAL
    {
        internal const string TBL_NAME = "PlayersTbl";

        internal const int STARTING_LEVEL = 1;
        internal const int STARTING_HEALTH = 5;
        internal const int STARTING_ATTACK = 4;
        internal const int STARTING_DEFENSE = 1;
        internal const int STARTING_XP = 0;

        /// <summary>
        /// Creates a new player with the given parameters.
        /// </summary>
        /// <param name="username">The player's username</param>
        /// <param name="password">The player's password</param>
        /// <param name="email">The player's email</param>
        /// <param name="country">The full name of the player's country of origin</param>
        /// <param name="DOB">The player's date of birth</param>
        public static void CreatePlayer(string username, string password, string email, string country, DateTime DOB)
        {
            try
            {
                OleDbHelper.Insert(TBL_NAME, "username, userPass, email, country, DOB", username, password, email, country, DOB);
            }
            catch (OleDbException ex)
            {
                if (ex.Errors[0].SQLState == "3022")
                    throw new DuplicateUsernameException("username", ex);

                throw;
            }
        }

        /// <summary>
        /// Returns a <see cref="DataRow"/> containing a player's physical properties 
        /// (username, level, room id, x-position, y-position, health, attack, defense and XP).
        /// </summary>
        public static DataRow GetPlayerPhysicalData(string username)
        {
            return OleDbHelper.GetDataRow(
                "SELECT username, [level], {0}.roomId, xPos, yPos, health, attack, defense, xp, "
                + "width AS roomWidth, length AS roomLength "
                + "FROM {0} LEFT JOIN {1} "
                + "ON {0}.roomId = {1}.roomId "
                + "WHERE username='{2}'",
                TBL_NAME, RoomsDAL.TBL_NAME, username);
        }

        internal static DataTable GetAllPlayers()
        {
            return OleDbHelper.GetDataTable("SELECT username, [level], roomId, xPos, yPos, health, attack, defense, xp, isBanned, email, country, DOB FROM {0}", TBL_NAME);
        }

        /// <summary>
        /// Returns a <see cref="DataRow"/> containing all the details about a player excluding the password.
        /// </summary>
        public static DataRow GetPlayerDetails(string username)
        {
            return OleDbHelper.GetDataRow("SELECT username, [level], roomId, xPos, yPos, health, attack, defense, xp, isBanned, email, country, DOB FROM {0} WHERE username='{1}'", TBL_NAME, username);
        }

        /// <summary>
        /// Returns the details of all players in the room with the given id. 
        /// Each row is in the same format as the one returned by <see cref="GetPlayerDetails"/>
        /// </summary>
        public static DataTable GetPlayersInRoom(int roomId)
        {
            return OleDbHelper.GetDataTable("SELECT username, [level], roomId, xPos, yPos, health, attack, defense, xp, isBanned, email, country, DOB FROM {0} WHERE roomId={1}", TBL_NAME, roomId);
        }

        /// <summary>
        /// Removes the player from the room he is in, thereby quitting his game
        /// </summary>
        public static void RemovePlayerFromRoom(string username)
        {
            OleDbHelper.Update(TBL_NAME, "username='" + username + "'", "roomId", (object)null);
        }

        /// <summary>
        /// Initializes a new player's level, health, attack, defense and XP to the defaults.
        /// </summary>
        public static void SetStartingStats(string username)
        {
            OleDbHelper.Update(
                TBL_NAME,
                "username='" + username + "'",
                "[level], health, attack, defense, xp",
                STARTING_LEVEL, STARTING_HEALTH, STARTING_ATTACK, STARTING_DEFENSE, STARTING_XP
            );
        }

        /// <summary>
        /// Resets a player's health to the starting health.
        /// </summary>
        /// <param name="username"></param>
        public static void ResetHealth(string username)
        {
            OleDbHelper.Update(
                TBL_NAME,
                "username=" + OleDbHelper.FormatForDB(username),
                "health",
                STARTING_HEALTH);
        }

        /// <summary>
        /// Changes the position of a player.
        /// </summary>
        /// <param name="username">The username of the player to change the position of</param>
        /// <param name="x">The new X-position</param>
        /// <param name="y">The new Y-position</param>
        public static void MovePlayer(string username, int x, int y)
        {
            OleDbHelper.Update(TBL_NAME, "username='" + username + "'", "xPos, yPos", x, y);
        }

        /// <summary>
        /// Moves a player to a new room
        /// </summary>
        /// <param name="username">The username of the player to change the position of</param>
        /// <param name="roomId">The id of the new room</param>
        /// <param name="x">The X-position in the new room</param>
        /// <param name="y">The Y-position in the new room</param>
        public static void ChangePlayerRoom(string username, int roomId, int x, int y)
        {
            try
            {
                OleDbHelper.Update(TBL_NAME, "username='" + username + "'", "roomId, xPos, yPos", roomId, x, y);
            }
            catch (OleDbException ex)
            {
                //3201 = You cannot add or change a record because a related record is required in table <name>.
                if (ex.Errors[0].SQLState == "3201")
                    throw new NonexistentRoomException("roomId", ex);

                throw;
            }
        }

        /// <summary>
        /// Changes a player's health by the given amount
        /// </summary>
        public static void ChangeHealth(string username, int amount)
        {
            OleDbHelper.ChangeNumeric(TBL_NAME, "health", amount, "username='" + username + "'");
        }

        /// <summary>
        /// Changes a player's attack power by the given amount
        /// </summary>
        public static void ChangeAttack(string username, int amount)
        {
            OleDbHelper.ChangeNumeric(TBL_NAME, "attack", amount, "username='" + username + "'");
        }

        /// <summary>
        /// Changes a player's defense power by the given amount
        /// </summary>
        public static void ChangeDefense(string username, int amount)
        {
            OleDbHelper.ChangeNumeric(TBL_NAME, "defense", amount, "username='" + username + "'");
        }

        /// <summary>
        /// Changes a player's experience points by the given amount
        /// </summary>
        public static void ChangeXp(string username, int amount)
        {
            OleDbHelper.ChangeNumeric(TBL_NAME, "xp", amount, "username='" + username + "'");
        }

        /// <summary>
        /// Increments the given player's level by 1, resets their XP to 0, and sets their attack, defense and health to the given values.
        /// </summary>
        public static void LevelUp(string username, int attack, int defense, int health)
        {
            OleDbHelper.ExecuteNonQuery("UPDATE {0} SET [level] = [level] + 1, {1} WHERE username={2}",
                TBL_NAME,
                OleDbHelper.MakeSetClause("xp, attack, defense, health", 0, attack, defense, health),
                OleDbHelper.FormatForDB(username));
        }

        /// <summary>
        /// Returns whether or not a user with the given username exists.
        /// </summary>
        public static bool IsUsernameTaken(string username)
        {
            return OleDbHelper.ExecuteScalar("SELECT username FROM {0} WHERE username={1}", TBL_NAME, OleDbHelper.FormatForDB(username)) != null;
        }

        /// <summary>
        /// Returns whether or not a user with the given username and password exists.
        /// </summary>
        public static bool DoesUserExist(string username, string password)
        {
            return OleDbHelper.ExecuteScalar("SELECT username FROM {0} WHERE {1}", TBL_NAME, OleDbHelper.MakeAndClause("username, userPass", username, password)) != null;
        }

        /// <summary>
        /// Changes whether or not the given user is banned.
        /// </summary>
        /// <param name="username">The username of the user to set the ban status for</param>
        /// <param name="banned">The new ban status - <see langword="true"/>  for banned, <see langword="false"/> for not banned.</param>
        public static void SetBanStatus(string username, bool banned)
        {
            OleDbHelper.Update(TBL_NAME, "username='" + username + "'", "isBanned", banned);
        }

        /// <summary>
        /// Returns whether or not the given user is banned.
        /// </summary>
        public static bool IsBanned(string username)
        {
            try
            {
                return (bool)OleDbHelper.ExecuteScalar("SELECT isBanned FROM {0} WHERE username='{1}'", TBL_NAME, username);
            }
            catch (NullReferenceException)
            {
                throw new ArgumentException("User does not exist", "username");
            }
        }

        /// <summary>
        /// Returns a <see cref="DataTable"/> containing a single column with the usernames of all users.
        /// </summary>
        public static DataTable GetAllPlayerNames()
        {
            return OleDbHelper.GetDataTable("SELECT username FROM " + TBL_NAME);
        }

        /// <summary>
        /// This method should be used for testing only!
        /// Normally, players should never be deleted
        /// </summary>
        /// <param name="username"></param>
        internal static void DeletePlayer(string username)
        {
            OleDbHelper.Delete(TBL_NAME, "username", username);
        }
    }

    /// <summary>
    /// Thrown when an operation would result in multiple users having the same username.
    /// </summary>
    public class DuplicateUsernameException : ArgumentException
    {
        /// <summary>Initializes a new instance of <see cref="DuplicateUsernameException" /> with a specified parameter name and a reference to the inner exception that is the cause of this exception.</summary>
        /// <param name="paramName">The name of the parameter that caused the current exception. </param>
        /// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference, the current exception is raised in a catch block that handles the inner exception. </param>
        public DuplicateUsernameException(string paramName, Exception innerException)
            : base("Cannot create a player with an existing username", paramName, innerException) { }
    }

    /// <summary>
    /// Thrown when a room id is given as an argument but it does not match any room.
    /// </summary>
    public class NonexistentRoomException : ArgumentException
    {
        /// <summary>Initializes a new instance of <see cref="NonexistentRoomException" /> with a specified parameter name and a reference to the inner exception that is the cause of this exception.</summary>
        /// <param name="paramName">The name of the parameter that caused the current exception. </param>
        /// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference, the current exception is raised in a catch block that handles the inner exception. </param>
        public NonexistentRoomException(string paramName, Exception innerException)
            : base("No room with the given id exists", paramName, innerException) { }
    }
}
