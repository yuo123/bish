﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BishDAL
{
    /// <summary>
    /// Provides access to the items table.
    /// </summary>
    public static class ItemsDAL
    {
        internal const string TBL_NAME = "ItemsTbl";

        /// <summary>
        /// Creates a new item on a room's floor.
        /// </summary>
        /// <param name="attack">The item's attack bonus</param>
        /// <param name="defense">The item's defense bonus</param>
        /// <param name="name">The item's name</param>
        /// <param name="displayCharacter">The character which represents the item in the game</param>
        /// <param name="roomId">The ID of the room which the item will be placed inside</param>
        /// <param name="xPos">The X-Position of the item in the room</param>
        /// <param name="yPos">The Y-Position of the item in the room</param>
        /// <returns>The ID of the new item</returns>
        public static int CreateItemOnFloor(int attack, int defense, string name, char displayCharacter, int roomId, int xPos, int yPos)
        {
            return OleDbHelper.InsertAndGetId(TBL_NAME,
                "attack, defense, itemName, displayCharacter, roomId, xPos, yPos",
                attack, defense, name, displayCharacter.ToString(), roomId, xPos, yPos);
        }

        /// <summary>
        /// Creates a new item in a player's inventory.
        /// </summary>
        /// <param name="attack">The item's attack bonus</param>
        /// <param name="defense">The item's defense bonus</param>
        /// <param name="name">The item's name</param>
        /// <param name="displayCharacter">The character which represents the item in the game</param>
        /// <param name="playerName">The username of the player whose inventory the item will be added to</param>
        /// <returns>The ID of the new item</returns>
        public static int CreateItemOnPlayer(int attack, int defense, string name, char displayCharacter, string playerName)
        {
            return OleDbHelper.InsertAndGetId(TBL_NAME,
                "attack, defense, itemName, displayCharacter, playerUsername",
                attack, defense, name, displayCharacter.ToString(), playerName);
        }

        /// <summary>
        /// Creates a new item in an enemy's inventory.
        /// </summary>
        /// <param name="attack">The item's attack bonus</param>
        /// <param name="defense">The item's defense bonus</param>
        /// <param name="name">The item's name</param>
        /// <param name="displayCharacter">The character which represents the item in the game</param>
        /// <param name="enemyId">The id of the enemy whose inventory the item will be added to</param>
        /// <returns>The ID of the new item</returns>
        public static int CreateItemOnEnemy(int attack, int defense, string name, char displayCharacter, int enemyId)
        {
            return OleDbHelper.InsertAndGetId(TBL_NAME,
                "attack, defense, itemName, displayCharacter, enemyId",
                attack, defense, name, displayCharacter.ToString(), enemyId);
        }

        /// <summary>
        /// Gets a <see cref="DataRow"/> containing the details of the item with the given id.
        /// </summary>
        public static DataRow GetItemDetails(int itemId)
        {
            return OleDbHelper.GetDataRow(string.Format("SELECT * FROM {0} WHERE itemId={1}", TBL_NAME, itemId));
        }

        /// <summary>
        /// Moves an item from a character's inventory to a room's floor.
        /// </summary>
        public static void DropItem(int itemId, int roomId, int xPos, int yPos)
        {
            OleDbHelper.Update(TBL_NAME, "itemId=" + itemId, "playerUsername, enemyId, roomId, xPos, yPos", null, null, roomId, xPos, yPos);
        }

        /// <summary>
        /// Moves an item from a room's floor to an enemy's inventory.
        /// </summary>
        public static void PickupItem(int itemId, int enemyId)
        {
            OleDbHelper.Update(TBL_NAME, "itemId=" + itemId, "roomId, enemyId", null, enemyId);
        }

        /// <summary>
        /// Moves an item from a room's floor to a player's inventory.
        /// </summary>
        public static void PickupItem(int itemId, string playerUsername)
        {
            OleDbHelper.Update(TBL_NAME, "itemId=" + itemId, "roomId, playerUsername", null, playerUsername);
        }

        /// <summary>
        /// Deletes the item with the given ID.
        /// </summary>
        /// <param name="itemId"></param>
        public static void DeleteItem(int itemId)
        {
            OleDbHelper.Delete(TBL_NAME, "itemId", itemId);
        }

        /// <summary>
        /// Returns a <see cref="DataTable"/> containing the details of all the items in a room.
        /// </summary>
        public static DataTable GetItemsInRoom(int roomId)
        {
            return OleDbHelper.GetDataTable("SELECT * FROM {0} WHERE roomId={1}", TBL_NAME, roomId);
        }

        /// <summary>
        /// Returns a <see cref="DataTable"/> containing the details of all the items in an enemy's inventory.
        /// </summary>
        public static DataTable GetItemsOnEnemy(int physicalEnemyId)
        {
            return OleDbHelper.GetDataTable("SELECT * FROM {0} WHERE enemyId={1}", TBL_NAME, physicalEnemyId);
        }

        /// <summary>
        /// Returns a <see cref="DataTable"/> containing the details of all the items in a player's inventory.
        /// </summary>
        public static DataTable GetItemsOnPlayer(string playerUsername)
        {
            return OleDbHelper.GetDataTable("SELECT * FROM {0} WHERE playerUsername='{1}'", TBL_NAME, playerUsername);
        }

        /// <summary>
        /// Deletes all the items in a given room.
        /// </summary>
        public static void DeleteItemsInRoom(int roomId)
        {
            OleDbHelper.Delete(TBL_NAME, "roomId", roomId);
        }

        /// <summary>
        /// Deletes all the items in a given player's inventory.
        /// </summary>
        public static void DeleteItemsOnPlayer(string username)
        {
            OleDbHelper.Delete(TBL_NAME, "playerUsername", username);
        }
    }
}
