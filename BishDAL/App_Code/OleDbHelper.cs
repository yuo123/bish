﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.IO;

namespace BishDAL
{
    /// <summary>
    /// A helper class for accessing MSAccess databases
    /// </summary>
    /// <remarks>
    /// For OleDbException types thrown by this class, 
    /// obtain the error code from exception.Errors[0].SqlState
    /// and see https://msdn.microsoft.com/en-us/library/bb221208.aspx
    /// </remarks>
    internal static class OleDbHelper
    {
        private static OleDbConnection _connection;

        private static string GetConnectionString()
        {
            // Name of database file, with extension (.accdb) 
            const string FILENAME = "BISHDAT.accdb";
            // Name of the DAL project folder
            const string DAL_PROJECT = "BishDAL";
            // Name of the solution folder
            const string SOLUTION_FOLDER = "BISH";

            // Get the solution directory, by getting the location of the EXE file and going up to the first folder whose name matches the solution:
            // System.Reflection.Assembly.GetExecutingAssembly() - the EXE file 
            // .EscapedCodeBase - the path to the file, as an escaped URI (e.g. "file:///C:/my%20example")
            // new Uri(...).LocalPath - the path as an actual system path (e.g. "C:\my example")
            // NavigateUpTo(...) - find the solution folder
            string solution = NavigateUpTo(
                new Uri(System.Reflection.Assembly.GetExecutingAssembly().EscapedCodeBase).LocalPath,
                SOLUTION_FOLDER);

            // Insert the path into the connection string, using Path.Combine which puts the slashes ('\') between the path components 
            return "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"" + Path.Combine(solution, DAL_PROJECT, "App_Data", FILENAME) + "\";Persist Security Info=True";
        }

        /// <summary>
        /// Returns a the path to the deepest folder of the given name in the given path string.
        /// For example, Calling <c>NavigateUpTo("C:a\b\a\b\c", "a")</c> will return <c>"C:a\b\a"</c>
        /// </summary>
        private static string NavigateUpTo(string path, string dir)
        {
            return path.Substring(0, path.LastIndexOf("\\" + dir + "\\") + dir.Length + 1);
        }

        private static OleDbConnection GetOpenConnection()
        {
            if (_connection == null)
                _connection = new OleDbConnection(GetConnectionString());
            if (_connection.State != ConnectionState.Open)
                _connection.Open();

            return _connection;
        }

        public static void CloseOpenConnection()
        {
            if (_connection != null && _connection.State == ConnectionState.Open)
                _connection.Close();
        }

        // Disconnected 
        /// <summary>
        /// Returns the DataSet resulting from the specified SQL command
        /// </summary>
        public static DataSet GetDataSet(string strSql)
        {
            OleDbConnection connection = GetOpenConnection();
            OleDbCommand command = new OleDbCommand(strSql, connection);
            OleDbDataAdapter dataAdapter = new OleDbDataAdapter(command);
            DataSet ds = new DataSet();
            // This makes the DataSet adapt its schema (i.e. primary key definitions) to the DB data
            dataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            dataAdapter.Fill(ds);
            return ds;
        }

        /// <summary>
        /// Returns the first DataTable resulting from the specified SQL command
        /// </summary>
        public static DataTable GetDataTable(string sqlFormat, params object[] args)
        {
            return GetDataTable(string.Format(sqlFormat, args));
        }

        /// <summary>
        /// Returns the first DataTable resulting from the specified SQL command
        /// </summary>
        public static DataTable GetDataTable(string strSql)
        {
            return GetDataSet(strSql).Tables[0];
        }

        /// <summary>
        /// Returns the first DataRow resulting from the specified SQL command
        /// </summary>
        public static DataRow GetDataRow(string sqlFormat, params object[] args)
        {
            return GetDataRow(string.Format(sqlFormat, args));
        }

        /// <summary>
        /// Returns the first DataRow resulting from the specified SQL command
        /// </summary>
        public static DataRow GetDataRow(string strSql)
        {
            DataTable dt = GetDataSet(strSql).Tables[0];
            if (dt.Rows.Count == 0)
                return null;
            return dt.Rows[0];
        }

        // connected 
        /// <summary>
        /// Returns the single value resulting from the specified SQL command
        /// </summary>
        public static object ExecuteScalar(string strSql)
        {
            OleDbConnection connection = GetOpenConnection();
            OleDbCommand command = new OleDbCommand(strSql, connection);
            return command.ExecuteScalar();
        }

        /// <summary>
        /// Returns the single value resulting from the specified SQL command, constructed with the given format string
        /// </summary>
        /// <param name="sqlFormat">An SQL query before a call to string.Format()</param>
        /// <param name="args">Objects to be put into the query using string.Format()</param>
        public static object ExecuteScalar(string sqlFormat, params object[] args)
        {
            return ExecuteScalar(string.Format(sqlFormat, args));
        }

        // connected 
        /// <summary>
        /// Executes the specified SQL command, and returns the number of rows affected
        /// </summary>
        public static int ExecuteNonQuery(string strSql)
        {
            OleDbConnection connection = GetOpenConnection();
            OleDbCommand command = new OleDbCommand(strSql, connection);
            return command.ExecuteNonQuery();
        }

        /// <summary>
        /// Executes the specified SQL command, constructed with the given format string, and returns the number of rows affected
        /// </summary>
        /// <param name="sqlFormat">An SQL query before a call to string.Format()</param>
        /// <param name="args">Objects to be put into the query using string.Format()</param>
        public static int ExecuteNonQuery(string sqlFormat, params object[] args)
        {
            return ExecuteNonQuery(string.Format(sqlFormat, args));
        }

        /// <summary>
        /// Inserts the given values into the specified columns of the specified table
        /// </summary>
        /// <param name="tblName">The name of the table in the database</param>
        /// <param name="columns">A string of column names separated by commas, like "col1, col2, col3"</param>
        /// <param name="values">The values to insert</param>
        public static void Insert(string tblName, string columns, params object[] values)
        {
            ExecuteNonQuery("INSERT INTO {0} ({1}) VALUES ({2});", tblName, columns, MakeValuesClause(values));
        }

        /// <summary>
        /// Inserts the given values into the specified columns of the specified table, 
        /// and returns the auto-generated integer type primary key
        /// </summary>
        /// <param name="tblName">The name of the table in the database</param>
        /// <param name="columns">A string of column names separated by commas, like "col1, col2, col3"</param>
        /// <param name="values">The values to insert</param>
        public static int InsertAndGetId(string tblName, string columns, params object[] values)
        {
            return ExecuteAndGetId("INSERT INTO {0} ({1}) VALUES ({2});", tblName, columns, MakeValuesClause(values));
        }

        /// <summary>
        /// Executes the INSERT statement given by the specified format string and parameters, 
        /// and returns the auto-generated integer type primary key.
        /// </summary>
        /// <param name="sqlInsertFormat">A format string for the INSERT statement</param>
        /// <param name="values">The format parameters to be put into <paramref name="sqlInsertFormat"/></param>
        public static int ExecuteAndGetId(string sqlInsertFormat, params object[] values)
        {
            OleDbConnection con = GetOpenConnection();
            //first command - insert
            string sql = string.Format(sqlInsertFormat, values);
            OleDbCommand com = new OleDbCommand(sql, con);
            com.ExecuteNonQuery();
            //second command - get the id
            //must be with the same open connection!
            com = new OleDbCommand("SELECT @@IDENTITY", con);
            try
            {
                return (int)com.ExecuteScalar();
            }
            catch (InvalidCastException ex)
            {
                throw new InvalidOperationException("Primary key created by query is not of type int", ex);
            }
        }

        /// <summary>
        /// Updates the specified columns of the specified table with the given values
        /// </summary>
        /// <param name="tblName">The name of the table in the database</param>
        /// <param name="where">A condition for the changed row to fulfill</param>
        /// <param name="columns">A string of column names separated by comas, like "col1, col2, col3"</param>
        /// <param name="values">The values to update</param>
        public static void Update(string tblName, string where, string columns, params object[] values)
        {
            ExecuteNonQuery("UPDATE {0} SET {1} WHERE {2};", tblName, MakeSetClause(columns, values), where);
        }

        /// <summary>
        /// Adds the given amount to the specified numeric column of the specified
        /// </summary>
        /// <param name="tblName">The field's table</param>
        /// <param name="column">The column name to change</param>
        /// <param name="amount">The signed amount to change by</param>
        /// <param name="where">A condition for the changed row to fulfill</param>
        /// <exception cref="OverflowException">Thrown on numeric overflow</exception>
        public static void ChangeNumeric(string tblName, string column, int amount, string where)
        {
            try
            {
                ExecuteNonQuery("UPDATE {0} SET {1}={1}+{2} WHERE {3}", tblName, column, amount, where);
            }
            catch (OleDbException ex)
            {
                if (ex.Errors[0].SQLState == "3072")
                    throw new OverflowException("Database value overflown", ex);

                throw;
            }
        }

        /// <summary>
        /// Deletes all rows from the specified table that fulfill the specified condition
        /// </summary>
        /// <param name="tblName">The table to delete from</param>
        /// <param name="columns">The columns for the condition</param>
        /// <param name="values">The values to which the columns must be equal</param>
        public static void Delete(string tblName, string columns, params object[] values)
        {
            ExecuteNonQuery("DELETE FROM {0} WHERE {1}", tblName, MakeAndClause(columns, values));
        }

        /// <summary>
        /// Updates the database table according to the given DataTable and SQL command
        /// </summary>
        /// <param name="dt">The DataTable to copy</param>
        /// <param name="sql">The SQL command</param>
        public static void Update(DataTable dt, string sql)
        {
            OleDbConnection cn = GetOpenConnection();
            OleDbCommand command = new OleDbCommand(sql, cn);

            OleDbDataAdapter adapter = new OleDbDataAdapter(command);

            OleDbCommandBuilder builder = new OleDbCommandBuilder(adapter);
            adapter.InsertCommand = builder.GetInsertCommand();
            adapter.DeleteCommand = builder.GetDeleteCommand();
            adapter.UpdateCommand = builder.GetUpdateCommand();

            adapter.Update(dt);
        }

        /// <summary>
        /// Formats the given object to be used in SQL
        /// </summary>
        public static string FormatForDB(object obj)
        {
            if (obj is int || obj is double || obj is bool)
                return obj.ToString();
            if (obj is string)
                return "'" + obj + "'";
            if (obj == null)
                return "NULL";
            if (obj is bool)
                return (bool)obj ? "1" : "0";
            if (obj is DateTime)
                //this is the DateTime format required by SQL
                return ((DateTime)obj).ToOADate().ToString();
            throw new ArgumentException("Unrecognized type for formatting", "obj");
        }

        /// <summary>
        /// Constructs a VALUES clause, without parentheses, for an UPDATE or INSERT statement
        /// </summary>
        public static string MakeValuesClause(params object[] values)
        {
            return string.Join(", ", values.Select(FormatForDB));
        }

        /// <summary>
        /// Constructs a SET clause, made of a series of column names, each followed by an equals sign and its value, and separated by commas
        /// </summary>
        public static string MakeSetClause(string columns, params object[] values)
        {
            string[] cols = columns.Replace(" ", "").Split(',');
            return string.Join(", ", cols.Zip(values, (col, val) => col + "=" + FormatForDB(val)));
        }

        /// <summary>
        /// Constructs a WHERE clause, made of a series of column names, each followed by an equals sign and its value, and separated by AND
        /// </summary>
        public static string MakeAndClause(string columns, params object[] values)
        {
            string[] cols = columns.Replace(" ", "").Split(',');
            return string.Join(" AND ", cols.Zip(values, //Zip turns column-values pairs into single strings, using the function below:
                (col, val) =>
                    col //the column name
                    + (val != null ? "=" : " IS ") // IS for null, '=' for everything else
                    + FormatForDB(val))); //properly formatted value (see FormatForDB)
        }

        public static string RowToString(DataRow dr)
        {
            return string.Join("\t", dr.ItemArray);
        }

        /// <summary>
        /// Returns a <see cref="char"/> from the database, that is stored as a single-character string.
        /// </summary>
        /// <param name="str">An object-reference to a string containing exactly one character.</param>
        /// <exception cref="InvalidCastException">When the argument is not a string.</exception>
        /// <exception cref="ArgumentException">When the input string does not contain exactly one character.</exception>
        public static char CharOrError(object str)
        {
            try
            {
                return ((string)str).Single();
            }
            catch (InvalidCastException ex)
            {
                throw new InvalidCastException("Object is not a string", ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new ArgumentException("Input string must contain exactly one character", "str", ex);
            }
        }
    }
}