﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;

namespace BishDAL
{
    static class Program
    {
        static void Main(string[] args)
        {
            TestItemsDAL();
        }

        public static void TestEnemyTypesDAL()
        {
            EnemyTypesDAL.AddEnemyType(3, 5, 2, 4, "Test Enemy", 'e');
            DataTable dt = EnemyTypesDAL.GetAllEnemies(3);
            DataRow dr = EnemyTypesDAL.GetEnemyType((int)dt.Rows[0]["enemyTypeId"]);
        }

        public static void TestGamesDAL()
        {
            GamesDAL.NewGame("a");
            DataRow game = GamesDAL.GetUnfinishedGame("a");
            int id = (int)game["gameId"];
            GamesDAL.AddXp(id, 13);
            GamesDAL.AddKilledEnemy(id, 8);
            GamesDAL.AddKilledEnemy(id, 9);
            GamesDAL.AddKilledEnemy(id, 8);
            GamesDAL.FinishGame(id);
            DataTable dt = GamesDAL.GetEnemiesKilled(id);
            dt = GamesDAL.GetAllGames("a");
            dt = GamesDAL.GetAllGames();
        }

        public static void TestPhysicalEnemiesDAL()
        {
            const int ROOM = 1;
            PhysicalEnemiesDAL.AddPhysicalEnemy(10, ROOM, 2, 3);
            DataRow enemy = PhysicalEnemiesDAL.GetEnemyAt(ROOM, 2, 3);
            int id = (int)enemy["physicalEnemyId"];
            PhysicalEnemiesDAL.MoveEnemy(id, 4, 4);
            PhysicalEnemiesDAL.ChangeHealth(id, -1);
            DataTable dt = PhysicalEnemiesDAL.GetFullPhysicalEnemies(ROOM);
            PhysicalEnemiesDAL.DeletePhysicalEnemy(id);
        }

        public static void TestPlayersDAL()
        {
            const string USERNAME = "Test User";
            const string PASS = "Test Password";
            const string EMAIL = "a@b.com";
            const string COUNTRY = "Israel";
            DateTime DOB = new DateTime(2000, 1, 2);
            try
            {
                Debug.Assert(!PlayersDAL.DoesUserExist(USERNAME, PASS));
                PlayersDAL.CreatePlayer(USERNAME, PASS, EMAIL, COUNTRY, DOB);
                Debug.Assert(PlayersDAL.DoesUserExist(USERNAME, PASS));
                Debug.Assert(PlayersDAL.IsUsernameTaken(USERNAME));

                PlayersDAL.SetStartingStats(USERNAME);
                PlayersDAL.ChangePlayerRoom(USERNAME, 1, 2, 3);
                PlayersDAL.ChangeXp(USERNAME, 2);
                DataRow dr = PlayersDAL.GetPlayerPhysicalData(USERNAME);
                Debug.Assert((int)dr["xp"] == 2);

                PlayersDAL.ChangeXp(USERNAME, 3);
                PlayersDAL.ChangeAttack(USERNAME, 2);
                PlayersDAL.ChangeDefense(USERNAME, -1);
                PlayersDAL.ChangeHealth(USERNAME, 5);
                PlayersDAL.MovePlayer(USERNAME, 6, 7);
                dr = PlayersDAL.GetPlayerPhysicalData(USERNAME);
                Debug.Assert((int)dr["xp"] == 5);
                Debug.Assert((int)dr["attack"] == 5);
                Debug.Assert((int)dr["defense"] == 3);
                Debug.Assert((int)dr["health"] == 15);
                Debug.Assert((int)dr["yPos"] == 7);

                PlayersDAL.LevelUp(USERNAME, 1, 1, 1);
                dr = PlayersDAL.GetPlayerPhysicalData(USERNAME);
                Debug.Assert((int)dr["xp"] == 0);
                Debug.Assert((int)dr["health"] == 1);
                Debug.Assert((int)dr["roomWidth"] > 0);

                DataTable dt = PlayersDAL.GetPlayersInRoom(1);
                Debug.Assert((string)dt.Rows[dt.Rows.Count - 1]["username"] == USERNAME);
            }
            finally
            {
                //test cleanup, for debugging only
                OleDbHelper.Delete(PlayersDAL.TBL_NAME, "username", USERNAME);
            }
        }

        public static void TestItemsDAL()
        {
            ItemsDAL.CreateItemOnFloor(2, 0, "Test Item", 'i', 1, 2, 1);
            DataTable dt = ItemsDAL.GetItemsInRoom(1);
            int id = (int)dt.Rows[dt.Rows.Count - 1]["itemId"];

            ItemsDAL.PickupItem(id, "a");
            DataRow dr = ItemsDAL.GetItemDetails(id);
            Debug.Assert((string)dr["playerUsername"] == "a");
            Debug.Assert(dr["roomId"] is DBNull);

            ItemsDAL.DropItem(id, 1, 8, 8);
            ItemsDAL.DeleteItemsInRoom(1);
            Debug.Assert(ItemsDAL.GetItemDetails(id) == null);

            PhysicalEnemiesDAL.AddPhysicalEnemy(10, 1, 0, 0);
            dr = PhysicalEnemiesDAL.GetEnemyAt(1, 0, 0);
            int eId = (int)dr["physicalEnemyId"];
            ItemsDAL.CreateItemOnEnemy(0, 1, "Test Item", 'i', eId);
            dt = ItemsDAL.GetItemsOnEnemy(eId);
            id = (int)dt.Rows[dt.Rows.Count - 1]["itemId"];
            PhysicalEnemiesDAL.DeletePhysicalEnemy(eId);
            Debug.Assert(ItemsDAL.GetItemDetails(id) == null);
        }

        public static void TestRoomsDAL()
        {
            //basic
            int room = RoomsDAL.CreateRoom(6, 9, 3);
            Debug.Assert(RoomsDAL.DoesRoomExist(room));
            DataTable empty = RoomsDAL.GetEmptyRoomsIds();
            Debug.Assert(ColumnContains(empty, "roomId", room));
            DataRow dr = RoomsDAL.GetRoom(room);
            Debug.Assert((int)dr["difficulty"] == 3);

            //door creation
            int door = RoomsDAL.CreateDoor(room, 1, 1);
            int room2 = RoomsDAL.CreateRoom(1, 1, 1);
            RoomsDAL.ConnectDoor(door, room2, 2, 2, 1);
            dr = RoomsDAL.GetDoorByPosition(room, 1, 1);
            Debug.Assert(dr != null);

            //door re-connection and cleanup
            RoomsDAL.DeleteRoom(room);
            dr = RoomsDAL.GetDoorByPosition(room2, 2, 2);
            Debug.Assert(dr != null);
            Debug.Assert(RoomsDAL.GetDoorByPosition(room, 1, 1) == null);
            RoomsDAL.ConnectDoor(door, room2, 1, 1, 0);
            dr = RoomsDAL.GetDoorByPosition(room2, 1, 1);
            Debug.Assert(dr != null);
            RoomsDAL.DeleteRoom(room2);
            RoomsDAL.RemoveDisconnectedDoors();
            Debug.Assert(OleDbHelper.GetDataRow("SELECT * FROM {0} WHERE doorId={1}", RoomsDAL.DOORS_TBL_NAME, door) == null);
        }

        private static bool ColumnContains(DataTable dt, string col, object value)
        {
            DataColumn dc = dt.Columns[col];
            foreach (DataRow dr in dt.Rows)
                if (dr[dc].Equals(value))
                    return true;

            return false;
        }
    }
}