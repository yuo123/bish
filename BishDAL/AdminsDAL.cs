﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;

namespace BishDAL
{
    /// <summary>
    /// Provides access to the admins table
    /// </summary>
    public static class AdminsDAL
    {
        internal const string TBL_NAME = "AdminsTbl";

        /// <summary>
        /// Creates a new admin with the given properties.
        /// </summary>
        /// <param name="username">The admin's username</param>
        /// <param name="password">The admin's password</param>
        /// <param name="permissionLevel">The admins permission level: 0 for regular, 1 for SuperAdmin.</param>
        public static void CreateAdmin(string username, string password, int permissionLevel)
        {
            try
            {
                OleDbHelper.Insert(TBL_NAME, "adminUsername, adminPass, permissionLevel", username, password, permissionLevel);
            }
            catch (OleDbException ex)
            {
                if (ex.Errors[0].SQLState == "3022")
                    throw new ArgumentException("Cannot create admin with duplicate username.", "username", ex);

                throw;
            }
        }

        /// <summary>
        /// Changes an admin's username.
        /// </summary>
        public static void ChangeUsername(string oldName, string newName)
        {
            try
            {
                OleDbHelper.Update(TBL_NAME, "adminUsername='" + oldName + "'", "adminUsername", newName);
            }
            catch (OleDbException ex)
            {
                if (ex.Errors[0].SQLState == "3022")
                    throw new ArgumentException("Cannot change admin to duplicate username.", "newName", ex);

                throw;
            }
        }

        /// <summary>
        /// Changes an admin's permission level: 0 for regular, 1 for SuperAdmin.
        /// </summary>
        public static void SetPermissionLevel(string username, int permissionLevel)
        {
            OleDbHelper.Update(TBL_NAME, "adminUsername='" + username + "'", "permissionLevel", permissionLevel);
        }

        /// <summary>
        /// Returns whether or not an admin with the given username and password exists.
        /// </summary>
        public static bool DoesAdminExist(string username, string password)
        {
            return OleDbHelper.ExecuteScalar("SELECT adminUsername FROM {0} WHERE adminUsername='{1}' AND adminPass='{2}'", TBL_NAME, username, password) != null;
        }

        /// <summary>
        /// Returns whether or not an admin with the given username exists.
        /// </summary>
        public static bool DoesAdminExist(string username)
        {
            return OleDbHelper.GetDataRow("SELECT adminUsername FROM {0} WHERE adminUsername='{1}'", TBL_NAME, username) != null;
        }

        /// <summary>
        /// Gets an admin's permission level: 0 for regular, 1 for SuperAdmin.
        /// </summary>
        public static int GetPermissionLevel(string username)
        {
            try
            {
                return (int)OleDbHelper.ExecuteScalar("SELECT permissionLevel FROM {0} WHERE adminUsername='{1}'", TBL_NAME, username);
            }
            catch (InvalidCastException ex)
            {
                throw new ArgumentException("Admin username not found", "username", ex);
            }
        }

        /// <summary>
        /// Deletes the admin with the given username.
        /// </summary>
        public static void DeleteAdmin(string username)
        {
            OleDbHelper.Delete(TBL_NAME, "adminUsername", username);
        }

        /// <summary>
        /// Returns a <see cref="DataTable"/> containing the details of all the admins that exist.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllAdmins()
        {
            return OleDbHelper.GetDataTable("SELECT adminUsername, permissionLevel FROM " + TBL_NAME);
        }

        /// <summary>
        /// Changes an admin's password.
        /// </summary>
        /// <param name="username">The username of the admin to change the password for</param>
        /// <param name="newPass">The new password for the admin</param>
        public static void ChangePassword(string username, string newPass)
        {
            OleDbHelper.Update(TBL_NAME, "adminUsername='" + username + "'", "adminPass", newPass);
        }
    }
}
