﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BishDAL
{
    /// <summary>
    /// Provides access to the physical enemies table.
    /// </summary>
    public static class PhysicalEnemiesDAL
    {
        internal const string TBL_NAME = "PhysicalEnemiesTbl";

        /// <summary>
        /// Adds a physical enemy of the specified type at the specified location to the database and returns its id.
        /// </summary>
        /// <returns>The auto-generated id of the new physical enemy</returns>
        public static int AddPhysicalEnemy(int enemyTypeId, int roomId, int x, int y)
        {
            return OleDbHelper.ExecuteAndGetId(
                "INSERT INTO {0} (enemyTypeId, roomId, health, xPos, yPos) "
                + "SELECT {1}, {2}, startingHealth, {3}, {4} FROM {5} "
                + "WHERE {5}.enemyTypeId={1}",
                TBL_NAME, enemyTypeId, roomId, x, y, EnemyTypesDAL.TBL_NAME);
        }

        /// <summary>
        /// Returns a <see cref="DataTable"/> with the details of all the enemies in the room with the specified ID.
        /// </summary>
        public static DataTable GetFullPhysicalEnemies(int roomId)
        {
            DataTable re = OleDbHelper.GetDataTable(
                "SELECT * FROM {0} "
                + "INNER JOIN {1} "
                + "ON {0}.enemyTypeId = {1}.enemyTypeId "
                + "WHERE roomId={2}"
                , TBL_NAME, EnemyTypesDAL.TBL_NAME, roomId);
            re.PrimaryKey = new[] { re.Columns["physicalEnemyId"] };

            return re;
        }

        /// <summary>
        /// Returns a <see cref="DataRow"/> representing the enemy in the given room and position,
        /// or <see langword="null"/> if no enemy is present there.
        /// </summary>
        /// <returns>A <see cref="DataRow"/> containing the enemy's details, or <see langword="null"/>.</returns>
        public static DataRow GetEnemyAt(int roomId, int x, int y)
        {
            return OleDbHelper.GetDataRow(string.Format(
                "SELECT * FROM {0} "
                + "INNER JOIN {1} "
                + "ON {0}.enemyTypeId = {1}.enemyTypeId "
                + "WHERE roomId={2} AND xPos={3} AND yPos={4}",
                TBL_NAME, EnemyTypesDAL.TBL_NAME, roomId, x, y));
        }

        /// <summary>
        /// Changes the position of the given enemy.
        /// </summary>
        /// <param name="physicalId">The enemy to change the position of</param>
        /// <param name="x">The new X-Position</param>
        /// <param name="y">The new Y-Position</param>
        public static void MoveEnemy(int physicalId, int x, int y)
        {
            OleDbHelper.Update(TBL_NAME, "physicalEnemyId=" + physicalId, "xPos, yPos", x, y);
        }

        /// <summary>
        /// Changes an enemy's health
        /// </summary>
        public static void ChangeHealth(int physicalId, int amount)
        {
            OleDbHelper.ChangeNumeric(TBL_NAME, "health", amount, "physicalEnemyId=" + physicalId);
        }

        /// <summary>
        /// Deletes a physical enemy.
        /// </summary>
        public static void DeletePhysicalEnemy(int physicalId)
        {
            OleDbHelper.Delete(TBL_NAME, "physicalEnemyId", physicalId);
        }

        /// <summary>
        /// Deletes all the enemies in a room with their items.
        /// </summary>
        public static void DeleteEnemiesInRoom(int roomId)
        {
            //Relation settings will delete any items that are on the deleted enemies
            OleDbHelper.Delete(TBL_NAME, "roomId", roomId);
        }
    }
}
