﻿using System;
using System.Linq;
using System.Collections;
using Microsoft.CSharp;
using System.CodeDom;
using System.Reflection;
using System.IO;
using System.Threading;
using System.Globalization;
using System.Text;
using System.Xml.Serialization;

namespace BishUI.CodeGeneration
{
    internal static class Program
    {
        public static void Main()
        {
            //GenerateViewModelDir(@"D:\Users\איל\Desktop\C# Projects\BISH\BishUI\ViewModel\", typeof(BishBL.Player).Assembly);
        }

        private static void GenerateViewModelDir(string outputPath, Assembly assembly)
        {
            if (!Directory.Exists(outputPath))
                Directory.CreateDirectory(outputPath);

            var provider = new CSharpCodeProvider();
            Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            foreach (TypeInfo cls in assembly.GetTypes().Where(
                t => t.IsClass && !t.IsAbstract && !t.IsSpecialName && t.Name != "Program" && !t.Name.StartsWith("Related") && !t.IsNested && t.IsPublic && !t.IsGenericType && !typeof(Delegate).IsAssignableFrom(t)))
            {
                var sb = new StringBuilder();
                using (var writer = new StringWriter(sb))
                {
                    CodeCompileUnit unit = GenerateModel(cls, provider);
                    provider.GenerateCodeFromCompileUnit(unit, writer,
                        new System.CodeDom.Compiler.CodeGeneratorOptions { BlankLinesBetweenMembers = false, BracingStyle = "C" });
                    sb.Replace("partial class ViewModelExtensions", "static partial class ViewModelExtensions");
                    File.WriteAllText(Path.Combine(outputPath, cls.Name + "Model.cs"), sb.ToString());
                }
            }
        }

        private static CodeCompileUnit GenerateModel(Type oType, CSharpCodeProvider provider)
        {
            var unit = new CodeCompileUnit();
            var space = new CodeNamespace("BishUI");
            var globalSpace = new CodeNamespace();
            unit.Namespaces.Add(globalSpace);
            unit.Namespaces.Add(space);

            globalSpace.Imports.Add(new CodeNamespaceImport("System"));
            globalSpace.Imports.Add(new CodeNamespaceImport("System.Linq"));
            globalSpace.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
            globalSpace.Imports.Add(new CodeNamespaceImport("BishBL"));

            CodeTypeDeclaration modelCls = GenerateModelClass(oType, provider);
            space.Types.Add(modelCls);
            CodeTypeDeclaration extCls = GenerateExtensionClass(oType, modelCls);
            space.Types.Add(extCls);

            return unit;
        }

        private static CodeTypeDeclaration GenerateModelClass(Type oType, CSharpCodeProvider provider)
        {
            var modelCls = new CodeTypeDeclaration(oType.Name + "Model");

            foreach (PropertyInfo prop in oType.GetProperties())
            {
                if (prop.Name != "PrimaryKey"
                    && !prop.PropertyType.Name.StartsWith("Related")
                    && !prop.CustomAttributes.Any(a => a.AttributeType == typeof(XmlIgnoreAttribute)))
                {
                    var originalTypeReference = new CodeTypeReference(prop.PropertyType);
                    bool toList = typeof(IEnumerable).IsAssignableFrom(prop.PropertyType) && prop.PropertyType != typeof(string);
                    CodeTypeReference typeReference =
                        toList
                        ? new CodeTypeReference("List", originalTypeReference.TypeArguments[0])
                        : originalTypeReference;

                    string type = provider.GetTypeOutput(typeReference).Replace("BishBL.", "");

                    var newProp = new CodeSnippetTypeMember
                    {
                        Name = prop.Name,
                        Attributes = MemberAttributes.Public | MemberAttributes.Final,
                        Text = string.Concat(Enumerable.Repeat("    ", 2)) + string.Format("public {0} {1} {{ get; set; }}", type, prop.Name) + Environment.NewLine
                    };
                    newProp.UserData["toList"] = toList;

                    modelCls.Members.Add(newProp);
                }
            }

            return modelCls;
        }

        private static CodeTypeDeclaration GenerateExtensionClass(Type oType, CodeTypeDeclaration modelCls)
        {
            var extCls = new CodeTypeDeclaration("ViewModelExtensions")
            {
                IsClass = true,
                IsPartial = true,
                Attributes = MemberAttributes.Public | MemberAttributes.Static
            };
            var extMethod = new CodeMemberMethod
            {
                Name = "ToViewModel",
                Attributes = MemberAttributes.Public | MemberAttributes.Static,
                ReturnType = new CodeTypeReference(modelCls.Name)
            };
            extMethod.Parameters.Add(new CodeParameterDeclarationExpression
            {
                Name = "source",
                Type = new CodeTypeReference("this " + oType.Name.Replace("BishBL.", ""))
            });

            var retVar = new CodeVariableDeclarationStatement("var", "re")
            {
                InitExpression = new CodeObjectCreateExpression(modelCls.Name)
            };
            extMethod.Statements.Add(retVar);

            foreach (CodeSnippetTypeMember prop in modelCls.Members)
            {
                CodeExpression rightSide = new CodePropertyReferenceExpression(new CodeArgumentReferenceExpression("source"), prop.Name);
                if ((bool)prop.UserData["toList"])
                    rightSide = new CodeMethodInvokeExpression(rightSide, "ToList");
                extMethod.Statements.Add(new CodeAssignStatement(
                    new CodePropertyReferenceExpression(new CodeVariableReferenceExpression(retVar.Name), prop.Name),
                    rightSide));
            }

            extMethod.Statements.Add(new CodeMethodReturnStatement(new CodeVariableReferenceExpression(retVar.Name)));

            extCls.Members.Add(extMethod);
            return extCls;
        }
    }
}
