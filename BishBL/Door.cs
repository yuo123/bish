﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Represents one side of a door connecting two room
    /// </summary>
    public class Door : IPhysicalEntity
    {
        /// <summary>
        /// This door's unique ID.
        /// Two <see cref="Door"/> objects can have the same ID, because each object
        /// only represents one side of the door.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The door in the second room that this door is connected to
        /// </summary>
        public Door Connected { get; private set; }

        /// <summary>
        /// The room this side of the door is in.
        /// </summary>
        public RelatedRoom CurrentRoom { get; private set; }

        /// <summary>
        /// The X-Position of this side of the door in its room.
        /// </summary>
        public int XPos { get; private set; }

        /// <summary>
        /// The Y-Position of this side of the door in its room.
        /// </summary>
        public int YPos { get; private set; }

        /// <summary>
        /// The character which represents door entities in the game display.
        /// </summary>
        /// <seealso cref="IPhysicalEntity.DisplayCharacter"/>
        public char DisplayCharacter { get { return '+'; } }

        /// <summary>
        /// The string associated with door entities.
        /// </summary>
        /// <seealso cref="IPhysicalEntity.DisplayClass"/>
        public string DisplayClass { get { return "door"; } }

        // The direction of connection; 0 for first room and 1 for second
        private readonly int _conDirection;

        /// <summary>
        /// Creates a new <see cref="Door"/> in the given room and position,
        /// optionally connected to an existing door in another room.
        /// </summary>
        public Door(RelatedRoom room, int x, int y, Door connected = null)
        {
            this.CurrentRoom = room;
            this.XPos = x;
            this.YPos = y;
            this.Connected = connected;

            if (Connected == null)
            {
                _conDirection = 0;
                this.Id = RoomsDAL.CreateDoor(CurrentRoom.PrimaryKey, x, y);
            }
            else
            {
                //this door side is connected in the opposite direction from the other
                _conDirection = 1 - Connected._conDirection;
                connected.Connected = this;
                RoomsDAL.ConnectDoor(Connected.Id, CurrentRoom.PrimaryKey, x, y, _conDirection);
            }
        }

        /// <summary>
        /// Initializes a new <see cref="Door"/> from the given <see cref="DataRow"/>, using fields which belong
        /// to the specified connection direction (0 for firstRoom, 1 for secondRoom)
        /// </summary>
        private Door(DataRow row, int conDir)
        {
            this.Id = (int)row["doorId"];
            _conDirection = conDir;

            if (_conDirection == 0)
            {
                this.CurrentRoom = new RelatedRoom((int)row["firstRoomId"]);
                this.XPos = (int)row["firstXPos"];
                this.YPos = (int)row["firstYPos"];
            }
            else
            {
                this.CurrentRoom = new RelatedRoom((int)row["secondRoomId"]);
                this.XPos = (int)row["secondXPos"];
                this.YPos = (int)row["secondYPos"];
            }
        }

        /// <summary>
        /// Returns the position in the room in which a character coming out of the door will land.
        /// </summary>
        public Position GetLandingPosition()
        {
            return new Position(XPos.Bound(0, CurrentRoom.Value.Width - 1), YPos.Bound(0, CurrentRoom.Value.Length - 1));
        }

        /// <summary>
        /// Reloads this door's properties from the given DataTable, which contains them,
        /// and returns the row used.
        /// </summary>
        /// <returns>
        /// The <see cref="DataRow" /> used by the door to update itself, or <see langword="null" /> if it no longer exists in the table.
        /// </returns>
        public DataRow RefreshFromDataTable(DataTable table, RelatedRoom room)
        {
            DataRow row = table.Rows.Find(this.Id);
            if (row != null)
            {
                // Get the id of the room the OTHER side of this door is in
                int? connected = _conDirection == 0 ? row.Field<int?>("secondRoomId") : row.Field<int?>("firstRoomId");
                // Do anything only if there was a change
                if ((connected == null) != (this.Connected == null))
                {
                    // Update the Connected property
                    if (connected == null)
                        this.Connected = null;
                    else
                        this.Connected = new Door(row, 1 - _conDirection);
                }
            }
            return row;
        }

        /// <summary>
        /// Get all the doors in the given room.
        /// </summary>
        public static List<Door> GetDoorsInRoom(RelatedRoom room)
        {
            return RoomsDAL.GetDoorsInRoom(room.PrimaryKey).ToObjects(row =>
            {
                Door d0 = null; //door on firstRooms' side
                Door d1 = null; //door on secondRooms' side

                if (row["firstRoomId"] is int)
                    d0 = new Door(row, 0);
                if (row["secondRoomId"] is int)
                    d1 = new Door(row, 1);

                if (d0 != null && d1 != null)
                {
                    d0.Connected = d1;
                    d1.Connected = d0;
                }

                // Return the door side that's in the requested room
                return d0 != null && room.Equals(d0.CurrentRoom) ? d0 : d1;
            });
        }

        /// <summary>
        /// Returns an empty <see cref="IEnumerable{T}"/>, because doors do not have status classes.
        /// </summary>
        public IEnumerable<string> ConsumeStatusClasses()
        {
            // Currently, doors cannot have status classes.
            return Enumerable.Empty<string>();
        }
    }
}
