﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BishBL
{
    /// <summary>
    /// Provides miscellaneous extension methods for project-wide use.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Transforms each row of the DataTable into a BL object using a specified conversion function
        /// </summary>
        /// <typeparam name="T">The type of BL object to give as output</typeparam>
        internal static List<T> ToObjects<T>(this DataTable table, Func<DataRow, T> transform)
        {
            return table.AsEnumerable().Select(transform).ToList(table.Rows.Count);
        }

        /// <summary>
        /// Returns <paramref name="obj"/> if <paramref name="condition"/> is false, and <paramref name="default"/> otherwise
        /// </summary>
        public static T DefaultIf<T>(this T obj, Predicate<T> condition, T @default = default(T))
        {
            return condition(obj) ? @default : obj;
        }

        /// <summary>
        /// Returns <paramref name="default"/> if <paramref name="obj"/> is equal to <paramref name="value"/>, and <paramref name="obj"/> otherwise
        /// </summary>
        public static T DefaultIf<T>(this T obj, T value, T @default = default(T))
        {
            return obj.Equals(value) ? @default : obj;
        }

        /// <summary>
        /// Returns the integer value nearest to the given integer that is between <paramref name="min"/> and <paramref name="max"/>, inclusive.
        /// </summary>
        public static int Bound(this int val, int min, int max)
        {
            if (val <= min)
                return min;
            if (val >= max)
                return max;

            return val;
        }

        /// <summary>
        /// Evaluates the given function for the given value only if it does not equal to the default input value.
        /// Otherwise, returns the default output value.
        /// </summary>
        /// <param name="value">The value that should be given to <paramref name="func"/></param>
        /// <param name="func">A function delegate which will be invoked with the <paramref name="value"/> parameter to obtain the result value.</param>
        /// <param name="defaultIn">
        /// The default value to compare <paramref name="value"/> against.
        /// If they are equal, <paramref name="func"/> will not be invoked,
        /// </param>
        /// <param name="defaultOut">
        /// The default value to return in case <paramref name="value"/> is equal to <paramref name="defaultIn"/>.
        /// </param>
        /// <returns></returns>
        public static TOut IfNotDefault<TIn, TOut>(this TIn value, Func<TIn, TOut> func, TIn defaultIn = default(TIn), TOut defaultOut = default(TOut))
        {
            return !value.Equals(defaultIn) ? func(value) : defaultOut;
        }

        /// <summary>
        /// Returns a random element of an <see cref="IList{T}"/> using the given <see cref="System.Random"/> instance.
        /// </summary>
        public static T Random<T>(this IList<T> source, Random rnd)
        {
            if (rnd == null)
                throw new ArgumentNullException("rnd");
            if (source.Count == 0)
                throw new ArgumentOutOfRangeException("source", "Cannot return a random value out of an empty list.");

            return source[rnd.Next(source.Count)];
        }

        /// <summary>
        /// Sums the elements of <paramref name="source"/> by using a function to extract an integer from each element.
        /// </summary>
        public static int Sum<T>(this IEnumerable source, Func<T, int> selector)
        {
            return source.Cast<T>().Sum(selector);
        }

        /// <summary>
        /// Returns a random <see cref="double"/> distributed around zero according to the Cauchy distribution.
        /// </summary>
        /// <remarks>
        /// See https://en.wikipedia.org/wiki/Cauchy_distribution
        /// </remarks>
        public static double NextCauchyDouble(this Random random)
        {
            return Math.Tan(Math.PI * (random.NextDouble() - 0.5));
        }

        /// <summary>
        /// Returns a random <see cref="double"/> distributed around the specified mean according to the Cauchy distribution.
        /// </summary>
        public static double NextCauchyDouble(this Random random, double mean, double scale = 1.0)
        {
            return scale * random.NextCauchyDouble() + mean;
        }

        /// <summary>
        /// Returns a random <see cref="int"/> distributed around the specified mean according to the Cauchy distribution,
        /// and rounded to the nearest integral value.
        /// </summary>
        public static int NextCauchy(this Random random, double mean, double scale)
        {
            return (int)Math.Round(random.NextCauchyDouble(mean, scale));
        }

        internal static int Sum(this DataTable dt, string field)
        {
            return dt.Rows.Sum<DataRow>(row => (int)row[field]);
        }

        /// <summary>
        /// Copies the elements of an <see cref="IEnumerable{T}"/> to a <see cref="List{T}"/> when the number of elements is known.
        /// </summary>
        public static List<T> ToList<T>(this IEnumerable<T> source, int count)
        {
            var re = new List<T>(count);
            foreach (T item in source)
                re.Add(item);
            return re;
        }

        /// <summary>
        /// Copies the elements of an <see cref="IEnumerable{T}"/> to an array when the number of elements is known.
        /// </summary>
        public static T[] ToArray<T>(this IEnumerable<T> source, int count)
        {
            var re = new T[count];
            int i = 0;
            foreach (T item in source)
                re[i++] = item;
            return re;
        }

        /// <summary>
        /// Wraps the <see cref="IDeletable"/> <typeparamref name="T"/> in a <see cref="TemporaryObject{T}"/>, usually to be used in a <see langword="using"/> statement
        /// </summary>
        internal static TemporaryObject<T> AsTemporary<T>(this T original) where T : IDeletable
        {
            return new TemporaryObject<T>(original);
        }

        /// <summary>
        /// Calculates a person's age based on their date of birth.
        /// </summary>
        /// <remarks>
        /// The calculation is rather crude, and does not account for leap years.
        /// </remarks>
        public static int CalcAge(DateTime dob)
        {
            return (int)(DateTime.UtcNow - dob).TotalDays / 365;
        }

        #region DateTime and TimeSpan sums and averages
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

        public static TimeSpan Sum(this IEnumerable<TimeSpan> source)
        {
            return new TimeSpan(source.Sum(t => t.Ticks));
        }

        public static TimeSpan Sum<T>(this IEnumerable<T> source, Func<T, TimeSpan> selector)
        {
            // A sum of TimeSpans is a TimeSpan whose ticks are the sum of all the terms' ticks.
            return source.Select(selector).Sum();
        }

        public static TimeSpan Average(this ICollection<TimeSpan> source)
        {
            return source.Average(source.Count);
        }

        public static TimeSpan Average<T>(this ICollection<T> source, Func<T, TimeSpan> selector)
        {
            return source.Select(selector).Average(source.Count);
        }

        public static TimeSpan Average(this IEnumerable<TimeSpan> source, int count)
        {
            return source.Sum().Divide(count);
        }

        public static TimeSpan Average<T>(this IEnumerable<T> source, Func<T, TimeSpan> selector, int count)
        {
            return source.Sum(selector).Divide(count);
        }

        public static DateTime Average(this ICollection<DateTime> source)
        {
            return source.Average(source.Count);
        }

        public static DateTime Average(this IEnumerable<DateTime> source, int count)
        {
            return new DateTime(source.Sum(d => d.Ticks) / count);
        }

        public static DateTime Average<T>(this ICollection<T> source, Func<T, DateTime> selector)
        {
            return source.Select(selector).Average(source.Count);
        }

        public static DateTime Average<T>(this IEnumerable<T> source, Func<T, DateTime> selector, int count)
        {
            return source.Select(selector).Average(count);
        }

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
        #endregion

        /// <summary>
        /// Returns a <see cref="TimeSpan"/> whose length is the length of <paramref name="t"/> divided by <paramref name="divisor"/>.
        /// </summary>
        public static TimeSpan Divide(this TimeSpan t, int divisor)
        {
            return new TimeSpan(t.Ticks / divisor);
        }

        /// <summary>
        /// Converts a <see cref="DateTime"/> to a <see cref="TimeSpan"/> by subtracting from it the date <c>DateTime.FromOADate(0)</c>
        /// </summary>
        /// <param name="dateTime"></param>
        public static TimeSpan ToTimeSpan(this DateTime dateTime)
        {
            return dateTime - DateTime.FromOADate(0);
        }

        /// <summary>
        /// Converts a <see cref="TimeSpan"/> to a <see cref="DateTime"/> by adding to it the date <c>DateTime.FromOADate(0)</c>
        /// </summary>
        /// <param name="timeSpan"></param>
        public static DateTime ToDateTime(this TimeSpan timeSpan)
        {
            return DateTime.FromOADate(0) + timeSpan;
        }

        /// <summary>
        /// Returns the output of <c>source.Average(selector)</c>, or <c>0.0</c> if there are no elements in <paramref name="source"/>.
        /// </summary>
        public static double AverageOrZero<T>(this IEnumerable<T> source, Func<T, int> selector)
        {
            return source.Any() ? source.Average(selector) : 0.0;
        }

        /// <summary>
        /// Returns the output of <c>source.Average(selector)</c>, or <see cref="TimeSpan.Zero"/> if there are no elements in <paramref name="source"/>.
        /// </summary>
        public static TimeSpan AverageOrZero<T>(this ICollection<T> source, Func<T, TimeSpan> selector)
        {
            return source.Count > 0 ? source.Average(selector) : TimeSpan.Zero;
        }
    }
}
