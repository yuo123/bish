﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Specifies an admin's permission level, determining the administrative actions available to them.
    /// </summary>
    public enum AdminPermissionLevel
    {
        /// <summary>
        /// Specifies that the administrator can perform only normal administrative actions.
        /// </summary>
        Regular = 0,

        /// <summary>
        /// Specifies that the administrator can also manage other administrators.
        /// </summary>
        SuperAdmin = 1
    }

    /// <summary>
    /// Represents an administrator user
    /// </summary>
    public class Admin
    {
        /// <summary>
        /// The admin's username.
        /// </summary>
        public string Username
        {
            get { return _username; }
            set
            {
                AdminsDAL.ChangeUsername(_username, value);
                _username = value;
            }
        }

        private AdminPermissionLevel _level;
        private string _username;

        /// <summary>
        /// The permission of the administrator, determining if they can create more administrators.
        /// </summary>
        public AdminPermissionLevel PermissionLevel
        {
            get { return _level; }
            set
            {
                _level = value;
                AdminsDAL.SetPermissionLevel(this.Username, (int)_level);
            }
        }

        private Admin(string username, AdminPermissionLevel permissionLevel)
        {
            this.Username = username;
            this.PermissionLevel = permissionLevel;
        }

        /// <summary>
        /// Creates a new admin with the given parameters.
        /// </summary>
        /// <param name="username">The admin's username</param>
        /// <param name="password">The admin's password</param>
        /// <param name="permissionLevel">The admins permission level: 0 for regular, 1 for SuperAdmin.</param>
        public Admin(string username, string password, AdminPermissionLevel permissionLevel)
        {
            this.Username = username;
            this._level = permissionLevel;

            AdminsDAL.CreateAdmin(username, password, (int)permissionLevel);
        }

        /// <summary>
        /// Returns an <see cref="Admin"/> object with the specified login credentials, or <see langword="null"/> if such an admin doesn't exist.
        /// </summary>
        public static Admin FromLogin(string username, string password)
        {
            if (AdminsDAL.DoesAdminExist(username, password))
                return FromDAL(username);
            else
                return null;
        }

        /// <summary>
        /// Initializes a new <see cref="Admin"/> object from an existing admin in the database.
        /// </summary>
        public static Admin FromDAL(string username)
        {
            return new Admin(username, (AdminPermissionLevel)AdminsDAL.GetPermissionLevel(username));
        }

        /// <summary>
        /// Returns whether or not an admin with the given username exists.
        /// </summary>
        public static bool IsUsernameTaken(string username)
        {
            return AdminsDAL.DoesAdminExist(username);
        }

        /// <summary>
        /// Returns a newly-created <see cref="Admin"/> and inserts it to the database.
        /// </summary>
        /// <returns></returns>
        public static Admin CreateAdmin(string username, string password, AdminPermissionLevel permissionLevel)
        {
            AdminsDAL.CreateAdmin(username, password, (int)permissionLevel);
            return new Admin(username, permissionLevel);
        }

        /// <summary>
        /// Returns a list of all admins that exist.
        /// </summary>
        public static List<Admin> GetAllAdmins()
        {
            return AdminsDAL.GetAllAdmins().ToObjects<Admin>(row => new Admin((string)row["adminUsername"], (AdminPermissionLevel)(int)row["permissionLevel"]));
        }

        /// <summary>
        /// Changes this admin's password.
        /// </summary>
        /// <param name="password">The new password to be given to this admin</param>
        public void ChangePassword(string password)
        {
            AdminsDAL.ChangePassword(this.Username, password);
        }

        /// <summary>
        /// Deletes this admin completely.
        /// </summary>
        public void Delete()
        {
            AdminsDAL.DeleteAdmin(this.Username);
        }
    }
}
