﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Represents a single autonomous enemy in the game.
    /// </summary>
    public class Enemy : Character, IDeletable, IRelated<Enemy>
    {
        /// <summary>
        /// This enemy's unique ID
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// This enemy's unique ID
        /// </summary>
        protected override object PrimaryKey { get { return Id; } }

        /// <summary>
        /// This enemy's type.
        /// </summary>
        /// <seealso cref="BishBL.EnemyType"/>
        public RelatedEnemyType EnemyType { get; private set; }

        /// <summary>
        /// The character which represents the type of this enemy in the game display.
        /// </summary>
        /// <seealso cref="P:BishBL.IPhysicalEntity.DisplayCharacter" />
        public override char DisplayCharacter { get { return this.EnemyType.Value.DisplayCharacter; } }

        /// <summary>
        /// The string associated enemy entities.
        /// </summary>
        /// <seealso cref="P:BishBL.IPhysicalEntity.DisplayClass" />
        public override string DisplayClass { get { return "enemy"; } }

        Enemy IRelated<Enemy>.Value { get { return this; } }

        /// <summary>
        /// Executes position changes in the database.
        /// </summary>
        public override void CommitMovement()
        {
            PhysicalEnemiesDAL.MoveEnemy(this.Id, this.XPos, this.YPos);
        }

        // Existing enemies can only be instantiated by calling GetAllEnemies()
        private Enemy(DataRow dr, RelatedRoom room)
        {
            InitFromDataRow(dr, room);
        }

        /// <summary>
        /// Creates a new <see cref="Enemy"/> with the given type and position
        /// </summary>
        public Enemy(RelatedEnemyType type, RelatedRoom room, int x, int y)
        {
            this.Id = PhysicalEnemiesDAL.AddPhysicalEnemy(type.PrimaryKey, room.PrimaryKey, x, y);
            this.EnemyType = type;
            this.BaseAttack = type.Value.Attack;
            this.BaseDefense = type.Value.Defense;
            this.Health = type.Value.StartingHealth;
            this.Level = type.Value.Level;

            this.CurrentRoom = room;
            this.XPos = x;
            this.YPos = y;
        }

        /// <summary>
        /// Returns this enemy as a <see cref="RelatedEnemy"/>.
        /// </summary>
        public RelatedEnemy AsRelated()
        {
            return new RelatedEnemy(this);
        }

        /// <summary>
        /// Moves the specified item from the floor to this enemy's inventory.
        /// </summary>
        /// <param name="item">The item to pick up</param>
        public override void PickUp(Item item)
        {
            this.AddItem(item);
            ItemsDAL.PickupItem(item.Id, this.Id);
        }

        /// <summary>
        /// Retrieves a list of all items on this enemy from the database.
        /// </summary>
        protected override List<Item> GetItems()
        {
            return Item.GetItemsOnEnemy(this);
        }

        /// <summary>
        /// Assigns the data from the given DataRow to this enemy's properties.
        /// </summary>
        /// <param name="dr">
        /// A <see cref="DataRow" /> containing at least the following fields: attack, defense, health, level, xPos, yPos, roomId
        /// </param>
        /// <param name="room">The room this enemy is in</param>
        /// <param name="initial">
        /// <see langword="true" /> if the enemy is first loaded from the database, <see langword="false" /> if it is only being refreshed.
        /// </param>
        protected override void InitFromDataRow(DataRow dr, RelatedRoom room, bool initial = true)
        {
            if (initial)
            {
                this.Id = (int)dr["physicalEnemyId"];
                this.EnemyType = new RelatedEnemyType((int)dr["EnemyTypesTBL.enemyTypeId"]);
            }

            //assign properties inherited from Character
            base.InitFromDataRow(dr, room, initial);
        }

        /// <summary>
        /// Called when this enemy is killed.
        /// </summary>
        protected override void OnKilled(Character killer)
        {
            base.OnKilled(killer);
            PhysicalEnemiesDAL.DeletePhysicalEnemy(this.Id);
            CurrentRoom = null;
        }

        /// <summary>
        /// Gets a list of all enemies in a given room
        /// </summary>
        public static List<Enemy> GetAllEnemies(RelatedRoom room)
        {
            return PhysicalEnemiesDAL.GetFullPhysicalEnemies(room.PrimaryKey).ToObjects(row => new Enemy(row, room));
        }

        /// <summary>
        /// Applies the specified amount of damage to this enemy.
        /// </summary>
        protected override void ReceiveDamage(int amount)
        {
            base.ReceiveDamage(amount);
            this.Health -= amount;
            PhysicalEnemiesDAL.ChangeHealth(this.Id, -amount);
        }

        internal static IEnumerable<Enemy> FromDataRows(IEnumerable<DataRow> rows, RelatedRoom room)
        {
            if (rows.Any())
                System.Diagnostics.Debugger.Break();

            return rows.Select(row => new Enemy(row, room));
        }

        void IDeletable.Delete()
        {
            PhysicalEnemiesDAL.DeletePhysicalEnemy(this.Id);
        }

        /// <summary>
        /// Moves or attacks nearby players according to simple rules.
        /// </summary>
        public void MakeMove()
        {
            Player player = FindAdjacentPlayer();
            if (player != null)
                this.Attack(player);
            else
                MoveRandomly();
        }

        private void MoveRandomly()
        {
            int minX = XPos > 0 ? -1 : 0;
            int maxX = XPos < CurrentRoom.Value.Width ? 1 : 0;
            int minY = YPos > 0 ? -1 : 0;
            int maxY = YPos < CurrentRoom.Value.Length ? 1 : 0;

            this.TryMove(Room._rnd.Next(minX, maxX + 1), Room._rnd.Next(minY, maxY + 1));
        }

        private Player FindAdjacentPlayer()
        {
            for (int dx = -1; dx <= 1; dx++)
                for (int dy = -1; dy <= 1; dy++)
                {
                    var player = this.CurrentRoom.Value.GetEntityAt(this.XPos + dx, this.YPos + dy) as Player;
                    if (player != null)
                        return player;
                }

            return null;
        }

        public override bool Equals(object obj)
        {
            var enemy = obj as Enemy;
            return enemy != null
                   && Id == enemy.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }
    }

    /// <summary>
    /// Represents an enemy object which can be lazily initialized.
    /// </summary>
    public class RelatedEnemy : RelatedObject<Enemy, int>, IDeletable
    {
        /// <summary>
        /// Initializes a new <see cref="RelatedEnemy"/> for an already-loaded enemy.
        /// </summary>
        /// <param name="enemy">An existing enemy object to wrap</param>
        public RelatedEnemy(Enemy enemy) : base(enemy.Id, enemy) { }

        void IDeletable.Delete()
        {
            ((IDeletable)this.Value).Delete();
        }
    }
}
