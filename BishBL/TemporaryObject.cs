﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BishBL
{
    /// <summary>
    /// Holds a data-backed object that can be disposed after use
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class TemporaryObject<T> : IDisposable where T : IDeletable
    {
        public bool Disposed { get; private set; }

        public T Original { get; set; }

        public TemporaryObject(T original)
        {
            this.Disposed = false;
            this.Original = original;
        }

        public void Dispose()
        {
            System.Diagnostics.Debug.WriteLine("disposing TemporaryObject");
            if (!Disposed)
            {
                this.Disposed = true;
                if (Original != null)
                    Original.Delete();
            }
        }

        public static implicit operator T(TemporaryObject<T> t)
        {
            return t.Original;
        }

        public static implicit operator TemporaryObject<T>(T o)
        {
            return new TemporaryObject<T>(o);
        }
    }

    /// <summary>
    /// Defines a data-backed object that can be deleted
    /// </summary>
    internal interface IDeletable
    {
        void Delete();
    }
}
