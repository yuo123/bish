﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

// Comments for GetHashCode() and Equals()
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.Room.GetHashCode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.Room.Equals(System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.RelatedObject`2.GetHashCode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.Player.GetHashCode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.Player.Equals(System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.Game.GetHashCode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.Game.Equals(System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.EnemyType.GetHashCode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.EnemyType.Equals(System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.Enemy.GetHashCode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "member", Target = "~M:BishBL.Enemy.Equals(System.Object)")]

// Other reasons
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CS1591", Scope = "type", Target = "~T:BishBL.RandomNameGenerator")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "RCS1170:Use read-only auto-implemented property.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "RCS1015:Use nameof operator.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "RCS1206:Use conditional access instead of conditional expression.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Redundancy", "RCS1163:Unused parameter.", Justification = "Parameter required by delegate type.", Scope = "member", Target = "~M:BishBL.RelatedObject`2.#ctor(`1,`0)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "RCS1165:Unconstrained type parameter checked for null.", Justification = "Only a null value can cause an exception.", Scope = "member", Target = "~M:BishBL.TemporaryObject`1.Dispose")]

