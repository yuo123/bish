﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BishBL
{
    public static class RandomNameGenerator
    {
        public const int DEFAULT_MIN_SYLL = 1;
        public const int DEFAULT_MAX_SYLL = 3;

        public static readonly string[] VOWELS = { "a", "o", "e", "u", "i" };
        public static readonly string[] CONSONANTS = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z" };
        public static readonly string[] PLOSIVES = { "p", "b", "t", "d", "k", "x", "c", "q", "ck", "g" };
        public static readonly string[] FRICATIVES = { "f", "ph", "v", "th", "s", "ss", "z", "s", "sh", "h" };
        public static readonly string[] AFFRICATES = { "ch", "j", "g" };
        public static readonly string[] NASALS = { "m", "n" };
        public static readonly string[] APPROXIMANTS = { "l", "r" };
        public static readonly string[] OTHER = { "y", "w", };

        public static string GetConsonant(Random rnd)
        {
            return CONSONANTS[rnd.Next(0, CONSONANTS.Length)];
        }

        public static string GetConsonant(Random rnd, ref string[] exclude)
        {
            return CONSONANTS.Except(exclude).ToArray()[rnd.Next(0, CONSONANTS.Length - exclude.Length)];
        }

        public static string GetVowel(Random rnd)
        {
            return VOWELS[rnd.Next(0, VOWELS.Length)];
        }

        public static string GetCVC(Random rnd, ref string[] prev)
        {
            if (prev == null)
                return GetCVC(rnd);
            return GetConsonant(rnd, ref prev) + GetVowel(rnd) + GetConsonant(rnd);
        }

        public static string GetCVC(Random rnd)
        {
            return GetConsonant(rnd) + GetVowel(rnd) + GetConsonant(rnd);
        }

        public static string GetCV(Random rnd, ref string[] prev)
        {
            if (prev == null)
                return GetCV(rnd);
            return GetConsonant(rnd, ref prev) + GetVowel(rnd);
        }

        public static string GetCV(Random rnd)
        {
            return GetConsonant(rnd) + GetVowel(rnd);
        }

        public static string GetVC(Random rnd)
        {
            return GetVowel(rnd) + GetConsonant(rnd);
        }

        public static string GetName(Random rnd, int maxSyll = DEFAULT_MAX_SYLL, int minSyll = DEFAULT_MIN_SYLL)
        {
            int sylls = rnd.Next(minSyll, maxSyll + 1);
            string name = "";
            string[] last = null;
            for (int i = 0; i < sylls; i++)
            {
                switch (rnd.Next(1, 4))
                {
                    case 1:
                        name += GetCVC(rnd, ref last);
                        break;
                    case 2:
                        name += GetCV(rnd, ref last);
                        break;
                    case 3:
                        name += GetVC(rnd);
                        break;
                }
                last = GetCategory(name);
            }

            return char.ToUpper(name[0]) + name.Substring(1);
        }

        public static string GetNameUnfiltered(Random rnd, int maxSyll = DEFAULT_MAX_SYLL, int minSyll = DEFAULT_MIN_SYLL)
        {
            int sylls = rnd.Next(minSyll, maxSyll + 1);
            string name = "";
            for (int i = 0; i < sylls; i++)
            {
                switch (rnd.Next(1, 4))
                {
                    case 1:
                        name += GetCVC(rnd);
                        break;
                    case 2:
                        name += GetCV(rnd);
                        break;
                    case 3:
                        name += GetVC(rnd);
                        break;
                }
            }

            return char.ToUpper(name[0]) + name.Substring(1);
        }

        private static string[] GetCategory(string name)
        {
            if (PLOSIVES.Contains(name[0].ToString()))
                return PLOSIVES;
            if (FRICATIVES.Contains(name[0].ToString()))
                return FRICATIVES;
            if (AFFRICATES.Contains(name[0].ToString()))
                return AFFRICATES;
            if (NASALS.Contains(name[0].ToString()))
                return NASALS;
            if (APPROXIMANTS.Contains(name[0].ToString()))
                return APPROXIMANTS;
            if (OTHER.Contains(name[0].ToString()))
                return OTHER;
            return new string[0];
        }
    }
}
