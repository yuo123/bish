﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BishBL
{
    /// <summary>
    /// Represents an entity (object, character, etc.) that can be present in a room
    /// </summary>
    public interface IPhysicalEntity
    {
        /// <summary>
        /// The room this entity is currently in. If null, means that the entity is not physically present anywhere.
        /// </summary>
        RelatedRoom CurrentRoom { get; }

        /// <summary>
        /// The x-position of the entity in the room.
        /// </summary>
        int XPos { get; }

        /// <summary>
        /// The y-position of the entity in the room.
        /// </summary>
        int YPos { get; }

        /// <summary>
        /// The character which represents this entity in the game display.
        /// </summary>
        char DisplayCharacter { get; }

        /// <summary>
        /// The string associated with the type of this entity.
        /// Different display classes are rendered with different but consistent visual styles (e.g. colors, font, etc.)
        /// </summary>
        string DisplayClass { get; }

        /// <summary>
        /// Returns a collection of strings describing the current status of the entity, for rendering purposes.
        /// Calling this method consumes the status, changing the return value for subsequent calls.
        /// </summary>
        IEnumerable<string> ConsumeStatusClasses();

        /// <summary>
        /// <para>
        /// Reloads this entity's properties from the given DataTable, which contains them,
        /// and returns the row used.
        /// </para>
        /// <para>
        /// The entity is only expected to update properties which typically change during a game.
        /// </para>
        /// </summary>
        /// <returns>
        /// The <see cref="DataRow"/> used by the entity to update itself, or <see langword="null"/> if it no longer exists in the table.
        /// </returns>
        DataRow RefreshFromDataTable(DataTable table, RelatedRoom room);
    }

    /// <summary>
    /// Represents an <see cref="IPhysicalEntity"/> that can move
    /// </summary>
    public interface IMovableEntity : IPhysicalEntity
    {
        /// <summary>
        /// The x-position of the entity in the room.
        /// </summary>
        new int XPos { get; set; }

        /// <summary>
        /// The y-position of the entity in the room.
        /// </summary>
        new int YPos { get; set; }

        /// <summary>
        /// Executes position changes in the database
        /// </summary>
        void Commit();
    }
}
