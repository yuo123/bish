﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Represents a room in the game where entities can exist.
    /// </summary>
    public class Room
    {
        internal static readonly Random _rnd = new Random();

        /// <summary>
        /// The room's ID
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// The room's width (x-direction), in cells
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// The room's length (y-direction), in cells
        /// </summary>
        public int Length { get; private set; }

        /// <summary>
        /// The room's difficulty level, determining the level of enemies and items in the room
        /// </summary>
        public int Difficulty { get; private set; }

        /// <summary>
        /// The room's floor area, in cells
        /// </summary>
        public int Area { get { return Width * Length; } }

        /// <summary>
        /// Gets all the physical entities in this room
        /// </summary>
        public RelatedPhysicalEntitiesList Entities { get; private set; }

        private Room(DataRow dr)
        {
            InitFromDataRow(dr);
            this.Entities = new RelatedPhysicalEntitiesList(this.AsRelated());
        }

        /// <summary>
        /// Creates a new room with the specified width, length and difficulty, and adds it to the database
        /// </summary>
        private Room(int width, int length, int difficulty)
        {
            this.Id = RoomsDAL.CreateRoom(width, length, difficulty);

            this.Width = width;
            this.Length = length;
            this.Difficulty = difficulty;

            this.Entities = new RelatedPhysicalEntitiesList(this.AsRelated());
        }

        /// <summary>
        /// Causes the entity list to be reloaded next time it is accessed.
        /// </summary>
        public void RefreshEntities()
        {
            if (this.Entities.IsValueCreated)
            {
                DataTable playersTable = PlayersDAL.GetPlayersInRoom(this.Id);
                DataTable enemiesTable = PhysicalEnemiesDAL.GetFullPhysicalEnemies(this.Id);
                DataTable itemsTable = ItemsDAL.GetItemsInRoom(this.Id);
                DataTable doorsTable = RoomsDAL.GetDoorsInRoom(this.Id);
                RelatedRoom related = this.AsRelated();
                var used = new HashSet<DataRow>();

                // for and not foreach because elements may need to be removed
                for (int i = 0; i < this.Entities.Value.Count; i++)
                {
                    IPhysicalEntity entity = this.Entities.Value[i];
                    DataTable table = null;
                    if (entity is Player)
                        table = playersTable;
                    else if (entity is Enemy)
                        table = enemiesTable;
                    else if (entity is Item)
                        table = itemsTable;
                    else if (entity is Door)
                        table = doorsTable;

                    if (table != null)
                    {
                        // Refresh the entity and remove it from the list if it was deleted
                        DataRow usedRow = entity.RefreshFromDataTable(table, related);
                        if (usedRow == null)
                        {
                            Entities.Value.RemoveAt(i);
                            i--;
                        }
                        else
                        {
                            used.Add(usedRow);
                        }
                    }
                }

                Entities.Value.AddRange(Player.FromDataRows(playersTable.AsEnumerable().Except(used), related));
                Entities.Value.AddRange(Enemy.FromDataRows(enemiesTable.AsEnumerable().Except(used), related));
                Entities.Value.AddRange(Item.FromDataRows(itemsTable.AsEnumerable().Except(used), related));
            }
        }

        /// <summary>
        /// Cause all the enemies in the room to make their moves.
        /// </summary>
        public void MoveEnemies()
        {
            foreach (Enemy enemy in this.Entities.Value.OfType<Enemy>())
            {
                enemy.MakeMove();
                enemy.CommitMovement();
            }
        }

        /// <summary>
        /// Gets the physical entity at the specified position in this room, or null if that space is empty
        /// </summary>
        public IPhysicalEntity GetEntityAt(int x, int y)
        {
            return this.Entities.Value.Find(ent => ent.XPos == x && ent.YPos == y);
        }

        /// <summary>
        /// Generates a new room with the specified difficulty, optionally with a door to connect to, and adds it to the database.
        /// </summary>
        public static Room Generate(int difficulty, Door connection = null)
        {
            const int MIN_SIZE = 4;
            const int MAX_SIZE = 15;
            Room re = new Room(_rnd.Next(MIN_SIZE, MAX_SIZE + 1), _rnd.Next(MIN_SIZE, MAX_SIZE + 1), difficulty);

            RelatedRoom related = re.AsRelated();
            re.GenerateDoors(connection);
            re.GenerateEnemies();
            re.GenerateItems();

            return re;
        }

        private void GenerateItems()
        {
            const double MIN_ITEM_DENSITY = 0.05;
            const double MAX_ITEM_DENSITY = 0.1;

            int itemCount = _rnd.Next((int)(Area * MIN_ITEM_DENSITY), (int)(Area * MAX_ITEM_DENSITY));

            foreach (Position pos in GetRandomPositions(itemCount))
                Item.Generate(this, pos, _rnd);
        }

        private void GenerateEnemies()
        {
            const double MIN_ENEMY_DENSITY = 0.05;
            const double MAX_ENEMY_DENSITY = 0.1;

            int enemyCount = _rnd.Next((int)(Area * MIN_ENEMY_DENSITY), (int)(Area * MAX_ENEMY_DENSITY));

            List<EnemyType> enemyTypes = EnemyType.GetAllEnemyTypes(this.Difficulty);
            RelatedRoom room = this.AsRelated();

            foreach (Position pos in GetRandomPositions(enemyCount))
                new Enemy(enemyTypes.Random(_rnd).AsRelated(), room, pos.X, pos.Y);
        }

        private void GenerateDoors(Door connection)
        {
            RelatedRoom related = this.AsRelated();
            Door con;

            con = connection.DefaultIf(c => c == null || c.XPos != c.CurrentRoom.Value.Width);
            new Door(related, -1, RandomY(), con);

            con = connection.DefaultIf(c => c == null || c.XPos != -1);
            new Door(related, this.Width, RandomY(), con);

            con = connection.DefaultIf(c => c == null || c.YPos != c.CurrentRoom.Value.Length);
            new Door(related, RandomX(), -1, con);

            con = connection.DefaultIf(c => c == null || c.YPos != -1);
            new Door(related, RandomX(), this.Length, con);
        }

        /// <summary>
        /// Returns a sequence of random, unique positions in the room.
        /// </summary>
        /// <param name="count">The number of positions to generate.</param>
        protected IEnumerable<Position> GetRandomPositions(int count)
        {
            var taken = new HashSet<Position>();
            for (int i = 0; i < count; i++)
            {
                Position pos;
                do
                    pos = new Position(RandomX(), RandomY());
                while (taken.Contains(pos));

                taken.Add(pos);
                yield return pos;
            }
        }

        // RandomX() and RandomY() never return positions adjacent to walls, 
        // to prevent things colliding with an incoming player.
        private int RandomX()
        {
            return _rnd.Next(1, this.Width - 1);
        }

        private int RandomY()
        {
            return _rnd.Next(1, this.Length - 1);
        }

        /// <summary>
        /// Returns a room with difficulty 1, either existing or generated
        /// </summary>
        public static Room GetStartingRoom()
        {
            DataRow roomData = RoomsDAL.GetStartingRoom();
            if (roomData == null)
                return Generate(1);
            else
                return new Room(roomData);
        }

        private void InitFromDataRow(DataRow dr)
        {
            this.Id = (int)dr["roomId"];
            this.Width = (int)dr["width"];
            this.Length = (int)dr["length"];
            this.Difficulty = (int)dr["difficulty"];
        }

        /// <summary>
        /// Tries to retrieve a room with the given id from the data layer and build an object from it.
        /// If no such room was found, returns null.
        /// </summary>
        public static Room FromDAL(int id)
        {
            DataRow dr = RoomsDAL.GetRoom(id);
            if (dr == null)
                return null;

            return new Room(dr);
        }

        /// <summary>
        /// Deletes all the empty (from players) rooms, with their enemies and items
        /// </summary>
        public static void DeleteEmptyRooms()
        {
            DataTable dt = RoomsDAL.GetEmptyRoomsIds();
            foreach (DataRow dr in dt.Rows)
                RoomsDAL.DeleteRoom((int)dr["roomId"]);

            RoomsDAL.RemoveDisconnectedDoors();
        }

        /// <summary>
        /// Returns all the entities in the room with the given id. 
        /// This method is private -- entities should be accessed via <see cref="Entities"/>
        /// </summary>
        private static List<IPhysicalEntity> GetAllEntities(RelatedRoom room)
        {
            return Player.GetAllPlayers(room)
                .Concat<IPhysicalEntity>(Enemy.GetAllEnemies(room))
                .Concat(Item.GetItemsInRoom(room))
                .Concat(Door.GetDoorsInRoom(room))
                .ToList();
        }

        /// <summary>
        /// Returns this room as a <see cref="RelatedRoom"/>.
        /// </summary>
        public RelatedRoom AsRelated()
        {
            return new RelatedRoom(this);
        }

        public override bool Equals(object obj)
        {
            var room = obj as Room;
            return room != null && Id == room.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }

        /// <summary>
        /// The list of entities in a room, only loaded when accessed.
        /// </summary>
        public class RelatedPhysicalEntitiesList : RelatedObject<List<IPhysicalEntity>, RelatedRoom>
        {
            /// <summary>
            /// Initializes a new <see cref="RelatedPhysicalEntitiesList"/> for the given room.
            /// </summary>
            /// <param name="room">The room to create the list for</param>
            public RelatedPhysicalEntitiesList(RelatedRoom room) : base(room, Room.GetAllEntities) { }

            /// <summary>
            /// Returns a dictionary which provides a mapping between a position in the room and the entity in that position.
            /// The returned dictionary does not contain entries for empty positions.
            /// </summary>
            public Dictionary<Position, IPhysicalEntity> GetPositionMapping()
            {
                try
                {
                    return this.Value.ToDictionary(ent => new Position(ent.XPos, ent.YPos));
                }
                catch (ArgumentException ex)
                {
                    throw new InvalidOperationException("More than one entity cannot occupy the same space.", ex);
                }
            }
        }
    }

    /// <summary>
    /// Represents a room object which can be lazily initialized.
    /// </summary>
    public class RelatedRoom : RelatedObject<Room, int>
    {
        /// <summary>
        /// Initializes a new <see cref="RelatedRoom"/> for the room with the given id.
        /// </summary>
        public RelatedRoom(int id) : base(id, Room.FromDAL) { }

        /// <summary>
        /// Initializes a new <see cref="RelatedRoom"/> for an already-loaded room.
        /// </summary>
        /// <param name="room">An existing room object to wrap</param>
        public RelatedRoom(Room room) : base(room.Id, room) { }
    }

    /// <summary>
    /// Represents an XY position in a room.
    /// </summary>
    public struct Position
    {
        private readonly int x;
        private readonly int y;

        /// <summary>
        /// The X-coordinate of this position
        /// </summary>
        public int X { get { return x; } }

        /// <summary>
        /// The Y-coordinate of this position
        /// </summary>
        public int Y { get { return y; } }

        /// <summary>
        /// Initializes a new <see cref="Position"/> with the given X and Y values.
        /// </summary>
        public Position(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>Returns the string representation of this point.</summary>
        public override string ToString()
        {
            return string.Format("({0}, {1})", X, Y);
        }
    }
}
