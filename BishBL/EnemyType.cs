﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Represents a type of enemy. <see cref="EnemyType"/>s are not represented in the game world,
    /// but rather determine the characteristics of <see cref="Enemy"/> instances.
    /// </summary>
    public class EnemyType : IDeletable
    {
        /// <summary>
        /// The enemy-type's unique ID.
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// The enemy-type's name.
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// The enemy-type's level, determining in which rooms it will spawn.
        /// </summary>
        public int Level { get; protected set; }

        /// <summary>
        /// The number of health points enemies of this type start with.
        /// </summary>
        public int StartingHealth { get; protected set; }

        /// <summary>
        /// The base attack of the enemies of this type.
        /// </summary>
        public int Attack { get; protected set; }

        /// <summary>
        /// The base defense of the enemies of this type.
        /// </summary>
        public int Defense { get; protected set; }

        /// <summary>
        /// The character which represents enemies of this type in the game display.
        /// </summary>
        /// <seealso cref="Enemy.DisplayCharacter"/>
        /// <seealso cref="IPhysicalEntity.DisplayCharacter"/>
        public char DisplayCharacter { get; protected set; }

        /// <summary>
        /// Initializes a new EnemyType with a given id and retrieves its information from the database.
        /// </summary>
        public EnemyType(int id)
        {
            DataRow dr = EnemyTypesDAL.GetEnemyType(id);
            if (dr == null)
                throw new ArgumentException("No enemy type found with the given id", "id");

            InitFromDataRow(dr);
        }

        /// <summary>
        /// Initializes a default instance of <see cref="EnemyType"/>.
        /// </summary>
        protected EnemyType() { }

        /// <summary>
        /// Initializes a new EnemyType with the given properties and creates a matching record in the database
        /// </summary>
        public EnemyType(string name, int level, int startingHealth, int attack, int defense, char displayChar)
        {
            this.Id = EnemyTypesDAL.AddEnemyType(level, startingHealth, attack, defense, name, displayChar);

            this.Name = name;
            this.Level = level;
            this.StartingHealth = startingHealth;
            this.Attack = attack;
            this.Defense = defense;
            this.DisplayCharacter = displayChar;
        }

        internal EnemyType(DataRow dr)
        {
            InitFromDataRow(dr);
        }

        private void InitFromDataRow(DataRow dr)
        {
            this.Id = (int)dr["enemyTypeId"];

            this.Name = (string)dr["enemyName"];
            this.Level = (int)dr["level"];
            this.StartingHealth = (int)dr["startingHealth"];
            this.Attack = (int)dr["attack"];
            this.Defense = (int)dr["defense"];
            this.DisplayCharacter = OleDbHelper.CharOrError(dr["displayCharacter"]);
        }

        /// <summary>
        /// Updates the properties of this <see cref="EnemyType"/> and reflects the changes in the database.
        /// </summary>
        public void Update(string name, int level, int health, int attack, int defense, char displayChar)
        {
            this.Name = name;
            this.Level = level;
            this.StartingHealth = health;
            this.Attack = attack;
            this.Defense = defense;
            this.DisplayCharacter = displayChar;

            EnemyTypesDAL.Update(this.Id, level, health, attack, defense, name, displayChar);
        }

        /// <summary>
        /// Returns all enemy types whose levels are closest to the specified level.
        /// </summary>
        public static List<EnemyType> GetAllEnemyTypes(int level)
        {
            return EnemyTypesDAL.GetAllEnemies(level).ToObjects((dr) => new EnemyType(dr));
        }

        /// <summary>
        /// Returns all enemy types.
        /// </summary>
        public static List<EnemyType> GetAllEnemyTypes()
        {
            return EnemyTypesDAL.GetAllEnemies().ToObjects((dr) => new EnemyType(dr));
        }

        /// <summary>
        /// Initializes an EnemyType object from an existing enemy-type in the database.
        /// </summary>
        public static EnemyType FromDAL(int id) { return new EnemyType(id); }

        /// <summary>
        /// Returns this enemy-type as a <see cref="RelatedEnemyType"/>.
        /// </summary>
        public RelatedEnemyType AsRelated()
        {
            return new RelatedEnemyType(this);
        }

        void IDeletable.Delete()
        {
            EnemyTypesDAL.DeleteEnemyType(this.Id);
        }

        public override bool Equals(object obj)
        {
            var other = obj as EnemyType;
            return other != null && this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }
    }

    /// <summary>
    /// Represents an enemy-type object which can be lazily initialized.
    /// </summary>
    public class RelatedEnemyType : RelatedObject<EnemyType, int>, IDeletable
    {
        /// <summary>
        /// Initializes a new <see cref="RelatedEnemyType"/> for the enemy-type with the given id.
        /// </summary>
        public RelatedEnemyType(int id) : base(id, EnemyType.FromDAL) { }

        /// <summary>
        /// Initializes a new <see cref="RelatedEnemyType"/> for an already-loaded enemy-type.
        /// </summary>
        /// <param name="instance">An existing enemy-type object to wrap</param>
        public RelatedEnemyType(EnemyType instance) : base(instance.Id, instance) { }

        void IDeletable.Delete()
        {
            ((IDeletable)Value).Delete();
        }
    }
}
