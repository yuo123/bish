﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BishBL
{
    /// <summary>
    /// Represents a database-backed object which can be lazily initialized
    /// </summary>
    /// <remarks>
    /// This class is used for properties in BL classes that reference other BL classes.
    /// <para>
    /// For example, the Game class has the property Player of type RelatedPlayer. As a result, when the game
    /// is just displayed in a list, and only the player's name has to be displayed, the entire player is not
    /// retreived from the database. When that player's full data has to be accessed, Player.Value will be used,
    /// and it will be seamlessly loaded.
    /// </para>
    /// </remarks>
    /// <typeparam name="TObj">The BL object type for lazy initialization</typeparam>
    /// <typeparam name="TKey">The type of the database object's primary key</typeparam>
    public abstract class RelatedObject<TObj, TKey> : Lazy<TObj>, IRelated<TObj> where TObj : class
    {
        /// <summary>
        /// The database object's primary key
        /// </summary>
        public TKey PrimaryKey { get; set; }

        /// <summary>
        /// Initialize a new RelatedObject with the given primary key and instantiation method
        /// </summary>
        /// <param name="primaryKey">The object's primary key in the database</param>
        /// <param name="instantiator">A function which loads a database-backed object given its primary key</param>
        protected RelatedObject(TKey primaryKey, Func<TKey, TObj> instantiator)
            : base(() => Instantiate(primaryKey, instantiator))
        {
            this.PrimaryKey = primaryKey;
        }

        /// <summary>
        /// Initializes a new RelatedObject with the given value, and function to extract the key from the value.
        /// <para>
        /// If this constructor is used, there is no lazy initialization
        /// </para>
        /// </summary>
        protected RelatedObject(TKey primaryKey, TObj value)
            : this(primaryKey, key => value)
        {
            //this forces the instantiation, which frees the lambda object
            var tmp = this.Value;
        }

        //used by the first constructor
        private static TObj Instantiate(TKey primaryKey, Func<TKey, TObj> instantiator)
        {
            TObj re = instantiator(primaryKey);
            if (re == null)
                throw new ArgumentException("No database object was found for the RelatedObjects's primary key", "primaryKey");

            return re;
        }

        /// <summary>
        /// Implicitly converts a <see cref="RelatedObject{TObj, TKey}"/> to the actual object.
        /// </summary>
        /// <param name="related">A related object</param>
        public static implicit operator TObj(RelatedObject<TObj, TKey> related)
        {
            if (related == null)
                return null;

            return related.Value;
        }

        /// <summary>
        /// Returns true if the two objects are of the same type and share the same primary key.
        /// </summary>
        /// <param name="obj">The object to compare this object against</param>
        /// <returns><see langword="true"/> if the objects are equal; <see langword="false"/> otherwise.</returns>
        public override bool Equals(object obj)
        {
            var other = obj as RelatedObject<TObj, TKey>;
            return other != null
                && other.GetType() == this.GetType()
                && other.PrimaryKey.Equals(this.PrimaryKey);
        }

        // this matches the Equals definition
        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() ^ this.PrimaryKey.GetHashCode();
        }
    }

    /// <summary>
    /// A covariant interface for related entities, supplying access to value only.
    /// This allows a more effective use of polymorphism for related entities whose classes have inheritance relations.
    /// </summary>
    /// <typeparam name="TObj">The concrete entity type</typeparam>
    /// <seealso cref="RelatedObject{TObj, TKey}"/>
    public interface IRelated<out TObj>
    {
        /// <summary>
        /// The actual entity object.
        /// </summary>
        TObj Value { get; }
    }
}
