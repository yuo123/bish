﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Represents an item in the game which can give bonuses to characters using it.
    /// </summary>
    public class Item : IPhysicalEntity
    {
        /// <summary>
        /// The item's unique ID.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// The item's name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The item's attack bonus.
        /// </summary>
        public int AttackBonus { get; private set; }

        /// <summary>
        /// The item's defense bonus.
        /// </summary>
        public int DefenseBonus { get; private set; }

        /// <summary>
        /// The character which represents the item in the game.
        /// </summary>
        /// <seealso cref="IPhysicalEntity.DisplayCharacter"/>
        public char DisplayCharacter { get; private set; }

        /// <summary>
        /// The string associated with item entities.
        /// </summary>
        /// <seealso cref="IPhysicalEntity.DisplayClass"/>
        public string DisplayClass { get { return "item"; } }

        #region In room

        /// <summary>
        /// The room the item is inside, or <see langword="null"/> if it's not inside a room.
        /// </summary>
        public RelatedRoom CurrentRoom { get; set; }

        /// <summary>
        /// The X-Position of the item in the room.
        /// </summary>
        public int XPos { get; private set; }

        /// <summary>
        /// The Y-Position of the item in the room.
        /// </summary>
        public int YPos { get; private set; }

        #endregion

        #region On character (enemy or player)

        /// <summary>
        /// The character whose inventory this item is in.
        /// </summary>
        public IRelated<Character> CurrentCharacter { get; set; }

        #endregion

        /// <summary>
        /// Initializes a new <see cref="Item"/> in the given room and position
        /// </summary>
        public Item(string name, int attackBonus, int defenseBonus, char displayChar, RelatedRoom room, int xPos, int yPos)
        {
            this.Name = name;
            this.AttackBonus = attackBonus;
            this.DefenseBonus = defenseBonus;
            this.DisplayCharacter = displayChar;

            this.CurrentRoom = room;
            this.XPos = xPos;
            this.YPos = yPos;

            this.Id = ItemsDAL.CreateItemOnFloor(attackBonus, defenseBonus, name, displayChar, room.PrimaryKey, xPos, yPos);

            if (room.IsValueCreated && room.Value.Entities.IsValueCreated)
                room.Value.Entities.Value.Add(this);
        }

        /// <summary>
        /// Initializes a new <see cref="Item"/> on the given character
        /// </summary>
        public Item(string name, int attackBonus, int defenseBonus, char displayChar, IRelated<Character> character)
        {
            this.Name = name;
            this.AttackBonus = attackBonus;
            this.DefenseBonus = defenseBonus;
            this.DisplayCharacter = displayChar;

            this.CurrentCharacter = character;

            if (character is RelatedPlayer)
                this.Id = ItemsDAL.CreateItemOnPlayer(attackBonus, defenseBonus, name, displayChar, ((RelatedPlayer)character).PrimaryKey);
            else if (character is RelatedEnemy)
                this.Id = ItemsDAL.CreateItemOnEnemy(attackBonus, defenseBonus, name, displayChar, ((RelatedEnemy)character).PrimaryKey);
        }

        internal static List<Item> GetItemsOnEnemy(Enemy enemy)
        {
            return FromDataTable(ItemsDAL.GetItemsOnEnemy(enemy.Id), enemy);
        }

        private Item(IRelated<Character> currentCharacter, DataRow data)
        {
            InitFromDataRow(data, null);
            this.CurrentCharacter = currentCharacter;
        }

        private Item(DataRow data, RelatedRoom room)
        {
            InitFromDataRow(data, room);
            this.CurrentRoom = room;
        }

        private void InitFromDataRow(DataRow dr, RelatedRoom room)
        {
            this.Id = (int)dr["itemId"];

            this.Name = (string)dr["itemName"];
            this.AttackBonus = (int)dr["attack"];
            this.DefenseBonus = (int)dr["defense"];
            this.DisplayCharacter = OleDbHelper.CharOrError(dr["displayCharacter"]);

            RefreshFromDataRow(dr, room);
        }

        private void RefreshFromDataRow(DataRow dr, RelatedRoom room)
        {
            object curRoom = dr["roomId"];
            //only update the room if there was a change
            if ((curRoom is DBNull) != (this.CurrentRoom == null))
            {
                this.CurrentRoom = curRoom is DBNull ? null : room;
                this.XPos = (int)dr["xPos"];
                this.YPos = (int)dr["yPos"];
            }

            object curPlayer = dr["playerUsername"];
            object curEnemy = dr["enemyId"];

            // Remove character if necessary. The character class will take care of assigning the item to itself, if needed.
            if (curPlayer is DBNull && curEnemy is DBNull)
                this.CurrentCharacter = null;
        }

        /// <summary>
        /// Generates a new item with random properties in the given room and position.
        /// </summary>
        /// <param name="room">The room to generate the item in</param>
        /// <param name="pos">The position to place the new item at</param>
        /// <param name="rnd">A <see cref="Random"/> instance used to generate the random properties of the item</param>
        public static void Generate(Room room, Position pos, Random rnd)
        {
            var character = room.GetEntityAt(pos.X, pos.Y) as Character;
            if (character != null)
            {
                character.AddItem(new Item(
                    RandomNameGenerator.GetName(rnd),
                    rnd.NextCauchy(room.Difficulty, 5).Bound(0, room.Difficulty + 10),
                    rnd.NextCauchy(room.Difficulty, 5).Bound(0, room.Difficulty + 10),
                    (char)rnd.Next('A', 'Z' + 1),
                    character));
            }
            else
            {
                new Item(
                   RandomNameGenerator.GetName(rnd),
                   rnd.NextCauchy(room.Difficulty, 5).Bound(0, room.Difficulty + 10),
                   rnd.NextCauchy(room.Difficulty, 5).Bound(0, room.Difficulty + 10),
                   (char)rnd.Next('A', 'Z' + 1),
                   room.AsRelated(), pos.X, pos.Y);
            }
        }

        /// <summary>
        /// Refreshes the dynamic properties of this item from the given <see cref="DataTable"/>.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> possibly containing a row which corresponds to this item,
        /// from which its properties must be refreshed.
        /// </param>
        /// <param name="room">The room this item is currently in</param>
        /// <returns>
        /// The <see cref="DataRow"/> used to refresh this item, or <see langword="null"/> if no row was found,
        /// implying the item was removed from the room's floor.
        /// </returns>
        public DataRow RefreshFromDataTable(DataTable table, RelatedRoom room)
        {
            DataRow row = table.Rows.Find(this.Id);
            if (row != null)
                RefreshFromDataRow(row, room);
            return row;
        }

        internal static List<Item> GetItemsOnPlayer(Player player)
        {
            return FromDataTable(ItemsDAL.GetItemsOnPlayer(player.Username), player);
        }

        internal static List<Item> FromDataTable(DataTable table, IRelated<Character> character)
        {
            return table.ToObjects(row => new Item(character, row));
        }

        internal static IEnumerable<Item> FromDataRows(IEnumerable<DataRow> table, RelatedRoom room)
        {
            return table.Select(row => new Item(row, room));
        }

        internal static List<Item> GetItemsInRoom(RelatedRoom room)
        {
            return ItemsDAL.GetItemsInRoom(room.PrimaryKey).ToObjects(row => new Item(row, room));
        }

        /// <summary>
        /// Returns an empty <see cref="IEnumerable{T}"/>, because items do not have status classes.
        /// </summary>
        public IEnumerable<string> ConsumeStatusClasses()
        {
            // Currently, items cannot have status classes.
            return Enumerable.Empty<string>();
        }
    }
}
