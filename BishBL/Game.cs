﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Represents a past or on-going game.
    /// Records various statistics about the game.
    /// </summary>
    public class Game
    {
        /// <summary>
        /// Whether or not this game has been created already. If false, all other properties will have missing or incorrect values except <see cref="Player"/>
        /// </summary>
        public bool IsStarted { get; private set; }

        /// <summary>
        /// Whether or not this game has been finished already.
        /// </summary>
        public bool IsFinished { get { return this.LevelFinished != null; } }

        /// <summary>
        /// Whether or not this game is currently active (started but not finished).
        /// </summary>
        public bool IsActive { get { return IsStarted && !IsFinished; } }

        /// <summary>
        /// The game's unique ID
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// The player this game belongs to.
        /// </summary>
        public RelatedPlayer Player { get; private set; }

        /// <summary>
        /// The level the player started this game at.
        /// </summary>
        public int LevelStarted { get; private set; }

        /// <summary>
        /// The level the player finished this game at.
        /// </summary>
        public int? LevelFinished { get; private set; }

        /// <summary>
        /// The number of levels gained by the player during this game.
        /// </summary>
        public int LevelsGained { get { return LevelFinished.Value - LevelStarted; } }

        /// <summary>
        /// The number of Experience Points gained during this game.
        /// </summary>
        public int XpGained { get; private set; }

        /// <summary>
        /// The date and time at which this game was started.
        /// </summary>
        public DateTime TimeStarted { get; private set; }

        /// <summary>
        /// The date and time at which this game has ended.
        /// </summary>
        public DateTime? TimeFinished { get; private set; }

        /// <summary>
        /// The duration of the game, represented as a <see cref="TimeSpan"/> plus <c>DateTime.FromOADate(0)</c>.
        /// </summary>
        public DateTime Duration { get { return DateTime.FromOADate(0) + (TimeFinished.Value - TimeStarted); } }

        // This is a RelatedEnemyTypesList because most of the time Game is used without populating it
        private RelatedEnemyTypesList _enemiesKilled;

        /// <summary>
        /// The collection of enemy types which were killed by the player during the game.
        /// </summary>
        public IEnumerable<KilledEnemyType> EnemiesKilled
        {
            get
            {
                try
                {
                    return _enemiesKilled.Value.Values;
                }
                catch (NullReferenceException ex)
                {
                    throw new InvalidOperationException("Properties of Game are undefined if it is not created", ex);
                }
            }
        }

        // Stores the kill count as long as _enemiesKilled is not populated
        private int _enemiesKilledCount = -1;

        /// <summary>
        /// Gets the number of enemies killed during this game.
        /// </summary>
        public int EnemiesKilledCount
        {
            get
            {
                if (_enemiesKilled.IsValueCreated || _enemiesKilledCount == -1)
                    return _enemiesKilled.Value.Sum(et => et.Value.KilledCount);
                else
                    return _enemiesKilledCount;
            }
        }

        /// <summary>
        /// Initialize a new game with a given player
        /// </summary>
        /// <param name="player"></param>
        public Game(RelatedPlayer player)
        {
            this.IsStarted = false;
            this.Player = player;
            DataRow dr = GamesDAL.GetUnfinishedGame(player.PrimaryKey);

            InitFromDataRow(dr);
        }

        private Game(DataRow dr)
        {
            InitFromDataRow(dr);
        }

        /// <summary>
        /// Registers that an enemy of the given type was killed in this game.
        /// If an enemy of that type was already killed in this game, its count will be incremented;
        /// otherwise, it will be added to the killed enemies list.
        /// </summary>
        public void AddKilledEnemy(RelatedEnemyType enemyType)
        {
            GamesDAL.AddKilledEnemy(this.Id, enemyType.PrimaryKey);

            if (_enemiesKilled.IsValueCreated)
            {
                KilledEnemyType val;
                if (_enemiesKilled.Value.TryGetValue(enemyType.Value, out val))
                {
                    val.KilledCount++;
                }
                else
                {
                    val = new KilledEnemyType(enemyType.Value);
                    _enemiesKilled.Value.Add(val, val);
                }
            }
            else
            {
                _enemiesKilledCount++;
            }
        }

        /// <summary>
        /// Adds the given number of points to the XP Gained counter.
        /// </summary>
        public void AddXpGained(int amount)
        {
            this.XpGained += amount;
            GamesDAL.AddXp(this.Id, amount);
        }

        /// <summary>
        /// Ends this game.
        /// </summary>
        public void Finish()
        {
            if (IsFinished)
                throw new InvalidOperationException("Cannot finish a game that has already finished");

            GamesDAL.FinishGame(this.Id);

            //refresh data to update time and level finished
            InitFromDataRow(GamesDAL.GetGameDetails(this.Id));
        }

        /// <summary>
        /// Starts this game.
        /// </summary>
        public void Start()
        {
            if (IsStarted)
                throw new InvalidOperationException("Cannot start a game that was already started");

            GamesDAL.NewGame(this.Player.PrimaryKey);
            InitFromDataRow(GamesDAL.GetUnfinishedGame(Player.PrimaryKey));
        }

        /// <summary>
        /// Returns a list of all games of the given player
        /// </summary>
        /// <param name="player">The username of the player to return the games for, or <see langword="null"/> for all players</param>
        public static List<Game> GetAllGames(RelatedPlayer player = null)
        {
            return GamesDAL.GetAllGames(player != null ? player.PrimaryKey : null).ToObjects((dr) => new Game(dr));
        }

        /// <summary>
        /// Returns a list of all finished games of the given player
        /// </summary>
        /// <param name="player">The username of the player to return the games for, or <see langword="null"/> for all players</param>
        public static List<Game> GetAllFinishedGames(RelatedPlayer player = null)
        {
            return GetAllGames(player).Where(g => g.IsFinished).ToList();
        }

        private void InitFromDataRow(DataRow dr)
        {
            this.IsStarted = dr != null;
            if (!IsStarted)
                return;

            this.Id = (int)dr["gameId"];
            this.Player = new RelatedPlayer((string)dr["playerUsername"]);
            this.LevelStarted = (int)dr["levelStarted"];
            this.LevelFinished = dr["levelFinished"] as int?;
            this.XpGained = (int)dr["xpGained"];
            this.TimeStarted = (DateTime)dr["timeStarted"];
            this.TimeFinished = dr["timeFinished"] as DateTime?;

            _enemiesKilled = new RelatedEnemyTypesList(Id);
            // get the enemy count as calculated by the database, if present
            if (dr.Table.Columns.Contains("enemiesKilled"))
                _enemiesKilledCount = (int)(double)dr["enemiesKilled"].DefaultIf(DBNull.Value, 0.0);
        }

        public override bool Equals(object obj)
        {
            Game game = obj as Game;
            return game != null && game.GetHashCode() == this.GetHashCode();
        }

        public override int GetHashCode()
        {
            return IsStarted ? Id.GetHashCode() : base.GetHashCode();
        }

        /// <summary>
        /// Refreshes this game's properties from the database.
        /// </summary>
        public void Refresh()
        {
            InitFromDataRow(GamesDAL.GetGameDetails(this.Id));
        }

        private class RelatedEnemyTypesList : RelatedObject<Dictionary<EnemyType, KilledEnemyType>, int>
        {
            public RelatedEnemyTypesList(int gameId)
                : base(gameId,
                      id => GamesDAL.GetEnemiesKilled(id).AsEnumerable()
                          // The keys in the dictionary are original EnemyTypes, and the values are their unique KilledEnemyType variants
                          .Select(row => new KilledEnemyType(row)).ToDictionary<KilledEnemyType, EnemyType, KilledEnemyType>(e => e, e => e))
            { }
        }
    }

    /// <summary>
    /// Represents an <see cref="EnemyType"/> in the killed enemies list of a game.
    /// </summary>
    public class KilledEnemyType : EnemyType
    {
        /// <summary>
        /// The number of times this enemy type was killed in the game
        /// </summary>
        public int KilledCount { get; set; }

        internal KilledEnemyType(DataRow dr)
            : base(dr)
        {
            this.KilledCount = (int)dr["enemyCount"];
        }

        /// <summary>
        /// Initializes a new <see cref="KilledEnemyType"/> based on a regular <see cref="EnemyType"/>.
        /// </summary>
        /// <param name="original">An <c>EnemyType</c> to copy the relevant properties from</param>
        public KilledEnemyType(EnemyType original)
        {
            this.Id = original.Id;
            this.Name = original.Name;
            this.Level = original.Level;
            this.Attack = original.Attack;
            this.Defense = original.Defense;
            this.StartingHealth = original.StartingHealth;
            this.DisplayCharacter = original.DisplayCharacter;

            this.KilledCount = 1;
        }
    }
}
