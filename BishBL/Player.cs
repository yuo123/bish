﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Represents a BISH player, both as a user and a game entity.
    /// </summary>
    public class Player : Character, IDeletable, IRelated<Player>
    {
        private const string LEVELUP_STATUS = "leveled-up";

        internal const int STARTING_LEVEL = PlayersDAL.STARTING_LEVEL;
        internal const int STARTING_HEALTH = PlayersDAL.STARTING_HEALTH;
        internal const int STARTING_ATTACK = PlayersDAL.STARTING_ATTACK;
        internal const int STARTING_DEFENSE = PlayersDAL.STARTING_DEFENSE;
        internal const int STARTING_XP = PlayersDAL.STARTING_XP;

        /// <summary>
        /// The player's username.
        /// </summary>
        public string Username { get; private set; }

        /// <summary>
        /// The player's username.
        /// </summary>
        protected override object PrimaryKey { get { return Username; } }

        /// <summary>
        /// The player's email address.
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// The full name of the player's country of origin.
        /// </summary>
        public string Country { get; private set; }

        /// <summary>
        /// The player's date of birth.
        /// </summary>
        public DateTime DOB { get; private set; }

        /// <summary>
        /// The player's age, in years.
        /// </summary>
        public int Age { get { return Extensions.CalcAge(this.DOB); } }

        /// <summary>
        /// The character which represents players in the game display.
        /// </summary>
        /// <seealso cref="P:BishBL.IPhysicalEntity.DisplayCharacter" />
        public override char DisplayCharacter { get { return '@'; } }

        /// <summary>
        /// The string associated with players.
        /// </summary>
        /// <seealso cref="P:BishBL.IPhysicalEntity.DisplayClass" />
        public override string DisplayClass { get { return "player"; } }

        private bool _isBanned;

        /// <summary>
        /// Whether or not this player is banned from playing by an admin.
        /// </summary>
        public bool IsBanned
        {
            get { return _isBanned; }
            set
            {
                _isBanned = value;
                PlayersDAL.SetBanStatus(this.Username, _isBanned);
            }
        }

        /// <summary>
        /// The number of Experience Points this player has.
        /// </summary>
        public int Xp { get; private set; }

        /// <summary>
        /// Returns a list of all registered players.
        /// </summary>
        public static List<Player> GetAllPlayers()
        {
            return PlayersDAL.GetAllPlayers().ToObjects(row => new Player(row));
        }

        /// <summary>
        /// The current game of this player.
        /// <para>
        /// The value of this property is never <see langword="null"/>. 
        /// If no game is in progress, the game's <c>IsStarted</c> property will be <see langword="false"/>.
        /// </para>
        /// </summary>
        /// <seealso cref="Game.IsStarted"/>
        public Game CurrentGame { get; private set; }

        Player IRelated<Player>.Value { get { return this; } }

        /// <summary>
        /// Initializes a player object by its login credentials.
        /// </summary>
        /// <param name="username">The player's username</param>
        /// <param name="password">The player's password</param>
        /// <returns>A <see cref="Player"/> object matching the given username and password, or <see langword="null"/> if no such user was found.</returns>
        public static Player FromLogin(string username, string password)
        {
            if (PlayersDAL.DoesUserExist(username, password))
                return FromDAL(username);
            else
                return null;
        }

        /// <summary>
        /// Initializes a player from the database.
        /// </summary>
        /// <param name="username">The username of the player to retrieve</param>
        /// <returns>A <see cref="Player"/> object matching the given username, or <see langword="null"/> if no such user was found.</returns>
        public static Player FromDAL(string username)
        {
            DataRow data = PlayersDAL.GetPlayerDetails(username);
            return data != null ? new Player(data) : null;
        }

        /// <summary>
        /// Registers a new player, adds it to the database, and returns it.
        /// </summary>
        /// <param name="username">The player's username</param>
        /// <param name="password">The player's password</param>
        /// <param name="email">The player's email</param>
        /// <param name="country">The full name of the player's country of origin</param>
        /// <param name="DOB">The player's date of birth</param>
        /// <returns>The newly-created player</returns>
        public static Player NewPlayer(string username, string password, string email, string country, DateTime DOB)
        {
            if (PlayersDAL.IsUsernameTaken(username))
                return null;

            PlayersDAL.CreatePlayer(username, password, email, country, DOB);
            PlayersDAL.SetStartingStats(username);
            return FromDAL(username);
        }

        private Player(DataRow dr)
            : this(dr,
                dr["roomId"] is DBNull ? null : new RelatedRoom((int)dr["roomId"]))
        { }

        private Player(DataRow dr, RelatedRoom room)
        {
            InitFromDataRow(dr, room);
            this.CurrentGame = new Game(new RelatedPlayer(this));
        }

        /// <summary>
        /// Assigns the data from the given DataRow to this player's properties
        /// </summary>
        /// <param name="dr">
        /// A <see cref="DataRow" /> containing at least the following fields: attack, defense, health, level, xPos, yPos, roomId
        /// </param>
        /// <param name="room">The room this player is in</param>
        /// <param name="initial">
        /// <see langword="true" /> if the player is first loaded from the database, <see langword="false" /> if it is only being refreshed
        /// </param>
        protected override void InitFromDataRow(DataRow dr, RelatedRoom room, bool initial = true)
        {
            if (initial)
            {
                this.Username = (string)dr["username"];
                this.Email = (string)dr["email"];
                this.Country = (string)dr["country"];
                this.DOB = (DateTime)dr["DOB"];
            }
            else
            {
                // Players' levels can change during the game
                this.Level = (int)dr["level"];
            }

            this.Xp = (int)dr["xp"];
            _isBanned = (bool)dr["isBanned"];
            if (this.CurrentGame == null)
            {
                this.CurrentGame = new Game(new RelatedPlayer(this));
            }
            else
            {
                this.CurrentGame.Refresh();
                if (CurrentGame.IsFinished)
                    this.CurrentGame = new Game(new RelatedPlayer(this));
            }

            //assign properties inherited from Character
            base.InitFromDataRow(dr, room, initial);
        }

        /// <summary>
        /// Moves the player by the given amounts if possible, and returns whether a move has occurred.
        /// </summary>
        public override bool TryMove(int dx, int dy)
        {
            bool moved = base.TryMove(dx, dy);
            if (!moved)
            {
                IPhysicalEntity ent = CurrentRoom.Value.GetEntityAt(XPos + dx, YPos + dy);
                if (ent is Character)
                    Attack((Character)ent);
                else if (ent is Door)
                    UseDoor((Door)ent);
            }
            return moved;
        }

        private void UseDoor(Door door)
        {
            if (door.Connected == null)
                CurrentRoom = Room.Generate(CurrentRoom.Value.Difficulty + 1, door).AsRelated();
            else
                CurrentRoom = door.Connected.CurrentRoom;

            Position pos = door.Connected.GetLandingPosition();
            this.XPos = pos.X;
            this.YPos = pos.Y;

            PlayersDAL.ChangePlayerRoom(this.Username, this.CurrentRoom.PrimaryKey, XPos, YPos);
            Room.DeleteEmptyRooms();
        }

        /// <summary>
        /// Raises the <see cref="E:BishBL.Character.Killed" /> event, with this <see cref="Player" /> as killed and <paramref name="killer" /> as killer.
        /// </summary>
        protected override void OnKilled(Character killer)
        {
            base.OnKilled(killer);
            FinishGame();
        }

        /// <summary>
        /// Ends the currently active game and removes the player from the room they are in.
        /// </summary>
        public virtual void FinishGame()
        {
            PlayersDAL.RemovePlayerFromRoom(this.Username);
            this.CurrentRoom = null;
            this.ClearItems();

            CurrentGame.Finish();
            this.CurrentGame = new Game(new RelatedPlayer(this));
        }

        /// <summary>
        /// Clears the internal list as well as the database list of items possessed by this character.
        /// </summary>
        protected override void ClearItems()
        {
            ItemsDAL.DeleteItemsOnPlayer(this.Username);
            base.ClearItems();
        }

        /// <summary>
        /// Applies the specified amount of damage to this player.
        /// </summary>
        protected override void ReceiveDamage(int amount)
        {
            base.ReceiveDamage(amount);
            this.Health -= amount;
            PlayersDAL.ChangeHealth(this.Username, -amount);
        }

        /// <summary>
        /// Moves the specified item from the floor to this player's inventory.
        /// </summary>
        /// <param name="item">The item to pick up</param>
        public override void PickUp(Item item)
        {
            this.AddItem(item);
            ItemsDAL.PickupItem(item.Id, this.Username);
        }

        /// <summary>
        /// Retrieves a list of all items on this player from the database.
        /// </summary>
        protected override List<Item> GetItems()
        {
            return Item.GetItemsOnPlayer(this);
        }

        /// <summary>
        /// Makes this player attack another character, and returns true if the other was killed.
        /// </summary>
        public override bool Attack(Character other)
        {
            bool killed = base.Attack(other);
            if (killed)
            {
                this.Xp += other.Level;
                PlayersDAL.ChangeXp(this.Username, other.Level);
                CurrentGame.AddXpGained(other.Level);
                if (other is Enemy)
                    this.CurrentGame.AddKilledEnemy(((Enemy)other).EnemyType);

                if (Xp > CalcXpThreshold(this.Level))
                    LevelUp();
            }

            return killed;
        }

        /// <summary>
        /// Increments the player's level and applies level-up bonuses.
        /// </summary>
        protected virtual void LevelUp()
        {
            this.Level++;
            this.Xp = 0;

            int bonus = CalcStatBonusPerLevel(this.Level);
            this.BaseAttack += bonus;
            this.BaseDefense += bonus;
            this.Health += bonus;

            PlayersDAL.LevelUp(Username, BaseAttack, BaseDefense, Health);

            this.AddStatus(LEVELUP_STATUS);
        }

        /// <summary>
        /// Returns the amount of XP required to advance beyond a given level
        /// </summary>
        public static int CalcXpThreshold(int level)
        {
            // This formula was manually found to give a reasonable progression curve
            return (int)Math.Round(0.2 * Math.Pow(2.35 + 0.5 * level, 2.6));
        }

        /// <summary>
        /// Calculates and returns the bonus to each numeric strength-like property (e.g. attack, defense)
        /// applied to a player on level-up.
        /// </summary>
        /// <param name="level">The player level to calculate the bonus for</param>
        public static int CalcStatBonusPerLevel(int level)
        {
            // This formula was manually found to give a reasonable progression curve
            return (int)Math.Round(Math.Log(level, 1.1));
        }

        /// <summary>
        /// Places the player in a starting room and starts the current game
        /// </summary>
        public void NewGame()
        {
            PlayersDAL.ResetHealth(this.Username);
            this.Health = STARTING_HEALTH;
            this.CurrentGame.Start();
            this.CurrentRoom = new RelatedRoom(Room.GetStartingRoom());
            PlayersDAL.ChangePlayerRoom(this.Username, CurrentRoom.Value.Id, 0, 0);
        }

        /// <summary>
        /// Gets a list of all players in a given room
        /// </summary>
        public static List<Player> GetAllPlayers(RelatedRoom room)
        {
            return PlayersDAL.GetPlayersInRoom(room.PrimaryKey).ToObjects(row => new Player(row, room));
        }

        /// <summary>
        /// Returns the names of all registered players.
        /// </summary>
        public static string[] GetPlayerNames()
        {
            DataTable tbl = PlayersDAL.GetAllPlayerNames();
            return tbl.AsEnumerable().Select(row => (string)row["username"]).ToArray(tbl.Rows.Count);
        }

        /// <summary>
        /// Refreshes the player's properties (which can be changed during a game) from the database.
        /// </summary>
        /// <param name="room">The room this player is in.</param>
        public void RefreshFromDatabase(RelatedRoom room)
        {
            this.InitFromDataRow(PlayersDAL.GetPlayerDetails(this.Username), room, false);
        }

        internal static IEnumerable<Player> FromDataRows(IEnumerable<DataRow> rows, RelatedRoom room)
        {
            return rows.Select(row => new Player(row, room));
        }

        /// <summary>
        /// Executes position changes in the database
        /// </summary>
        public override void CommitMovement()
        {
            PlayersDAL.MovePlayer(this.Username, this.XPos, this.YPos);
        }

        void IDeletable.Delete()
        {
            PlayersDAL.DeletePlayer(this.Username);
        }

        public override bool Equals(object obj)
        {
            return obj is Player && ((Player)obj).Username == this.Username;
        }

        public override int GetHashCode()
        {
            return Username.GetHashCode();
        }
    }

    /// <summary>
    /// Represents a player which can be lazily initialized from the database
    /// </summary>
    /// <seealso cref="RelatedObject{TObj, TKey}"/>
    public class RelatedPlayer : RelatedObject<Player, string>, IDeletable
    {
        /// <summary>
        /// Initializes a new <see cref="RelatedPlayer"/> with the given username.
        /// </summary>
        /// <param name="username"></param>
        public RelatedPlayer(string username) : base(username, Player.FromDAL) { }

        /// <summary>
        /// Initializes a new <see cref="RelatedPlayer"/> which wraps an existing <see cref="Player"/>
        /// </summary>
        /// <param name="player"></param>
        public RelatedPlayer(Player player) : base(player.Username, player) { }

        void IDeletable.Delete()
        {
            ((IDeletable)this.Value).Delete();
        }
    }
}
