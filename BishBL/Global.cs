﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Static class which provides access to application-wide functionality.
    /// </summary>
    public static class Global
    {
        /// <summary>
        /// Closes any open database connections.
        /// Should be called before an unknown period of inactivity (such as the completion of a web request).
        /// </summary>
        public static void CloseDBConnection()
        {
            OleDbHelper.CloseOpenConnection();
        }
    }
}
