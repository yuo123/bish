﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BishDAL;

namespace BishBL
{
    /// <summary>
    /// Represents an independent agent in the world that can attack other characters
    /// </summary>
    public abstract class Character : IMovableEntity, IRelated<Character>
    {
        private const string DAMAGED_STATUS = "damaged";
        private const string BLOCKED_STATUS = "blocked";

        /// <summary>
        /// The character's database primary key used to refresh its properties.
        /// </summary>
        protected abstract object PrimaryKey { get; }

        /// <summary>
        /// The character's attack power, unmodified by item bonuses.
        /// </summary>
        public virtual int BaseAttack { get; protected set; }

        /// <summary>
        /// The character's defense power, unmodified by item bonuses.
        /// </summary>
        public virtual int BaseDefense { get; protected set; }

        /// <summary>
        /// The number of health points this character has.
        /// </summary>
        public virtual int Health { get; protected set; }

        /// <summary>
        /// This character's level.
        /// </summary>
        public virtual int Level { get; protected set; }

        /// <summary>
        /// Gets the effective attack of this character, including item bonuses.
        /// </summary>
        public virtual int EffectiveAttack { get { return BaseAttack + Items.Sum(item => item.AttackBonus); } }

        /// <summary>
        /// Gets the effective defense of this character, including item bonuses.
        /// </summary>
        public virtual int EffectiveDefense { get { return BaseDefense + Items.Sum(item => item.DefenseBonus); } }

        /// <summary>
        /// The character which represents this character in the game display.
        /// </summary>
        /// <seealso cref="IPhysicalEntity.DisplayCharacter"/>
        public abstract char DisplayCharacter { get; }

        /// <summary>
        /// The string associated with the type of this character.
        /// </summary>
        /// <seealso cref="IPhysicalEntity.DisplayClass"/>
        public abstract string DisplayClass { get; }

        private List<Item> _items;

        /// <summary>
        /// Gets all the items currently on this character.
        /// </summary>
        public IEnumerable<Item> Items { get { return _items; } }

        private HashSet<string> _statuses = new HashSet<string>();

        #region IMovableEntity implementation

        /// <summary>
        /// The room this character is currently in. If null, means that the character is not physically present anywhere.
        /// </summary>
        public RelatedRoom CurrentRoom { get; protected set; }

        /// <summary>
        /// The x-position of the character in the room.
        /// </summary>
        public int XPos { get; set; }

        /// <summary>
        /// The y-position of the character in the room.
        /// </summary>
        public int YPos { get; set; }

        #endregion

        Character IRelated<Character>.Value { get { return this; } }

        /// <summary>
        /// Returns a collection of strings describing the current status of the character, for rendering purposes.
        /// Calling this method consumes the status, changing the return value for subsequent calls.
        /// </summary>
        /// <seealso cref="IPhysicalEntity.ConsumeStatusClasses"/>
        public IEnumerable<string> ConsumeStatusClasses()
        {
            // The status list needs to be cleared, so we just pass over the original HashSet to the caller and create a new one.
            HashSet<string> re = _statuses;
            _statuses = new HashSet<string>();
            return re;
        }

        void IMovableEntity.Commit()
        {
            CommitMovement();
        }

        /// <summary>
        /// Executes position changes in the database.
        /// </summary>
        public abstract void CommitMovement();

        /// <summary>
        /// Adds a status string to the current status of this character, if the same string is not already present.
        /// The status will be consumed on the next call to <see cref="ConsumeStatusClasses"/>.
        /// </summary>
        protected void AddStatus(string status)
        {
            if (!_statuses.Contains(status))
                _statuses.Add(status);
        }

        /// <summary>
        /// Moves the character by the given amounts if possible, and returns whether a move has occurred.
        /// </summary>
        public virtual bool TryMove(int dx, int dy)
        {
            if (CurrentRoom == null)
                throw new InvalidOperationException("Cannot move a character not in a room");

            // Optimization - no move
            if (dx == 0 && dy == 0)
                return true;

            int nx = XPos + dx;
            int ny = YPos + dy;

            // Detect wall collision
            if (nx < 0 || nx >= CurrentRoom.Value.Width || ny < 0 || ny >= CurrentRoom.Value.Length)
                return false;

            IPhysicalEntity ent = CurrentRoom.Value.GetEntityAt(nx, ny);
            if (ent is Item)
                this.PickUp((Item)ent);
            else if (ent != null)
                return false;

            XPos += dx;
            YPos += dy;
            return true;
        }

        /// <summary>
        /// Moves the specified item from the floor to this character's inventory.
        /// </summary>
        /// <param name="item">The item to pick up</param>
        public abstract void PickUp(Item item);

        internal virtual void AddItem(Item item)
        {
            _items.Add(item);
            item.CurrentCharacter = this;
            item.CurrentRoom = null;
        }

        /// <summary>
        /// Clears the internal items list possessed by this character.
        /// </summary>
        protected virtual void ClearItems()
        {
            _items.Clear();
        }

        /// <summary>
        /// Initializes a new <see cref="Character"/> in a default state.
        /// </summary>
        protected Character()
        {
            _items = new List<Item>();
        }

        /// <summary>
        /// Assigns the data from the given DataRow to this character's properties
        /// </summary>
        /// <param name="dr">
        /// A <see cref="DataRow"/> containing at least the following fields: attack, defense, health, level, xPos, yPos, roomId
        /// </param>
        /// <param name="room">The room this character is in</param>
        /// <param name="initial">
        /// <see langword="true"/> if the character is first loaded from the database, <see langword="false"/> if it is only being refreshed.
        /// </param>
        protected virtual void InitFromDataRow(DataRow dr, RelatedRoom room, bool initial = true)
        {
            if (initial)
            {
                this.BaseAttack = (int)dr["attack"];
                this.BaseDefense = (int)dr["defense"];
                this.Level = (int)dr["level"];
                _items = GetItems();
            }
            this.Health = (int)dr["health"];

            if (dr["roomId"] != DBNull.Value)
            {
                this.XPos = (int)dr["xPos"];
                this.YPos = (int)dr["yPos"];
                if (CurrentRoom == null || CurrentRoom.PrimaryKey != room.PrimaryKey)
                    this.CurrentRoom = room;
            }
        }

        /// <summary>
        /// Retrieves a list of all items on this character from the database.
        /// </summary>
        protected abstract List<Item> GetItems();

        /// <summary>
        /// Called when this character is killed.
        /// </summary>
        protected virtual void OnKilled(Character killer) { }

        /// <summary>
        /// Applies the specified amount of damage to this character.
        /// </summary>
        protected virtual void ReceiveDamage(int amount)
        {
            //actual property changes are handled by child classes
            if (amount > 0)
                this.AddStatus(DAMAGED_STATUS);
            else
                this.AddStatus(BLOCKED_STATUS);
        }

        /// <summary>
        /// Makes this character attack another one, and returns true if the other was killed.
        /// </summary>
        public virtual bool Attack(Character other)
        {
            other.ReceiveDamage(Math.Max(0, this.EffectiveAttack - other.EffectiveDefense));
            bool killed = other.Health <= 0;
            if (killed)
                other.OnKilled(this);

            return killed;
        }

        /// <summary>
        /// Refreshes the dynamic properties of this character from the given <see cref="DataTable"/>.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> possibly containing a row which corresponds to this character,
        /// from which its properties must be refreshed.
        /// </param>
        /// <param name="room">The room this character is currently in</param>
        /// <returns>
        /// The <see cref="DataRow"/> used to refresh this character, or <see langword="null"/> if no row was found,
        /// implying the character was killed.
        /// </returns>
        public virtual DataRow RefreshFromDataTable(DataTable table, RelatedRoom room)
        {
            DataRow row = table.Rows.Find(this.PrimaryKey);
            if (row != null)
                InitFromDataRow(row, room, false);
            else
                OnKilled(null);

            return row;
        }
    }
}
