﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BishBL.Tests
{
    [TestClass]
    public class GameTests
    {
        private TemporaryObject<RelatedPlayer> GetTestPlayer()
        {
            return new TemporaryObject<RelatedPlayer>(new RelatedPlayer(
                Player.NewPlayer(new Guid().ToString(), "", "", "", DateTime.FromOADate(0))));
        }

        [TestMethod]
        public void New_game_that_doesnt_exist_is_not_started()
        {
            using (TemporaryObject<RelatedPlayer> player = GetTestPlayer())
            {
                Game game = new Game(player);
                Assert.IsFalse(game.IsStarted);
            }
        }

        [TestMethod]
        public void New_game_that_does_exists_is_started()
        {
            using (TemporaryObject<RelatedPlayer> player = GetTestPlayer())
            {
                Game game = new Game(player);
                game.Start();
                Assert.IsTrue(game.IsStarted);

                game = new Game(player);
                Assert.IsTrue(game.IsStarted);
            }
        }

        [TestMethod]
        public void Starting_started_game_throws()
        {
            using (TemporaryObject<RelatedPlayer> player = GetTestPlayer())
            {
                Game game = new Game(player);
                game.Start();
                Assert.ThrowsException<InvalidOperationException>(() => game.Start());
            }
        }

        [TestMethod]
        public void New_Game_included_in_all_games()
        {
            using (TemporaryObject<RelatedPlayer> player = GetTestPlayer())
            {
                Game game = new Game(player);
                if (game.IsStarted)
                    Assert.Inconclusive("A game is already in progress for the test player.");
                game.Start();
                List<Game> games = Game.GetAllGames();
                CollectionAssert.Contains(games, game);
            }
        }

        [TestMethod]
        public void EnemiesKilled_of_not_started_game_throws()
        {
            using (TemporaryObject<RelatedPlayer> player = GetTestPlayer())
            {
                Game game = new Game(player);
                Assert.ThrowsException<InvalidOperationException>(() => game.EnemiesKilled);
            }
        }

        [TestMethod]
        public void New_game_has_no_killed_enemies()
        {
            using (TemporaryObject<RelatedPlayer> player = GetTestPlayer())
            {
                Game game = new Game(player);
                game.Start();
                Assert.AreEqual(0, game.EnemiesKilledCount);
            }
        }
    }
}
