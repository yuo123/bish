﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BishBL.Tests
{
    /// <summary>
    /// Quick and dirty tests written when out of time
    /// </summary>
    [TestClass]
    public class AdHocTests
    {
        [TestMethod]
        public void TestPlayer()
        {
            const string USERNAME = "AdHocTests.TestPlayer";
            const string PASSWORD = "test pass";
            const string EMAIL = "TEST@TEST.COM";
            const string COUNTRY = "TEST COUNTRY";
            DateTime DOB = new DateTime(2000, 1, 2);

            using (TemporaryObject<Player> tmpPlayer = Player.NewPlayer(USERNAME, PASSWORD, EMAIL, COUNTRY, DOB).AsTemporary())
            {
                Player player = tmpPlayer.Original;
                Assert.IsNotNull(player);
                Assert.IsFalse(player.CurrentGame.IsStarted);

                player.NewGame();
                Assert.AreEqual(1, player.CurrentRoom.Value.Difficulty);
                Assert.IsTrue(player.CurrentGame.IsStarted);
                Assert.IsFalse(player.CurrentGame.IsFinished);

                player.XPos += 1;
                player.YPos -= 2;
                player.CommitMovement();

                player.Attack(player);

                Player updated = Player.FromLogin(USERNAME, PASSWORD);
                Assert.AreEqual(player.XPos, updated.XPos);
                Assert.AreEqual(player.YPos, updated.YPos);
                Assert.AreEqual(Player.STARTING_HEALTH - Player.STARTING_ATTACK + Player.STARTING_DEFENSE, updated.Health);

                player.Attack(updated);

                Assert.IsNull(updated.CurrentRoom);
                Assert.IsTrue(updated.CurrentGame.IsFinished);
            }
        }

        [TestMethod]
        public void TestEnemy()
        {
            const int BASE_ATTACK = 4;
            const int BASE_DEFENSE = 3;
            const int BASE_HEALTH = 2;
            using (var tmpEnemyType = new EnemyType("AdHocTests.TestEnemy", 2, BASE_HEALTH, BASE_ATTACK, BASE_DEFENSE, 'E').AsRelated().AsTemporary())
            using (var tmpEnemy = new Enemy(tmpEnemyType, Room.GetStartingRoom().AsRelated(), 0, 0).AsTemporary())
            {
                Enemy enemy = tmpEnemy.Original;
                EnemyType type = tmpEnemyType.Original;

                Assert.AreEqual(type.Attack, enemy.BaseAttack);
                Assert.AreEqual(type.Defense, enemy.BaseDefense);
                Assert.AreEqual(type.Level, enemy.Level);
                Assert.AreEqual(type.StartingHealth, enemy.Health);
                Assert.IsNotNull(enemy.CurrentRoom);

                enemy.XPos += 5;
                enemy.YPos += 1;
                enemy.CommitMovement();
                enemy.Attack(enemy);
                Assert.AreEqual(BASE_HEALTH - BASE_ATTACK + BASE_DEFENSE, enemy.Health);

                Enemy updated = Enemy.GetAllEnemies(enemy.CurrentRoom).Find(e => e.Id == enemy.Id);
                Assert.AreEqual(enemy.XPos, updated.XPos);
                Assert.AreEqual(enemy.YPos, updated.YPos);
                Assert.AreEqual(enemy.Health, updated.Health);
                Assert.AreEqual(enemy.CurrentRoom, updated.CurrentRoom);

                enemy.Attack(updated);
                Assert.IsNull(updated.CurrentRoom);
                Assert.IsNull(Enemy.GetAllEnemies(enemy.CurrentRoom).Find(e => e.Id == enemy.Id));
            }
        }
    }
}
