﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;

using BishUI.GeoIPService;

using BishBL;
using System.Threading.Tasks;
using System.Net;

namespace BishUI
{
    public partial class SignUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            foreach (CompareValidator val in this.Validators.OfType<CompareValidator>().Where(vld => vld.Type == ValidationDataType.Date))
                val.ValueToCompare = DateTime.Today.ToShortDateString();

            RegisterAsyncTask(new PageAsyncTask(SetDefaultCountry));
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            CountryBox.DataSource = File.ReadAllLines(Server.MapPath("~/other/countries.txt"), Encoding.UTF8);
            CountryBox.DataBind();
        }

        protected void ConfirmBtn_Click(object sender, EventArgs e)
        {
            Validate();
            if (IsValid)
            {
                var player = Player.NewPlayer(UsernameBox.Text, PasswordBox.Text, EmailBox.Text, CountryBox.Text, DateTime.Parse(DOBBox.Text));
                //if username already exists, Player.NewPlayer will return null
                if (player == null)
                {
                    UniqueUsernameValidator.FailValidatation();
                }
                else
                {
                    Sessions.LoggedIn = player;
                    Response.Redirect(ResolveUrl("~/Play.aspx"), false);
                }
            }
        }

        private async Task SetDefaultCountry()
        {
            var soapClient = new P2GeoSoapClient("IP2GeoSoap");
            // If running locally (usually when debugging), the client IP will be a local address and therefore won't reflect the client's country.
            // Instead, use a known Israeli IP address for testing.
            string ip = Request.IsLocal ? "147.235.246.139" : Request.UserHostAddress;
            CountryBox.SelectedValue = (await soapClient.ResolveIPAsync(ip, "0")).Country;
        }
    }
}