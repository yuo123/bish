﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BishUI.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="js/jquery.ba-throttle-debounce.js"></script>
    <script type="text/javascript">
        var chartResize = function () {
            $('#<%= ChartWidth.ClientID %>').val($('#<%= StatisticsChart.ClientID %>').width());
            $('#<%= ChartHeight.ClientID %>').val($('#<%= StatisticsChart.ClientID %>').height());
            <%= ClientScript.GetPostBackEventReference(ResizeChartBtn, null) %>
        };
        // Resize the chart server-side, but only after 250ms of no resize.
        $(window).resize($.debounce(250, chartResize));
        // Perform the initial resize
        $(document).ready(chartResize);
    </script>
    <div class="jumbotron">
        <h1 class="display-3">BISH - Browser Immersed Symbol Hunter</h1>
        <p class="lead">
            BISH is a <a href="https://en.wikipedia.org/wiki/MUD">
                <abbr class="initialism" title="Multi-User Dungeon">MUD</abbr></a> which allows players
            to play in randomly-generated rooms and interact with the environment and with each other.
        </p>
        <p class="lead" runat="server" id="notLoggedInContent">
            <a class="btn btn-primary btn-md" href="SignUp.aspx">Sign up</a> or <a class="btn btn-primary btn-md" href="LogIn.aspx">log in</a> to play.
        </p>
        <p class="lead" runat="server" id="loggedInContent">
            Welcome back <%= Server.HtmlEncode(Sessions.LoggedIn.Username) %>!
            <a class="btn btn-primary btn-md" href="ManagePlayer.aspx">Manage your player</a> or <a class="btn btn-primary btn-md" href="Play.aspx">play now</a>.
        </p>
    </div>
    <%-- Statistics --%>
    <div class="display-1">
        <asp:UpdatePanel ID="ChartUpdatePanel" runat="server" class="d-flex flex-row h-100 p-2">
            <ContentTemplate>
                <div class="d-flex flex-row align-items-center" style="flex: 0 0 auto">
                    <asp:DropDownList runat="server" ID="YSelector" AutoPostBack="True" CssClass="form-control" />
                </div>
                <div class="d-flex flex-column" style="flex: 1 1 auto">
                    <bishUI:ConfigurableChart ViewStateMode="Disabled" ID="StatisticsChart" runat="server" BackColor="#f0f6fc" CssClass="chart" Style="flex: 1 1">
                        <Series>
                            <bishUI:ConfigurableSeries MarkerStyle="Circle" MarkerSize="10" BorderWidth="3" IsValueShownAsLabel="true">
                                <bishUI:SeriesVariable Title="Country" ValueType="String" ValueMember="Country" ChartType="Column" AxisRestriction="X" IsGrouping="true" />
                                <bishUI:AverageSeriesVariable Title="Date of Birth" ValueType="Date" ValueMember="DOB" ChartType="Line" />
                                <bishUI:AverageSeriesVariable Title="Age" ValueType="Int32" ValueMember="Age" ChartType="Column" />
                                <bishUI:SumSeriesVariable Title="Xp" ValueType="Int32" ValueMember="Xp" ChartType="Line" />
                                <bishUI:AverageSeriesVariable Title="Base Attack" ValueType="Int32" ValueMember="BaseAttack" ChartType="Column" />
                                <bishUI:AverageSeriesVariable Title="Base Defense" ValueType="Int32" ValueMember="BaseDefense" ChartType="Column" />
                                <bishUI:AverageSeriesVariable Title="Level" ValueType="Int32" ValueMember="Level" ChartType="Column" />
                                <bishUI:AverageSeriesVariable Title="Average Enemies Killed Per Game" ValueType="Double" ValueMember="AvgEnemiesKilled" ChartType="Line" />
                                <bishUI:SumSeriesVariable Title="Total Enemies Killed" ValueType="Int32" ValueMember="TotalEnemiesKilled" ChartType="Line" />
                                <bishUI:AverageSeriesVariable Title="Average Xp Gained Per Game" ValueType="Double" ValueMember="AvgXpGained" ChartType="Line" />
                                <bishUI:SumSeriesVariable Title="Total Xp Gained" ValueType="Int32" ValueMember="TotalXpGained" ChartType="Line" />
                                <bishUI:AverageSeriesVariable Title="Average Game Duration" ValueType="Double" ValueMember="AvgDuration" ChartType="Line" IsTimeSpan="true" LabelFormat="g" />
                                <bishUI:SumSeriesVariable Title="Total Game Time" ValueType="Double" ValueMember="TotalDuration" ChartType="Line" IsTimeSpan="true" LabelFormat="g" />
                            </bishUI:ConfigurableSeries>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="Default" BackColor="#f0f6fc">
                                <AxisX Title="Date Finished" />
                            </asp:ChartArea>
                        </ChartAreas>
                        <BorderSkin PageColor="#f0f6fc" BackColor="#f0f6fc" />
                    </bishUI:ConfigurableChart>
                    <asp:DropDownList runat="server" ID="XSelector" AutoPostBack="True" CssClass="form-control" Style="flex: 0 0 auto" />
                </div>
                <%-- Hidden controls to dynamically resize the chart --%>
                <asp:Button runat="server" ID="ResizeChartBtn" CssClass="d-none" OnClick="ResizeChartBtn_Click" />
                <asp:HiddenField runat="server" ID="ChartWidth" Value="1" />
                <asp:HiddenField runat="server" ID="ChartHeight" Value="1" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
