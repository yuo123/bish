﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BishBL;

namespace BishUI
{
    public partial class Play : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (this.RequireLogin())
                AssignMovementEvents();
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (this.RequireLogin())
            {
                // Initial refresh, important for ban status
                Sessions.LoggedIn.RefreshFromDatabase(Sessions.LoggedIn.CurrentRoom);

                if (Sessions.LoggedIn.IsBanned)
                    DisplayBannedText();
                else if (Sessions.LoggedIn.CurrentGame.IsActive)
                    DisplayCurrentGame();
                else
                    DisplayNewGameButton();

                ShowInfoPanels();
            }
        }

        private void UpdatePanels()
        {
            GameUpdatePanel.Update();
            StatsUpdatePanel.Update();
            RightInfoUpdatePanel.Update();
        }

        private void AssignMovementEvents()
        {
            foreach (Button button in MovementControls.Controls.OfType<Button>())
            {
                button.Click += MovementButtonClick;
                // Cause the movement buttons to trigger an asynchronous postback rather than a normal one.
                // This is done because the buttons are only used to update an UpdatePanel.
                ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(button);
            }
        }

        private void MovementButtonClick(object sender, EventArgs e)
        {
            string direction = ((Button)sender).Text;
            int dx = 0;
            switch (direction)
            {
                case "↖":
                case "←":
                case "↙":
                    dx = -1;
                    break;
                case "↗":
                case "→":
                case "↘":
                    dx = +1;
                    break;
            }
            int dy = 0;
            switch (direction)
            {
                case "↖":
                case "↑":
                case "↗":
                    dy = -1;
                    break;
                case "↙":
                case "↓":
                case "↘":
                    dy = +1;
                    break;
            }

            MakeMove(dx, dy);
        }

        private void MakeMove(int dx, int dy)
        {
            Sessions.LoggedIn.TryMove(dx, dy);
            Sessions.LoggedIn.CommitMovement();
            UpdatePanels();
        }

        private void ShowInfoPanels()
        {
            PlayerStatsView.BindToPlayer(Sessions.LoggedIn);
            PlayerInventoryView.CurrentCharacter = Sessions.LoggedIn;
        }

        private void DisplayNewGameButton()
        {
            CurrentGameView.Visible = false;
            bannedText.Visible = false;
            newGameText.Visible = true;
            if (IsPostBack)
                Response.Redirect("Play.aspx");
        }

        private void DisplayCurrentGame()
        {
            // If a game is in progress, move enemies
            Sessions.LoggedIn.CurrentRoom.Value.RefreshEntities();
            Sessions.LoggedIn.CurrentRoom.Value.MoveEnemies();
            Sessions.LoggedIn.CurrentRoom.Value.RefreshEntities();
            Sessions.LoggedIn.RefreshFromDatabase(Sessions.LoggedIn.CurrentRoom);

            // Check again, in case the character was killed
            if (Sessions.LoggedIn.CurrentGame.IsActive)
            {
                newGameText.Visible = false;
                bannedText.Visible = false;
                CurrentGameView.Visible = true;
                CurrentGameView.CurrentRoom = Sessions.LoggedIn.CurrentRoom;
                CurrentGameView.CurrentPlayer = Sessions.LoggedIn;
            }
            else
            {
                DisplayNewGameButton();
            }
        }

        private void DisplayBannedText()
        {
            CurrentGameView.Visible = false;
            newGameText.Visible = false;
            bannedText.Visible = true;
        }

        protected void NewGameBtnClick(object sender, EventArgs e)
        {
            if (!Sessions.LoggedIn.CurrentGame.IsStarted)
            {
                Sessions.LoggedIn.NewGame();
                DisplayCurrentGame();
            }
        }

        protected void EndGameBtn_Click(object sender, EventArgs e)
        {
            if (Sessions.LoggedIn.CurrentGame.IsActive)
                Sessions.LoggedIn.FinishGame();
        }
    }
}