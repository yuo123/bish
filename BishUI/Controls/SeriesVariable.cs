﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using BishBL;

namespace BishUI
{
    /// <summary>
    /// Represents a variable which can be picked by the user to be displayed on one of the axes of a <see cref="ConfigurableChart"/>.
    /// </summary>
    public class SeriesVariable
    {
        public virtual string Title { get; set; }
        public virtual string ValueMember { get; set; }

        /// <summary>
        /// The <see cref="SeriesChartType"/> used by the chart if this variable is on the X-axis.
        /// </summary>
        public virtual SeriesChartType ChartType { get; set; }

        /// <summary>
        /// The sort order of the chart if this variable is on the X-axis.
        /// </summary>
        public PointSortOrder SortOrder { get; set; }
        public ChartValueType ValueType { get; set; }

        /// <summary>
        /// The format string for formatting values of this variable
        /// </summary>
        public string LabelFormat { get; set; }

        /// <summary>
        /// Specifies whether this variable is actually of type <see cref="TimeSpan"/>.
        /// In this case, <see cref="ValueType"/> should be set to <see cref="ChartValueType.Double"/> and <see cref="DateTime"/> values should be provided,
        /// where the given values are the actual values + <c>DateTime.FromOADate(0)</c>
        /// </summary>
        public bool IsTimeSpan { get; set; }

        /// <summary>
        /// If not null, specifies that this variable can only be selected for the given axis.
        /// </summary>
        public virtual AxisName? AxisRestriction { get; set; }

        /// <summary>
        /// When true, specifies that the Y-Variable will be grouped by this variable when it is the X-Variable.
        /// </summary>
        public virtual bool IsGrouping { get; set; }

        /// <summary>
        /// Returns the value of this variable for the given data point.
        /// </summary>
        public virtual object Eval(object dataItem)
        {
            if (IsGrouping && dataItem is IGrouping<object, object>)
                return ((IGrouping<object, object>)dataItem).Key;

            return DataBinder.Eval(dataItem, ValueMember);
        }
    }

    /// <summary>
    /// Represents a <see cref="SeriesVariable"/> which uses the average value, if there is more than one.
    /// </summary>
    public class AverageSeriesVariable : SeriesVariable
    {
        public override object Eval(object dataItem)
        {
            if (dataItem is IEnumerable)
                return ((IEnumerable)dataItem).AdaptiveAverage(this.ValueType, obj => base.Eval(obj), this.IsTimeSpan);

            return base.Eval(dataItem);
        }
    }

    /// <summary>
    /// Represents a <see cref="SeriesVariable"/> which uses the sum of all values, if there is more than one.
    /// </summary>
    public class SumSeriesVariable : SeriesVariable
    {
        public override object Eval(object dataItem)
        {
            if (dataItem is IEnumerable)
                return ((IEnumerable)dataItem).AdaptiveSum(this.ValueType, obj => base.Eval(obj), this.IsTimeSpan);

            return base.Eval(dataItem);
        }
    }
}
