﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace BishUI
{
    [ToolboxData("<{0}:ConfigurableChart runat=server></{0}:ConfigurableChart>")]
    public class ConfigurableChart : Chart
    {
        /// <summary>
        /// Determines whether the variables' titles will be displayed on the chart itself
        /// </summary>
        [DefaultValue(false)]
        public bool ShowAxisTitlesOnChart { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            // Customize chart according to configurable series.
            // If two configurable series share the same ChartArea, the last one will take precedence.
            foreach (ConfigurableSeries series in this.Series.OfType<ConfigurableSeries>())
            {
                ChartArea chartArea = this.ChartAreas.FindByNameOrDefault(series.Name);

                // For tags, see OnFormatNumber()
                chartArea.AxisX.Title = this.ShowAxisTitlesOnChart ? series.XVariable.Title : "";
                chartArea.AxisX.LabelStyle.Format = series.XVariable.LabelFormat;
                chartArea.AxisX.Tag = series.XVariable.IsTimeSpan;

                chartArea.AxisY.Title = this.ShowAxisTitlesOnChart ? series.YVariable.Title : "";
                chartArea.AxisY.LabelStyle.Format = series.YVariable.LabelFormat;
                chartArea.AxisY.Tag = series.YVariable.IsTimeSpan;
            }
            base.OnLoad(e);
        }

        protected override void OnFormatNumber(object caller, FormatNumberEventArgs e)
        {
            base.OnFormatNumber(caller, e);

            // This is a hack to display a TimeSpan in a chart. If a number needs to be formatted either:
            //  1) for an axis label, and that axis's variable has the value `true` for a tag, or
            //  2) for a data point, and the Y axis has `true` for a tag,
            // that means it should be formatted as a TimeSpan (tags are assigned in OnLoad()).
            bool timeSpan = false;
            if (caller is Axis)
            {
                object tag = ((Axis)caller).Tag;
                if (tag is bool && (bool)tag)
                    timeSpan = true;
            }
            else if (caller is DataPoint && (bool)this.ChartAreas[0].AxisY.Tag)
            {
                timeSpan = true;
            }

            // to format as a TimeSpan, we need to substruct the DateTime value from the "base date" 1970-1-1 
            // (apparently, the Chart control uses OleAutomationDate internally, because it doesn't accept `new DateTime(0)`)
            if (timeSpan)
                e.LocalizedValue = (DateTime.FromOADate(e.Value) - DateTime.FromOADate(0)).ToString(e.Format);
        }

        public void ConfigurableDataBind(IEnumerable dataSource)
        {
            foreach (ConfigurableSeries series in this.Series.OfType<ConfigurableSeries>())
            {
                series.DataSource = dataSource;
                series.DataBind();
            }
        }
    }
}
