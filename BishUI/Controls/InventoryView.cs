﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BishBL;

namespace BishUI
{
    [ToolboxData("<{0}:InventoryView runat=server></{0}:InventoryView>")]
    public class InventoryView : DataGrid
    {
        private Character _character;

        // Attributes prevent setting from markup and designer
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Character CurrentCharacter
        {
            get { return _character; }
            set
            {
                _character = value;
                this.DataSource = _character.Items;
                DataBind();
            }
        }

        public InventoryView()
        {
            // Set default view state mode to `disabled`
            this.ViewStateMode = ViewStateMode.Disabled;
            this.CssClass = "table";
            this.UseAccessibleHeader = true;
            SetDefaultColumns();
        }

        private void SetDefaultColumns()
        {
            this.AutoGenerateColumns = false;
            this.Columns.Add(new BoundColumn { HeaderText = "Name", DataField = "Name", ReadOnly = true });
            this.Columns.Add(new BoundColumn { HeaderText = "Attack Bonus", DataField = "AttackBonus", ReadOnly = true });
            this.Columns.Add(new BoundColumn { HeaderText = "Defense Bonus", DataField = "DefenseBonus", ReadOnly = true });
        }
    }
}
