﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using BishBL;

namespace BishUI
{
    [ParseChildren(true, "Variables")]
    [PersistChildren(false)]
    [ToolboxData("<{0}:ConfigurableChart runat=server></{0}:ConfigurableChart>")]
    public class ConfigurableSeries : Series
    {
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public List<SeriesVariable> Variables { get; set; }

        public ListControl XSelector { get; set; }
        public ListControl YSelector { get; set; }

        public SeriesVariable XVariable
        {
            get
            {
                if (XSelector == null)
                    throw new ArgumentNullException("XSelector", "XSelector must be set before accessing XVariable");

                return Variables.Find(v => v.ValueMember == XSelector.SelectedValue);
            }
        }

        public SeriesVariable YVariable
        {
            get
            {
                if (XSelector == null)
                    throw new ArgumentNullException("YSelector", "YSelector must be set before accessing YVariable");

                return Variables.Find(v => v.ValueMember == YSelector.SelectedValue);
            }
        }

        public IEnumerable DataSource { get; set; }

        public void DataBind()
        {
            int xIndex = XSelector.SelectedIndex.DefaultIf(-1, 0);
            int yIndex = YSelector.SelectedIndex.DefaultIf(-1, 1); // start Y from second variable, so they're different by default
            BindSelectors();
            XSelector.SelectedIndex = xIndex;
            YSelector.SelectedIndex = yIndex;
            GenerateSeries();
        }

        private void BindSelectors()
        {
            if (XSelector == null || YSelector == null)
                throw new ArgumentException("XSelector and YSelector must be set before databinding.");

            YSelector.DataTextField = XSelector.DataTextField = "Title";
            YSelector.DataValueField = XSelector.DataValueField = "ValueMember";
            XSelector.DataSource = this.Variables.Where(v => v.AxisRestriction == null || v.AxisRestriction.Value == AxisName.X);
            YSelector.DataSource = this.Variables.Where(v => v.AxisRestriction == null || v.AxisRestriction.Value == AxisName.Y);
            XSelector.DataBind();
            YSelector.DataBind();
        }

        private void GenerateSeries()
        {
            var xVariable = this.XVariable;
            var yVariable = this.YVariable;
            this.ChartType = xVariable.ChartType;

            IEnumerable ds = this.DataSource;
            if (xVariable.IsGrouping)
                ds = ds.Cast<object>().GroupBy(obj => xVariable.Eval(obj));

            foreach (object item in ds)
            {
                int index = this.Points.AddXY(xVariable.Eval(item), yVariable.Eval(item));
                this.Points[index].Tag = item;
            }

            Sort(XVariable.SortOrder, "X");
        }
    }
}
