﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BishBL;

namespace BishUI
{
    [ToolboxData("<{0}:GameView runat=server />")]
    public class GameView : WebControl
    {
        // Attributes prevent setting from markup and designer
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Room CurrentRoom { get; set; }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Player CurrentPlayer { get; set; }

        // Pre tag renders text as it appears in source (affects line breaks and font).
        protected override HtmlTextWriterTag TagKey { get { return HtmlTextWriterTag.Pre; } }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (CurrentRoom != null)
            {
                int indent = writer.Indent;
                writer.Indent = 0;
                RenderRoom(writer);
                writer.Indent = indent;
            }
        }

        public void RenderRoom(HtmlTextWriter o)
        {
            if (CurrentRoom == null)
                return;

            Dictionary<Position, IPhysicalEntity> entities = CurrentRoom.Entities.GetPositionMapping();

            for (int i = -1; i <= CurrentRoom.Length; i++)
            {
                for (int j = -1; j <= CurrentRoom.Width; j++)
                {
                    Position pos = new Position(j, i);
                    IPhysicalEntity ent;
                    if (entities.TryGetValue(pos, out ent))
                        RenderEntity(o, ent);
                    else
                        o.Write(GetEmptyChar(CurrentRoom, pos));
                }
                if (i < CurrentRoom.Length)
                    o.WriteLineNoTabs("");
            }
        }

        /// <summary>
        /// Returns the character that should be rendered when there is no entity in the given position.
        /// This could be either a wall or an empty space.
        /// </summary>
        private static char GetEmptyChar(Room room, Position pos)
        {
            bool left = pos.X == -1;
            bool top = pos.Y == -1;
            bool right = pos.X == room.Width;
            bool bottom = pos.Y == room.Length;

            // Corners
            if (left && top)
                return '┌';
            if (right && top)
                return '┐';
            if (left && bottom)
                return '└';
            if (right && bottom)
                return '┘';

            // Straight walls
            if (top || bottom)
                return '─';
            if (left || right)
                return '│';

            // Empty space
            return '\u00A0'; //non-breaking space character
        }

        private void RenderEntity(HtmlTextWriter o, IPhysicalEntity ent)
        {
            // Determine the classes that need to be applied to the rendered entity, starting with the display class.
            string classes = ent.DisplayClass.DefaultIf((string)null, "");

            // Highlight current player
            if (ent.Equals(CurrentPlayer))
                classes += " highlight";

            // Add status classes if necessary
            classes += string.Concat(ent.ConsumeStatusClasses().Select(cls => " " + cls));

            // Wrap entity in <span> if classes need to be applied
            if (classes != "")
            {
                o.AddAttribute(HtmlTextWriterAttribute.Class, classes);
                o.RenderBeginTag(HtmlTextWriterTag.Span);
            }

            o.WriteEncodedText(ent.DisplayCharacter.ToString());

            if (classes != "")
                o.RenderEndTag();
        }
    }
}
