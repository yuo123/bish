﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BishBL;

namespace BishUI
{
    /// <summary>
    /// A DetailsView pre-configured to display details about a player
    /// </summary>
    [ToolboxData("<{0}:PlayerStatsView runat=server></{0}:PlayerStatsView>")]
    public class PlayerStatsView : DetailsView
    {
        public PlayerStatsView()
        {
            AutoGenerateRows = false;
            this.Fields.Add(new BoundField { DataField = "Health", HeaderText = "Health" });
            this.Fields.Add(new BoundField { DataField = "EffectiveAttack", HeaderText = "Attack" });
            this.Fields.Add(new BoundField { DataField = "EffectiveDefense", HeaderText = "Defense" });
            this.Fields.Add(new BoundField { DataField = "Level", HeaderText = "Level" });
            this.Fields.Add(new BoundField { DataField = "Xp", HeaderText = "XP" });

            this.CssClass = "table vertical";
            this.ViewStateMode = ViewStateMode.Disabled;
        }

        public void BindToPlayer(Player player)
        {
            this.DataSource = new[] { player };
            DataBind();
        }
    }
}
