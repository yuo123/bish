﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="BishUI.LogIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="page-title">Log In</h1>
    <div class="centered-form">
        <div class="form-group row">
            <asp:Label runat="server" EnableViewState="false" AssociatedControlID="UsernameBox" CssClass="col-form-label col-sm-3" Text="Username:" />
            <asp:TextBox ID="UsernameBox" CssClass="form-control col-sm-6" runat="server" />
            <asp:RequiredFieldValidator ErrorMessage="Please provide a username" ControlToValidate="UsernameBox" runat="server"
                CssClass="invalid-feedback col-sm-3" />
        </div>
        <div class="form-group row">
            <asp:Label runat="server" EnableViewState="false" AssociatedControlID="PasswordBox" CssClass="col-form-label col-sm-3" Text="Password:" />
            <asp:TextBox ID="PasswordBox" TextMode="Password" CssClass="form-control col-sm-6" runat="server" />
            <asp:RequiredFieldValidator ErrorMessage="Please provide a password" ControlToValidate="PasswordBox" runat="server"
                CssClass="invalid-feedback col-sm-3" />
        </div>
        <div class="form-group row offset-sm-3">
            <asp:CustomValidator ErrorMessage="Incorrect username or password" ID="LoginFoundVld" runat="server" CssClass="static-invalid-feedback" />
        </div>

        <div class="offset-sm-3">
            <asp:Button ID="LogInBtn" Text="Log in" CssClass="btn btn-primary" runat="server" OnClick="LogInBtn_Click" />
            <asp:Button ID="AdminLogInBtn" Text="Log in as admin" CssClass="btn btn-secondary" runat="server" OnClick="AdminLogInBtn_Click" />
        </div>
    </div>
</asp:Content>
