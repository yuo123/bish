﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Net;
using System.Net.Sockets;

using BishBL;

namespace BishUI
{
    public static class Utility
    {
        public const string IS_INVALID_CLASS = "is-invalid";

        /// <summary>
        /// Updates all controls that have validators on the page with <see cref="IS_INVALID_CLASS"/>,
        /// and removes the class from controls that have passed validation.
        /// </summary>
        public static void UpdateBootstrapAfterValidation(this Page page)
        {
            //holds the ids of controls that have already failed at least one validator
            var alreadyFailed = new HashSet<string>();
            foreach (IValidator validator in page.Validators.Cast<IValidator>())
            {
                //all IValidators should be BaseValidators, but even if they aren't we can't do anything with them
                var baseVld = validator as BaseValidator;
                //skip controls which have already failed
                if (baseVld != null && !alreadyFailed.Contains(baseVld.ControlToValidate))
                {
                    Control validated = baseVld.GetControlToValidate();
                    if (validator.IsValid)
                    {
                        validated.RemoveClass(IS_INVALID_CLASS);
                    }
                    else
                    {
                        validated.EnsureClass(IS_INVALID_CLASS);
                        alreadyFailed.Add(validated.ID);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the control validated by the given validator, using <see cref="BaseValidator.ControlToValidate"/>
        /// </summary>
        public static Control GetControlToValidate(this BaseValidator validator)
        {
            return validator.FindControl(validator.ControlToValidate);
        }

        /// <summary>
        /// Checks whether the given control has the specified CSS class, and appends the class to it if it doesn't
        /// </summary>
        public static void EnsureClass(this Control control, string cls)
        {
            string clss = control.GetClassString();
            if (!clss.Contains(cls))
                control.SetClassString(clss + " " + cls);
        }

        /// <summary>
        /// Removes the given CSS class from the given control, if it exists
        /// </summary>
        public static void RemoveClass(this Control control, string cls)
        {
            string clss = control.GetClassString();
            control.SetClassString(Regex.Replace(clss, "(^| )" + cls + "( |$)", "", RegexOptions.Compiled));
        }

        /// <summary>
        /// Returns the CSS classes of the control in string form
        /// </summary>
        public static string GetClassString(this Control control)
        {
            if (control is WebControl)
                return ((WebControl)control).CssClass;
            return ((HtmlControl)control).Attributes["class"] ?? "";
        }

        /// <summary>
        /// Sets the CSS classes of the control in string form.
        /// For a <see cref="WebControl"/>, this method sets the <see cref="WebControl.CssClass"/> property,
        /// and for an <see cref="HtmlControl"/>, it sets the value of the "class" attribute.
        /// </summary>
        public static void SetClassString(this Control control, string value)
        {
            if (value.Length == 0)
                value = null;

            if (control is WebControl)
                ((WebControl)control).CssClass = value;
            else
                ((HtmlControl)control).Attributes["class"] = value;
        }

        /// <summary>
        /// Ends the response and redirects to the sign up page if the user is not logged in.
        /// </summary>
        /// <returns>Whether the user is logged in or not</returns>
        public static bool RequireLogin(this Page page)
        {
            return RequireLoginInternal(page, Sessions.LoggedIn != null);
        }

        /// <summary>
        /// Ends the response and redirects to the sign up page if the user is not logged in as an administrator
        /// </summary>
        /// <returns>Whether the user is logged in or not</returns>
        public static bool RequireAdminLogin(this Page page)
        {
            return RequireLoginInternal(page, Sessions.LoggedInAdmin != null);
        }

        private static bool RequireLoginInternal(Page page, bool re)
        {
            if (!re)
            {
                // Stop all events 
                // Note: the documentation is misleading, this does NOT stop events of the same page from occuring, only request events
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                // Ignore all page content
                page.Response.SuppressContent = true;
                page.Visible = false;
                // Redirect to signup
                page.Response.Redirect(page.ResolveUrl("~/SignUp.aspx"), false);
            }
            return re;
        }

        /// <summary>
        /// Causes the validator to appear as though it has failed validation, and sets <see cref="BaseValidator.IsValid"/> to <see langword="false"/>
        /// </summary>
        public static void FailValidatation(this BaseValidator validator)
        {
            validator.IsValid = false;
            Control control = validator.GetControlToValidate();
            if (control != null)
                control.EnsureClass(IS_INVALID_CLASS);
        }

        public static T FindByNameOrDefault<T>(this ChartNamedElementCollection<T> collection, string name) where T : ChartNamedElement
        {
            return collection.FindByName(name) ?? collection.FindByName("Default");
        }

        /// <summary>
        /// Computes the average value of a weakly-typed <see cref="IEnumerable"/>, 
        /// using a mapping from objects to a numeric type which matches a <see cref="ChartValueType"/>.
        /// </summary>
        /// <param name="type">
        /// The type of data <paramref name="selector"/> returns.
        /// Must be one of <see cref="ChartValueType.Double"/>, <see cref="ChartValueType.Single"/>, <see cref="ChartValueType.Int32"/>, 
        /// <see cref="ChartValueType.Int64"/>, <see cref="ChartValueType.Date"/> <see cref="ChartValueType.DateTime"/> or <see cref="ChartValueType.Time"/>.
        /// </param>
        public static object AdaptiveAverage(this IEnumerable source, ChartValueType type, Func<object, object> selector, bool isTimeSpan = false)
        {
            IEnumerable<object> en = source.Cast<object>();
            if (isTimeSpan)
                return en.Select(selector).Select(o => ((DateTime)o).ToTimeSpan()).Average(en.Count()).ToDateTime();

            switch (type)
            {
                case ChartValueType.Double: return en.Select(selector).Cast<double>().Average();
                case ChartValueType.Single: return en.Select(selector).Cast<float>().Average();
                case ChartValueType.Int32: return en.Select(selector).Cast<int>().Average();
                case ChartValueType.Int64: return en.Select(selector).Cast<long>().Average();
                default:
                    throw new InvalidOperationException("Cannot average type " + type);
            }
        }

        /// <summary>
        /// Computes the sum of a weakly-typed <see cref="IEnumerable"/>, 
        /// using a mapping from objects to a numeric type which matches a <see cref="ChartValueType"/>.
        /// </summary>
        /// <param name="type">
        /// The type of data <paramref name="selector"/> returns.
        /// Must be one of <see cref="ChartValueType.Double"/>, <see cref="ChartValueType.Single"/>, <see cref="ChartValueType.Int32"/>, 
        /// <see cref="ChartValueType.Int64"/>, <see cref="ChartValueType.Date"/> <see cref="ChartValueType.DateTime"/> or <see cref="ChartValueType.Time"/>.
        /// </param>
        public static object AdaptiveSum(this IEnumerable source, ChartValueType type, Func<object, object> selector, bool isTimeSpan = false)
        {
            IEnumerable<object> en = source.Cast<object>();
            if (isTimeSpan)
                return en.Select(selector).Select(o => ((DateTime)o).ToTimeSpan()).Sum().ToDateTime();

            switch (type)
            {
                case ChartValueType.Double: return en.Select(selector).Cast<double>().Sum();
                case ChartValueType.Single: return en.Select(selector).Cast<float>().Sum();
                case ChartValueType.Int32: return en.Select(selector).Cast<int>().Sum();
                case ChartValueType.Int64: return en.Select(selector).Cast<long>().Sum();
                default:
                    throw new InvalidOperationException("Cannot average type " + type);
            }
        }

        /// <summary>
        /// Returns the max between <paramref name="unit"/> and <paramref name="min"/>, assuming both are of the same type.
        /// </summary>
        public static Unit MinBound(this Unit unit, Unit min)
        {
            return unit.Value < min.Value ? min : unit;
        }
    }
}