﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace BishUI
{
    public partial class Main : System.Web.UI.MasterPage
    {
        /// <summary>
        /// Gets whether the client has javascript enabled (self-reported).
        /// </summary>
        public bool IsJavascriptEnabled
        {
            //see https://www.codeproject.com/Tips/1217469/How-to-detect-if-client-has-Javascript-enabled-dis
            get { return Request.Cookies["hasJs"] == null || Request.Cookies["hasJs"].Value != "false"; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ChangeNavForLogin();
        }

        /// <summary>
        /// Change navigational UI to reflect whether a player is logged in or not.
        /// </summary>
        private void ChangeNavForLogin()
        {
            if (Sessions.LoggedIn != null)
            {
                LogInLogOutLink.Text = "Log Out";
                LogInLogOutLink.NavigateUrl = ResolveUrl("~/Default.aspx") + "?Logout=yes";

                signUpNavItem.Visible = false;
            }
            else
            {
                managePlayerNavItem.Visible = false;
                usernameDisplay.Visible = false;
            }

            adminNavItem.Visible = Sessions.LoggedInAdmin != null;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (Page.Validators.Count > 0)
            {
                var script = new HtmlGenericControl("script");
                script.Attributes.Add("type", "text/javascript");
                script.Attributes.Add("src", ResolveUrl("~/js/aspnet-bootstrap-validation.js"));
                FindControl("masterBody").Controls.Add(script);

                if (IsPostBack && !IsJavascriptEnabled)
                    Page.UpdateBootstrapAfterValidation();
            }

            base.OnPreRender(e);
        }
    }
}