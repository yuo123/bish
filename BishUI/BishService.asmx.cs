﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using BishBL;
using System.IO;

namespace BishUI
{
    /// <summary>
    /// Summary description for GameHisoryService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BishService : System.Web.Services.WebService
    {
        /// <summary>
        /// Returns a list of all games of all players
        /// </summary>
        [WebMethod]
        public List<GameModel> GetAllGames()
        {
            return Game.GetAllGames().Select(g => g.ToViewModel()).ToList();
        }

        /// <summary>
        /// Returns all games of a given player
        /// </summary>
        [WebMethod]
        public List<GameModel> GetAllGamesByPlayer(string player)
        {
            return Game.GetAllGames(new RelatedPlayer(player)).Select(g => g.ToViewModel()).ToList();
        }

        /// <summary>
        /// Renders the room with the given ID to HTML and returns it
        /// </summary>
        [WebMethod]
        public string GetRenderedRoom(int roomId)
        {
            var view = new GameView();
            view.CurrentRoom = Room.FromDAL(roomId);

            var sb = new System.Text.StringBuilder();
            using (var stWriter = new StringWriter(sb))
            using (var htmlWriter = new HtmlTextWriter(stWriter))
                view.RenderRoom(htmlWriter);

            return sb.ToString();
        }

        /// <summary>
        /// Returns a list of all registered players
        /// </summary>
        [WebMethod]
        public List<PlayerModel> GetAllPlayers()
        {
            List<Player> players = Player.GetAllPlayers();
            return players.Select(player => player.ToViewModel()).ToList(players.Count);
        }

        /// <summary>
        /// Logs in with the given username and password, and makes a move on behalf of this player.
        /// </summary>
        /// <param name="dx">The amount to move horizontally, between -1 and 1.</param>
        /// <param name="dy">The amount to move vertically, between -1 and 1.</param>
        /// <returns>Whether or not the move resulted in a physical move.</returns>
        /// <exception cref="ArgumentException">If the username and password combination is invalid.</exception>
        [WebMethod]
        public bool MakeMove(string username, string password, int dx, int dy)
        {
            Player player = Player.FromLogin(username, password);
            if (player == null)
                throw new ArgumentException("Invalid username or password");

            bool re = player.TryMove(dx.Bound(-1, 1), dy.Bound(-1, 1));
            player.CommitMovement();
            return re;
        }
    }
}
