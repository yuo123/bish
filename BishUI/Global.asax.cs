﻿using BishBL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;

namespace BishUI
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            InitJQuery();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            // Auto-login the given user if the debug option is set
            string login = ConfigurationManager.AppSettings["autoLogin"];
            if (login != "")
                Sessions.LoggedIn = Player.FromDAL(login);

            // Same for admin
            login = ConfigurationManager.AppSettings["autoAdminLogin"];
            if (login != "")
            {
                Sessions.LoggedInAdmin = BishBL.Admin.FromDAL(login);
                Response.Redirect("~/Admin.aspx", false);
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            BishBL.Global.CloseDBConnection();
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        private void InitJQuery()
        {
            string jQueryVersion = JQueryInstalledVersion;
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/js/jquery-" + jQueryVersion + ".min.js",
                DebugPath = "~/js/jquery-" + jQueryVersion + ".js",
                CdnPath = "https://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + jQueryVersion + ".min.js",
                CdnDebugPath = "https://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + jQueryVersion + ".js",
                CdnSupportsSecureConnection = true,
                LoadSuccessExpression = "window.jQuery"
            });
        }

        private string JQueryInstalledVersion
        {
            get
            {
                var scriptsDir = Context.Server.MapPath("~/js/");
                foreach (var file in System.IO.Directory.EnumerateFiles(scriptsDir, "jquery-*.min.js"))
                {
                    var match = Regex.Match(file, @"(\d+(?:\.\d+){1,3})", RegexOptions.Compiled);
                    if (match.Success)
                    {
                        return match.Groups[0].Value;
                    }
                }
                throw new InvalidOperationException("jQuery is not installed in the ~/js dir");
            }
        }
    }
}