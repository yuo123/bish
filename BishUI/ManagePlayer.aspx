﻿<%@ Page Title="Manage Player" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ManagePlayer.aspx.cs" Inherits="BishUI.ManagePlayer" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="js/jquery.ba-throttle-debounce.js"></script>
    <script type="text/javascript">
        var chartResize = function () {
            $('#<%= ChartWidth.ClientID %>').val($('#<%= StatisticsChart.ClientID %>').width());
            $('#<%= ChartHeight.ClientID %>').val($('#<%= StatisticsChart.ClientID %>').height());
            <%= ClientScript.GetPostBackEventReference(ResizeChartBtn, null) %>
        };
        // Resize the chart server-side, but only after 250ms of no resize.
        $(window).resize($.debounce(250, chartResize));
        // Perform the initial resize
        $(document).ready(chartResize);
    </script>

    <h1 class="page-title">Player <%= Sessions.LoggedIn.Username %></h1>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <%-- Status --%>
                <div class="card">
                    <div class="card-header text-center">
                        Current Status
                    </div>
                    <bishUI:PlayerStatsView runat="server" ID="PlayerStatsView" />
                </div>
                <%-- Inventory --%>
                <div class="card" style="height: 400px;">
                    <div class="card-header text-center">
                        Current Inventory
                    </div>
                    <div class="card-body table-wrapper">
                        <bishUI:InventoryView runat="server" ID="PlayerInventory" />
                    </div>
                </div>
            </div>
            <div class="col-md-8 d-flex flex-column">
                <%-- Statistics --%>
                <div class="card" style="flex: 2 1;">
                    <div class="card-header text-center">
                        Game Statistics
                    </div>
                    <asp:UpdatePanel ID="ChartUpdatePanel" runat="server" class="d-flex flex-row h-100 p-2">
                        <ContentTemplate>
                            <div class="d-flex flex-row align-items-center" style="flex: 0 0 auto">
                                <asp:DropDownList runat="server" ID="YSelector" AutoPostBack="True" CssClass="form-control" />
                            </div>
                            <div class="d-flex flex-column" style="flex: 1 1 auto">
                                <bishUI:ConfigurableChart ViewStateMode="Disabled" ID="StatisticsChart" runat="server" BackColor="#f0f6fc" CssClass="chart" Style="flex: 1 1" OnClick="StatisticsChart_Click">
                                    <Series>
                                        <bishUI:ConfigurableSeries MarkerStyle="Circle" MarkerSize="10" BorderWidth="3" IsValueShownAsLabel="true" PostBackValue="#SER:#INDEX">
                                            <bishUI:SeriesVariable Title="Time Finished" ValueType="Date" ValueMember="TimeFinished" ChartType="Line" />
                                            <bishUI:SeriesVariable Title="XP Gained" ValueType="Int32" ValueMember="XpGained" ChartType="Line" />
                                            <bishUI:SeriesVariable Title="Time Started" ValueType="Date" ValueMember="TimeStarted" ChartType="Line" />
                                            <bishUI:SeriesVariable Title="Game Duration" ValueType="Double" ValueMember="Duration" IsTimeSpan="true" LabelFormat="g" ChartType="Line" />
                                            <bishUI:SeriesVariable Title="Level Started" ValueType="Int32" ValueMember="LevelStarted" ChartType="Column" />
                                            <bishUI:SeriesVariable Title="Level Finished" ValueType="Int32" ValueMember="LevelFinished" ChartType="Column" />
                                            <bishUI:SeriesVariable Title="Levels Gained" ValueType="Int32" ValueMember="LevelsGained" ChartType="Column" />
                                            <bishUI:SeriesVariable Title="Kill Count" ValueType="Int32" ValueMember="EnemiesKilledCount" ChartType="Column" />
                                        </bishUI:ConfigurableSeries>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="Default" BackColor="#f0f6fc">
                                            <AxisX Title="Date Finished" />
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    <BorderSkin PageColor="#f0f6fc" BackColor="#f0f6fc" />
                                </bishUI:ConfigurableChart>
                                <asp:DropDownList runat="server" ID="XSelector" AutoPostBack="True" CssClass="form-control" Style="flex: 0 0 auto" />
                            </div>
                            <%-- Hidden controls to dynamically resize the chart --%>
                            <asp:Button runat="server" ID="ResizeChartBtn" CssClass="d-none" OnClick="ResizeChart" />
                            <asp:HiddenField runat="server" ID="ChartWidth" />
                            <asp:HiddenField runat="server" ID="ChartHeight" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md">
                <div class="card">
                    <div class="card-header text-center">
                        Game Details
                    </div>
                    <asp:UpdatePanel ID="GameDetailsPanel" runat="server" UpdateMode="Conditional" class="card-body p-0">
                        <ContentTemplate>
                            <asp:DetailsView ViewStateMode="Disabled" ID="GameDetailsView" runat="server" ItemType="BishBL.Game" AutoGenerateRows="false" CssClass="table vertical">
                                <Fields>
                                    <asp:BoundField DataField="TimeStarted" HeaderText="Started:" />
                                    <asp:BoundField DataField="TimeFinished" HeaderText="Finished:" />
                                    <asp:BoundField DataField="XpGained" HeaderText="XP Gained:" />
                                    <asp:BoundField DataField="LevelStarted" HeaderText="Starting Level:" />
                                    <asp:BoundField DataField="LevelFinished" HeaderText="Ending Level:" />
                                    <asp:BoundField DataField="LevelsGained" HeaderText="Levels Gained:" />
                                    <asp:BoundField DataField="EnemiesKilledCount" HeaderText="Enemies Killed:" />
                                    <asp:TemplateField HeaderText="Duration">
                                        <ItemTemplate>
                                            <%# (Item.Duration - DateTime.FromOADate(0)).ToString("g") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Fields>
                                <EmptyDataTemplate>
                                    Select a point on the graph to see an individual game's details
                                </EmptyDataTemplate>
                            </asp:DetailsView>
                            <div class="px-2 table-wrapper" style="max-height: 400px">
                                <h4 enableviewstate="false" runat="server" id="killedEnemiesTitle" visible="false" class="ml-2">Killed Enemies</h4>
                                <asp:GridView runat="server" ID="KilledEnemiesView" ItemType="BishBL.KilledEnemyType" AutoGenerateColumns="False" CssClass="table">
                                    <Columns>
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField DataField="Level" HeaderText="Level" />
                                        <asp:BoundField DataField="StartingHealth" HeaderText="Health" />
                                        <asp:BoundField DataField="Attack" HeaderText="Attack" />
                                        <asp:BoundField DataField="Defense" HeaderText="Defense" />
                                        <asp:BoundField DataField="KilledCount" HeaderText="Count" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
