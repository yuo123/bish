﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BishBL;
using AdminBL = BishBL.Admin;

namespace BishUI
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            this.RequireAdminLogin();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Only show admins to SuperAdmins
            AdminsView.Visible = adminsViewRow.Visible = Sessions.LoggedInAdmin.PermissionLevel == AdminPermissionLevel.SuperAdmin;
            if (!IsPostBack)
            {
                BindEnemyTypesView();
                BindAdminsView();
            }
            UsersDropDown.DataSource = Player.GetPlayerNames();
            UsersDropDown.DataBind();
        }

        #region EnemyTypesView
        private void BindEnemyTypesView(bool newRow = false)
        {
            IEnumerable<object> allTypes = EnemyType.GetAllEnemyTypes();
            if (newRow)
            {
                EnemyTypesView.EditIndex = ((IList<EnemyType>)allTypes).Count;
                // Append an anonymous object to the list, with the same properties as EnemyType containing default values
                allTypes = allTypes.Concat(new object[] { new { Id = -1, Attack = "", Defense = "", DisplayCharacter = "", Level = "", Name = "", StartingHealth = "" } });
            }

            EnemyTypesView.DataSource = allTypes;
            EnemyTypesView.DataBind();
        }

        protected void EnemyTypesView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            EnemyTypesView.EditIndex = e.NewEditIndex;
            BindEnemyTypesView();
            AdminsView.EditIndex = -1;
            BindAdminsView();
        }

        protected void EnemyTypesView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            EnemyTypesView.EditIndex = -1;
            BindEnemyTypesView();
        }

        protected void EnemyTypesView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (!IsValid)
                return;

            // Put all the new values into strongly-typed variables
            string name = (string)e.NewValues["Name"];
            int level = int.Parse((string)e.NewValues["Level"]);
            int health = int.Parse((string)e.NewValues["StartingHealth"]);
            int attack = int.Parse((string)e.NewValues["Attack"]);
            int defense = int.Parse((string)e.NewValues["Defense"]);
            char displayChar = ((string)e.NewValues["DisplayCharacter"]).Single();

            int id = (int)e.Keys["Id"];
            // If it's a new type, create it; otherwise just update
            if (id == -1)
                new EnemyType(name, level, health, attack, defense, displayChar);
            else
                new EnemyType(id).Update(name, level, health, attack, defense, displayChar);

            EnemyTypesView.EditIndex = -1;
            BindEnemyTypesView();
        }

        protected void AddET_Click(object sender, EventArgs e)
        {
            BindEnemyTypesView(true);
        }
        #endregion

        #region AdminsView
        private void BindAdminsView(bool newRow = false)
        {
            if (!AdminsView.Visible)
                return;

            IEnumerable<object> allAdmins = AdminBL.GetAllAdmins();
            if (newRow)
            {
                AdminsView.EditIndex = ((IList<AdminBL>)allAdmins).Count;
                // Append an anonymous object to the list, with the same properties as AdminBL containing default values
                allAdmins = allAdmins.Concat(new object[] { new { Username = "", PermissionLevel = AdminPermissionLevel.Regular } });
            }

            AdminsView.DataSource = allAdmins;
            AdminsView.DataBind();
        }

        protected void AdminsView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            AdminsView.EditIndex = e.NewEditIndex;
            BindAdminsView();
            EnemyTypesView.EditIndex = -1;
            BindEnemyTypesView();
        }

        protected void AdminsView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            AdminsView.EditIndex = -1;
            BindAdminsView();
        }

        protected void AdminsView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (!IsValid)
                return;

            GridViewRow editedRow = AdminsView.Rows[e.RowIndex];
            var oldUsername = (string)e.Keys["Username"];
            var newUsername = (string)e.NewValues["Username"];

            // Check for duplicate username
            if (newUsername != oldUsername && AdminBL.IsUsernameTaken(newUsername))
            {
                ((BaseValidator)editedRow.FindControl("UsernameTakenVld")).FailValidatation();
                return;
            }

            var password = ((TextBox)editedRow.FindControl("AdPassBox")).Text;
            var permLevel = (AdminPermissionLevel)Enum.Parse(typeof(AdminPermissionLevel), (string)e.NewValues["PermissionLevel"]);

            // If it's an existing admin, update; otherwise create a new one
            if (oldUsername != "")
            {
                AdminBL admin = AdminBL.FromDAL(oldUsername);
                // Don't change password if the field was left empty
                if (password != "")
                    admin.ChangePassword(password);
                admin.Username = newUsername;
                admin.PermissionLevel = permLevel;
            }
            else
            {
                new AdminBL(newUsername, password, permLevel);
            }
            AdminsView.EditIndex = -1;
            BindAdminsView();
        }

        protected void AdminsView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            AdminBL.FromDAL((string)e.Keys["Username"]).Delete();
            BindAdminsView();
        }

        protected void AddAdmin_Click(object sender, EventArgs e)
        {
            BindAdminsView(true);

            GridViewRow row = AdminsView.Rows[AdminsView.EditIndex];
            ((WebControl)row.FindControl("AdPasswdVld")).Enabled = true;
        }

        protected void AdminsView_PreRender(object sender, EventArgs e)
        {
            // If AdminsView is in edit mode for adding a new row, remove the password box's "placeholder" attribute saying it can be left blank
            if (AdminsView.EditIndex != -1)
            {
                GridViewRow row = AdminsView.Rows[AdminsView.EditIndex];
                // The validator's Enabled property is saved in ViewState, so it can be used to tell if the password is required
                if (((WebControl)row.FindControl("AdPasswdVld")).Enabled)
                    ((WebControl)row.FindControl("AdPassBox")).Attributes.Remove("placeholder");
            }
        }
        #endregion

        protected void LogOut_Click(object sender, EventArgs e)
        {
            Sessions.LoggedInAdmin = null;
            Response.Redirect("~/Default.aspx");
        }

        protected void UsersDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            BanBtn.Text = Player.FromDAL(UsersDropDown.SelectedValue).IsBanned ? "Unban" : "Ban";
        }

        protected void BanBtn_Click(object sender, EventArgs e)
        {
            Player player = Player.FromDAL(UsersDropDown.SelectedValue);
            player.IsBanned = !player.IsBanned;
            BanBtn.Text = player.IsBanned ? "Unban" : "Ban";
        }
    }
}