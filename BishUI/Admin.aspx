﻿<%@ Page Title="Admin Page" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="BishUI.Admin" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button Text="Log out admin" runat="server" CssClass="btn btn-link" OnClick="LogOut_Click" />
    <h1 class="page-title">Admin <%= Sessions.LoggedInAdmin.Username %></h1>

    <div class="row mb-5">
        <div class="col">
            <h3>Enemy Types</h3>
            <asp:GridView runat="server" ID="EnemyTypesView" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="editable table"
                OnRowEditing="EnemyTypesView_RowEditing" OnRowCancelingEdit="EnemyTypesView_RowCancelingEdit" OnRowUpdating="EnemyTypesView_RowUpdating">
                <Columns>
                    <asp:TemplateField HeaderText="Name">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" Text='<%# Bind("Name") %>' ID="ETNameBox" />
                            <asp:RequiredFieldValidator CssClass="invalid-feedback" Display="None" runat="server" ControlToValidate="ETNameBox" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="L1" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" Text='<%# Bind("Level") %>' ID="ETLevelBox" TextMode="Number" />
                            <asp:RequiredFieldValidator CssClass="invalid-feedback" Display="None" runat="server" ControlToValidate="ETLevelBox" />
                            <asp:CompareValidator CssClass="invalid-feedback" Display="Dynamic" ErrorMessage="Level must be positive" ControlToValidate="ETLevelBox" runat="server" ValueToCompare="0" Type="Integer" Operator="GreaterThan" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="L2" runat="server" Text='<%# Bind("Level") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Health">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" Text='<%# Bind("StartingHealth") %>' ID="ETHealthBox" TextMode="Number" />
                            <asp:RequiredFieldValidator CssClass="invalid-feedback" Display="None" runat="server" ControlToValidate="ETHealthBox" />
                            <asp:CompareValidator CssClass="invalid-feedback" Display="Dynamic" ErrorMessage="Starting health must be positive" ControlToValidate="ETHealthBox" runat="server" ValueToCompare="0" Type="Integer" Operator="GreaterThan" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="L3" runat="server" Text='<%# Bind("StartingHealth") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Attack">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" Text='<%# Bind("Attack") %>' ID="ETAttackBox" TextMode="Number" />
                            <asp:RequiredFieldValidator CssClass="invalid-feedback" Display="None" runat="server" ControlToValidate="ETAttackBox" />
                            <asp:CompareValidator CssClass="invalid-feedback" Display="Dynamic" ErrorMessage="Attack power must be positive" ControlToValidate="ETAttackBox" runat="server" ValueToCompare="0" Type="Integer" Operator="GreaterThan" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="L4" runat="server" Text='<%# Bind("Attack") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Defense">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" Text='<%# Bind("Defense") %>' ID="ETDefenseBox" TextMode="Number" />
                            <asp:RequiredFieldValidator CssClass="invalid-feedback" Display="None" runat="server" ControlToValidate="ETDefenseBox" />
                            <asp:CompareValidator CssClass="invalid-feedback" Display="Dynamic" ErrorMessage="Defense power must be positive" ControlToValidate="ETDefenseBox" runat="server" ValueToCompare="0" Type="Integer" Operator="GreaterThan" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="L5" runat="server" Text='<%# Bind("Defense") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Display Character">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" Text='<%# Bind("DisplayCharacter") %>' ID="ETDisplayCHar" MaxLength="1" Columns="1" />
                            <asp:RequiredFieldValidator CssClass="invalid-feedback" Display="None" runat="server" ControlToValidate="ETDisplayCHar" />
                            <asp:RegularExpressionValidator CssClass="invalid-feedback" Display="Dynamic" runat="server" ControlToValidate="ETDisplayCHar" ValidationExpression="[a-z]" ErrorMessage="Display character must be exactly one lower-case letter" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="L6" runat="server" Text='<%# Bind("DisplayCharacter") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="true" EditText="Edit" CancelText="Cancel" UpdateText="Confirm" />
                </Columns>
            </asp:GridView>
            <asp:Button Text="Add new enemy type" runat="server" OnClick="AddET_Click" CssClass="btn btn-outline-light" />
        </div>
    </div>

    <div class="row" id="adminsViewRow" runat="server">
        <div class="col">
            <h3>Administrators</h3>
            <asp:GridView runat="server" ID="AdminsView" AutoGenerateColumns="false" DataKeyNames="Username" CssClass="editable table"
                OnRowEditing="AdminsView_RowEditing" OnRowCancelingEdit="AdminsView_RowCancelingEdit" OnRowUpdating="AdminsView_RowUpdating"
                OnRowDeleting="AdminsView_RowDeleting" OnPreRender="AdminsView_PreRender">
                <Columns>
                    <asp:TemplateField HeaderText="Username">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" Text='<%# Bind("Username") %>' ID="AdUsernameBox"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="AdUsernameBox" Display="None" CssClass="invalid-feedback" />
                            <asp:CustomValidator ErrorMessage="Username taken" ControlToValidate="AdUsernameBox" runat="server"
                                CssClass="invalid-feedback" Display="Dynamic" ID="UsernameTakenVld" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Bind("Username") %>' ID="L7"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Permissions">
                        <EditItemTemplate>
                            <asp:DropDownList runat="server" ID="AdPermBox" CssClass="form-control"
                                DataSource='<%# Enum.GetNames(typeof(BishBL.AdminPermissionLevel)) %>'
                                SelectedValue='<%# Bind("PermissionLevel") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Bind("PermissionLevel") %>' ID="L8"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Password">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" placeholder="Leave empty to have password unchanged" TextMode="Password" ID="AdPassBox" />
                            <asp:RequiredFieldValidator ID="AdPasswdVld" runat="server" ControlToValidate="AdPassBox" Display="None" CssClass="invalid-feedback" Enabled="false" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label Text="****" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="true" EditText="Change" ShowDeleteButton="true" DeleteText="Remove" UpdateText="Confirm" CancelText="Cancel" />
                </Columns>
            </asp:GridView>
            <asp:Button Text="Add new admin" runat="server" OnClick="AddAdmin_Click" CssClass="btn btn-outline-light" />
        </div>
    </div>

    <div class="row mt-5 form-group">
        <h3 class="w-100">Ban/Unban Players</h3>
        <asp:Label Text="Username:" CssClass="form-control-label col-3" runat="server" />
        <asp:DropDownList runat="server" ID="UsersDropDown" AutoPostBack="true" CssClass="form-control-chosen col-8"
            OnSelectedIndexChanged="UsersDropDown_SelectedIndexChanged" />
        <asp:UpdatePanel runat="server" class="col-1" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ViewStateMode="Disabled" Text="Ban" ID="BanBtn" runat="server" CssClass="btn btn-outline-light" OnClick="BanBtn_Click" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="UsersDropDown" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
