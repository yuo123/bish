﻿<%@ Page Title="Sign Up" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="BishUI.SignUp" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="page-title">Sign Up</h1>
    <div class="centered-form sign-up">
        <div class="form-row">
            <asp:Label runat="server" EnableViewState="false" AssociatedControlID="UsernameBox" Text="Username:" />
            <asp:TextBox ID="UsernameBox" runat="server" />
            <asp:RequiredFieldValidator ErrorMessage="Please provide a username" ControlToValidate="UsernameBox" runat="server" Display="Dynamic" />
            <asp:CustomValidator ID="UniqueUsernameValidator" ErrorMessage="Username taken" ControlToValidate="UsernameBox" runat="server" Display="Dynamic" />
        </div>
        <div class="form-row">
            <asp:Label runat="server" EnableViewState="false" AssociatedControlID="PasswordBox" Text="Password:" />
            <asp:TextBox ID="PasswordBox" TextMode="Password" runat="server" />
            <asp:RequiredFieldValidator ErrorMessage="Please provide a password" ControlToValidate="PasswordBox" runat="server" />
        </div>
        <div class="form-row">
            <asp:Label runat="server" EnableViewState="false" AssociatedControlID="ConfirmPasswordBox" Text="Confirm Password:" />
            <asp:TextBox ID="ConfirmPasswordBox" TextMode="Password" runat="server" />
            <asp:RequiredFieldValidator ErrorMessage="Please re-enter your password" ControlToValidate="ConfirmPasswordBox" runat="server" Display="Dynamic" />
            <asp:CompareValidator ErrorMessage="The two passwords must match" ControlToValidate="ConfirmPasswordBox" ControlToCompare="PasswordBox" runat="server" Display="Dynamic" />
        </div>
        <div class="form-row">
            <asp:Label runat="server" EnableViewState="false" AssociatedControlID="EmailBox" Text="Email:" />
            <asp:TextBox ID="EmailBox" TextMode="Email" runat="server" />
            <asp:RequiredFieldValidator ErrorMessage="Please provide an email address" ControlToValidate="EmailBox" runat="server" Display="Dynamic" />
            <asp:RegularExpressionValidator runat="server" ErrorMessage="Invalid email address" ControlToValidate="EmailBox" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
        </div>
        <div class="form-row">
            <asp:Label runat="server" EnableViewState="false" AssociatedControlID="CountryBox" Text="Country:" />
            <asp:DropDownList ID="CountryBox" runat="server" CssClass="form-control-chosen" />
        </div>
        <div class="form-row">
            <asp:Label runat="server" EnableViewState="false" AssociatedControlID="DOBBox" Text="Date of Birth:" />
            <asp:TextBox ID="DOBBox" TextMode="Date" runat="server" />
            <asp:RequiredFieldValidator ErrorMessage="Please enter your date of birth" ControlToValidate="DOBBox" runat="server" Display="Dynamic" />
            <%-- ValueToCompare set in behind-code --%>
            <asp:CompareValidator runat="server" ErrorMessage="Invalid date" ControlToValidate="DOBBox" Operator="LessThan" Type="Date" Display="Dynamic" />
        </div>

        <asp:Button ID="ConfirmBtn" Text="Confirm" CssClass="btn btn-primary" runat="server" OnClick="ConfirmBtn_Click" />
    </div>
</asp:Content>
