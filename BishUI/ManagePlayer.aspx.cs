﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BishBL;

namespace BishUI
{
    public partial class ManagePlayer : System.Web.UI.Page
    {
        private Game _selectedGame = null;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            this.RequireLogin();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.RequireLogin())
            {
                PlayerStatsView.BindToPlayer(Sessions.LoggedIn);
                PlayerInventory.CurrentCharacter = Sessions.LoggedIn;
                PlayerInventory.DataBind();
                LoadChart();

                if (!IsPostBack)
                    // Cause EmptyItemTemplate to show up
                    GameDetailsView.DataBind();
            }
        }

        private void LoadChart()
        {
            var series = (ConfigurableSeries)StatisticsChart.Series[0];
            series.XSelector = XSelector;
            series.YSelector = YSelector;
            StatisticsChart.ConfigurableDataBind(Game.GetAllFinishedGames(new RelatedPlayer(Sessions.LoggedIn)));
            StatisticsChart.Width = Unit.Parse(ChartWidth.Value);
            StatisticsChart.Height = Unit.Parse(ChartHeight.Value);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (_selectedGame != null)
            {
                GameDetailsView.DataSource = new[] { _selectedGame };
                GameDetailsView.DataBind();
                KilledEnemiesView.DataSource = _selectedGame.EnemiesKilled;
                KilledEnemiesView.DataBind();
                killedEnemiesTitle.Visible = true;
                GameDetailsPanel.Update();
            }
        }

        protected void ResizeChart(object sender, EventArgs e)
        {
        }

        protected void StatisticsChart_Click(object sender, ImageMapEventArgs e)
        {
            string[] point = e.PostBackValue.Split(':');
            string series = point[0];
            int index = int.Parse(point[1]);

            _selectedGame = (Game)StatisticsChart.Series[series].Points[index].Tag;
        }
    }
}