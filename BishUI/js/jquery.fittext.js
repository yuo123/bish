// The purpose of this script is to resize the text of display: inline-block elements to fit their container.
// The font-size is set so that the inline-block element is as big as possible, while still being in full view.

(function ($) {

    $.fn.fitText = function () {
        return this.each(function () {

            // Store the object, because `this` will change once we're in the resizer function.
            var $this = $(this);

            var resizer = function () {
                var fontSize = parseFloat($this.css('font-size'));
                var byWidth = $this.parent().width() / $this.width();
                var byHeight = $this.parent().height() / $this.height();
                $this.css('font-size', Math.min(byWidth, byHeight) * fontSize);
            };

            // Call once to set.
            resizer();

            // Call on resize. Opera debounces their resize by default.
            $(window).on('resize.fittext orientationchange.fittext', resizer);

        });

    };

})(jQuery);
