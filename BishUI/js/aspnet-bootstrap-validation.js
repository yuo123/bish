// Adapted from https://gist.github.com/meziantou/1755cd2d21c8a1d1d148

function extendedValidatorUpdateDisplay(val) {
    // Call the original function
    if (typeof originalValidatorUpdateDisplay === "function") {
        originalValidatorUpdateDisplay(val);
    }
    // Update the control with the .is-invalid class if validation was unsuccessful
    var control = document.getElementById(val.controltovalidate);
    if (control) {
        // Only remove invalid status if no validator for this control was checked before
        if (typeof control.isValid === "undefined" || !control.isValid) {
            control.isValid = true;
            $(control).removeClass("is-invalid");
        }

        // If at least one failed validator was found, set the invalid status
        if (!val.isvalid) {
            control.isValid = false;
            $(control).addClass("is-invalid");
        }
    }
}
// Replace the ValidatorUpdateDisplay function
var originalValidatorUpdateDisplay = window.ValidatorUpdateDisplay;
window.ValidatorUpdateDisplay = extendedValidatorUpdateDisplay;