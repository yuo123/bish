﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BishBL;

namespace BishUI
{
    public partial class LogIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LogInBtn_Click(object sender, EventArgs e)
        {
            var player = Player.FromLogin(UsernameBox.Text, PasswordBox.Text);
            if (player == null)
            {
                LoginFoundVld.FailValidatation();
            }
            else
            {
                Sessions.LoggedIn = player;
                Response.Redirect(ResolveUrl("~/Play.aspx"), false);
            }
        }

        protected void AdminLogInBtn_Click(object sender, EventArgs e)
        {
            var admin = BishBL.Admin.FromLogin(UsernameBox.Text, PasswordBox.Text);
            if (admin == null)
            {
                LoginFoundVld.FailValidatation();
            }
            else
            {
                Sessions.LoggedInAdmin = admin;
                Response.Redirect(ResolveUrl("~/Admin.aspx"), false);
            }
        }
    }
}