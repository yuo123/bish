﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BishBL;

namespace BishUI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HandleLogout();
            ChangeLoggedInMessage();
            LoadChart();
        }

        private void LoadChart()
        {
            var series = (ConfigurableSeries)StatisticsChart.Series[0];
            series.XSelector = XSelector;
            series.YSelector = YSelector;
            StatisticsChart.ConfigurableDataBind(new BishWebService.BishServiceSoapClient("BishServiceSoap").GetAllPlayers());
            StatisticsChart.Width = Unit.Parse(ChartWidth.Value).MinBound(new Unit(1));
            StatisticsChart.Height = Unit.Parse(ChartHeight.Value).MinBound(new Unit(1));
        }

        private void HandleLogout()
        {
            if (Request.QueryString["Logout"] == "yes")
                Sessions.LoggedIn = null;
        }

        private void ChangeLoggedInMessage()
        {
            notLoggedInContent.Visible = !(loggedInContent.Visible = Sessions.LoggedIn != null);
        }

        protected void ResizeChartBtn_Click(object sender, EventArgs e)
        {

        }
    }
}