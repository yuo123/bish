﻿<%@ Page Title="Play" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Play.aspx.cs" Inherits="BishUI.Play" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/playStyle.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="js/jquery.fittext.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Apply fit-text script to adjust the size of the game view and movement controls.
            // See jquery.fittext.js for details.
            $('.movement-controls').fitText();
            $('#<%= CurrentGameView.ClientID %>').fitText();
        })
        // Re-apply the script on every update of the UpdatePanel, because the element is replaced entirely.
        // The parameter to add_endRequest is called after every postback.
        // Reference: http://www.dotnetcurry.com/ShowArticle.aspx?ID=256
        Sys.Application.add_init(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                $('#<%= CurrentGameView.ClientID %>').fitText();
            })
        })
    </script>
    <div class="row h-100 flex-lg-row flex-column">
        <div class="col-lg">
            <div class="row">
                <div class="col">
                    <%-- Player stats --%>
                    <asp:UpdatePanel runat="server" ID="StatsUpdatePanel" UpdateMode="Conditional" class="card">
                        <ContentTemplate>
                            <div class="card-header text-center">
                                Status
                            </div>
                            <bishUI:PlayerStatsView runat="server" ID="PlayerStatsView"></bishUI:PlayerStatsView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="row">
                <div class="align-self-center ml-3">
                    <button type="button" class="btn btn-info d-block d-lg-none" data-toggle="modal" data-target="#legendModal">Legend</button>
                    <asp:Button Text="End Game" runat="server" OnClick="EndGameBtn_Click" CssClass="btn btn-secondary d-block" />
                </div>
                <div class="col p-1">
                    <%-- Movement controls --%>
                    <asp:Panel runat="server" ID="MovementControls" CssClass="container movement-controls">
                        <div class="d-table-row">
                            <asp:Button Text="↖" runat="server" />
                            <asp:Button Text="↑" runat="server" />
                            <asp:Button Text="↗" runat="server" />
                        </div>
                        <div class="d-table-row">
                            <asp:Button Text="←" runat="server" />
                            <asp:Button Text="⚫" runat="server" />
                            <asp:Button Text="→" runat="server" />
                        </div>
                        <div class="d-table-row">
                            <asp:Button Text="↙" runat="server" />
                            <asp:Button Text="↓" runat="server" />
                            <asp:Button Text="↘" runat="server" />
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div class="col-lg-5 text-center game-column">
            <div class="d-flex justify-content-center align-items-center h-100 w-100" runat="server" id="newGameText">
                <p class="d-block text-center">
                    You have no game in progress.
                    <br />
                    <asp:Button Text="Start a new game" CssClass="btn btn-primary mt-1" runat="server" OnClick="NewGameBtnClick" />
                </p>
            </div>
            <div class="d-flex justify-content-center align-items-center h-100 w-100" runat="server" id="bannedText">
                <p class="d-block text-center text-danger">
                    You are currently banned by an administrator, and cannot play.
                </p>
            </div>
            <asp:UpdatePanel runat="server" ID="GameUpdatePanel" UpdateMode="Conditional" class="container-fluid h-100 m-0 p-0 game-view">
                <ContentTemplate>
                    <bishUI:GameView runat="server" ID="CurrentGameView" ViewStateMode="Disabled" CssClass="d-inline-block w-auto h-auto m-0" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel runat="server" ID="RightInfoUpdatePanel" UpdateMode="Conditional" class="col-lg">
            <ContentTemplate>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header text-center">
                                Inventory
                            </div>
                            <div class="card-body table-wrapper">
                                <bishUI:InventoryView runat="server" ID="PlayerInventoryView" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row d-none d-lg-block">
                    <div class="col">
                        <div class="card">
                            <div class="card-header text-center">
                                Legend
                            </div>
                            <ul class="list-group list-group-flush game-legend">
                                <li class="list-group-item item">Item</li>
                                <li class="list-group-item enemy">Enemy</li>
                                <li class="list-group-item player">Player</li>
                                <li class="list-group-item player highlight">You</li>
                                <li class="list-group-item">Doors (<span class="generic-entity">+</span>) and walls (<span class="generic-entity">│</span>)</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%-- Legend modal --%>
    <div class="modal" id="legendModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Legend</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="list-group list-group-flush game-legend">
                        <li class="list-group-item item">Item</li>
                        <li class="list-group-item enemy">Enemy</li>
                        <li class="list-group-item player">Player</li>
                        <li class="list-group-item player highlight">You</li>
                        <li class="list-group-item">Doors (<span class="generic-entity">+</span>) and walls (<span class="generic-entity">│</span>)</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
