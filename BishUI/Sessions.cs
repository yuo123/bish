﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

using BishBL;
using AdminBL = BishBL.Admin;

namespace BishUI
{
    /// <summary>
    /// Provides strongly-typed objects stored in session state
    /// </summary>
    public static class Sessions
    {
        private static HttpSessionState Session { get { return HttpContext.Current.Session; } }

        /// <summary>
        /// The currently logged-in player, or null if browsing anonymously
        /// </summary>
        public static Player LoggedIn
        {
            get { return (Player)Session["LoggedIn"]; }
            set { Session["LoggedIn"] = value; }
        }

        /// <summary>
        /// The currently logged-in admin, or null if no admin is logged in
        /// </summary>
        public static AdminBL LoggedInAdmin
        {
            get { return (AdminBL)Session["LoggedInAdmin"]; }
            set { Session["LoggedInAdmin"] = value; }
        }
    }
}