﻿using System;
using System.Data;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BishDAL.Tests
{
    [TestClass]
    public class EnemyTypesTests
    {
        private const int LVL = 1, HEALTH = 2, ATK = 3, DEF = 4;
        private const string NAME = "Test EnemyType";
        private const char CHAR = 'e';
        private readonly object[] props = { 0, LVL, HEALTH, ATK, DEF, NAME, CHAR.ToString() };

        [TestMethod]
        public void New_enemy_type_has_supplied_parameters()
        {
            int typeId = EnemyTypesDAL.AddEnemyType(LVL, HEALTH, ATK, DEF, NAME, CHAR);
            props[0] = typeId;

            try
            {
                DataRow enemyTypeData = EnemyTypesDAL.GetEnemyType(typeId);
                CollectionAssert.AreEquivalent(enemyTypeData.ItemArray, props);
            }
            finally
            {
                OleDbHelper.Delete(EnemyTypesDAL.TBL_NAME, "enemyTypeId", typeId);
            }
        }

        [TestMethod]
        public void New_enemy_type_included_in_all_enemies()
        {
            int typeId = EnemyTypesDAL.AddEnemyType(LVL, HEALTH, ATK, DEF, NAME, CHAR);
            props[0] = typeId;

            try
            {
                DataTable allEnemies = EnemyTypesDAL.GetAllEnemies(LVL);
                DataAssert.ColumnContains(allEnemies, "enemyTypeId", typeId);
            }
            finally
            {
                OleDbHelper.Delete(EnemyTypesDAL.TBL_NAME, "enemyTypeId", typeId);
            }
        }

        [TestMethod]
        public void New_enemy_type_not_included_in_all_enemies_of_another_level()
        {
            int typeId = EnemyTypesDAL.AddEnemyType(LVL, HEALTH, ATK, DEF, NAME, CHAR);
            props[0] = typeId;

            try
            {
                DataTable allEnemies = EnemyTypesDAL.GetAllEnemies(LVL + 1);
                DataAssert.ColumnDoesNotContain(allEnemies, "enemyTypeId", typeId);
            }
            finally
            {
                OleDbHelper.Delete(EnemyTypesDAL.TBL_NAME, "enemyTypeId", typeId);
            }
        }
        
        [DataTestMethod]
        [DataRow(1)]
        [DataRow(5)]
        [DataRow(int.MaxValue)]
        [ExpectedException(typeof(ArgumentOutOfRangeException), AllowDerivedTypes = true)]
        public void GetAllEnemies_throws_on_invalid_level(int diff)
        {
            EnemyTypesDAL.GetAllEnemies(PlayersDAL.STARTING_LEVEL - diff);
        }
    }
}
