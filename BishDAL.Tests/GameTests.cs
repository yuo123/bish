﻿using System;
using System.Data;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BishDAL.Tests
{
    [TestClass]
    public class GameTests
    {
        private const string EXISTING_USER = "a";
        private const int EXISTING_ENEMY_TYPE = 8;
        private const int DIFFERENT_ENEMY_TYPE = 9;

        private static int gameId;

        [ClassInitialize]
        public static void CreateTestGame(TestContext c)
        {
            // Cleanup existing game that could mess up with test
            OleDbHelper.Delete(GamesDAL.TBL_NAME, "playerUsername, timeFinished", EXISTING_USER, null);
            GamesDAL.NewGame(EXISTING_USER);
            DataRow dr = GamesDAL.GetUnfinishedGame(EXISTING_USER);
            Assert.IsNotNull(dr);
            gameId = (int)dr["gameId"];
        }

        [TestMethod]
        public void New_game_can_be_restrieved_with_GetUnfinishedGame()
        {
            //this is tested by [TestInitialize]CreateGame()
        }

        [TestMethod]
        public void New_game_has_correct_data()
        {
            DataRow gameData = GamesDAL.GetGameDetails(gameId);
            DataRow playerData = PlayersDAL.GetPlayerDetails(EXISTING_USER);

            Assert.AreEqual(EXISTING_USER, gameData["playerUsername"]);
            DateAssert.IsNewlyCreated(gameData["timeStarted"]);
            DataAssert.IsDBNull(gameData["timeFinished"]);
            Assert.AreEqual(0, gameData["xpGained"]);
            Assert.AreEqual(playerData["level"], gameData["levelStarted"]);
            DataAssert.IsDBNull(gameData["levelFinished"]);
        }

        [TestMethod]
        public void New_game_included_in_all_games()
        {
            DataTable allGames = GamesDAL.GetAllGames();
            DataAssert.ColumnContains(allGames, "gameId", gameId);
            allGames = GamesDAL.GetAllGames(EXISTING_USER);
            DataAssert.ColumnContains(allGames, "gameId", gameId);
        }

        [TestMethod]
        public void New_game_not_included_in_all_games_of_a_different_player()
        {
            DataTable somePlayersGames = GamesDAL.GetAllGames("somePlayer");
            DataAssert.ColumnDoesNotContain(somePlayersGames, "gameId", gameId);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(5)]
        [DataRow(-1)]
        [DataRow(0)]
        public void Adding_xp_results_in_expected_value(int amount)
        {
            DataRow dr = GamesDAL.GetGameDetails(gameId);
            int before = (int)dr["xpGained"];

            GamesDAL.AddXp(gameId, amount);

            dr = GamesDAL.GetGameDetails(gameId);
            Assert.AreEqual(before + amount, (int)dr["xpGained"]);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(5)]
        [DataRow(-1)]
        [DataRow(0)]
        public void Adding_xp_multiple_times_results_in_expected_value(int amount)
        {
            DataRow dr = GamesDAL.GetGameDetails(gameId);
            int before = (int)dr["xpGained"];

            GamesDAL.AddXp(gameId, amount);
            GamesDAL.AddXp(gameId, amount);

            dr = GamesDAL.GetGameDetails(gameId);
            Assert.AreEqual(before + (amount * 2), (int)dr["xpGained"]);
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException), AllowDerivedTypes = true)]
        public void Overflowing_xp_throws()
        {
            GamesDAL.AddXp(gameId, int.MaxValue);
            GamesDAL.AddXp(gameId, 2);
        }

        [TestMethod]
        public void Finished_game_has_correct_data()
        {
            GamesDAL.FinishGame(gameId);
            DataRow gameData = GamesDAL.GetGameDetails(gameId);
            DataRow playerData = PlayersDAL.GetPlayerDetails(EXISTING_USER);

            Assert.AreEqual(playerData["level"], gameData["levelFinished"]);
            DateAssert.IsNewlyCreated(gameData["timeFinished"]);
        }

        [TestMethod]
        public void Added_killed_enemy_appears_in_killed_enemies_table()
        {
            GamesDAL.AddKilledEnemy(gameId, EXISTING_ENEMY_TYPE);
            GamesDAL.AddKilledEnemy(gameId, DIFFERENT_ENEMY_TYPE);

            DataTable killedEnemies = GamesDAL.GetEnemiesKilled(gameId);

            DataAssert.ColumnContains(killedEnemies, "enemyTypeId", EXISTING_ENEMY_TYPE);
            DataAssert.ColumnContains(killedEnemies, "enemyTypeId", DIFFERENT_ENEMY_TYPE);
        }

        [TestMethod]
        public void Killed_enemy_added_multiple_times_has_correct_count()
        {
            DataTable killedEnemies = GamesDAL.GetEnemiesKilled(gameId);
            DataRow[] matches = killedEnemies.Select("enemyTypeId=" + EXISTING_ENEMY_TYPE);
            int enemyCountBefore = matches.Length == 0 ? 0 : (int)matches[0]["enemyCount"];

            GamesDAL.AddKilledEnemy(gameId, EXISTING_ENEMY_TYPE);
            GamesDAL.AddKilledEnemy(gameId, EXISTING_ENEMY_TYPE);

            killedEnemies = GamesDAL.GetEnemiesKilled(gameId);

            int enemyCount = (int)killedEnemies.Select("enemyTypeId=" + EXISTING_ENEMY_TYPE)[0]["enemyCount"];
            Assert.AreEqual(enemyCountBefore + 2, enemyCount);
        }

        [ClassCleanup]
        public static void DeleteTestGame()
        {
            OleDbHelper.Delete(GamesDAL.ENEMIES_TBL, "gameId", gameId);
            OleDbHelper.Delete(GamesDAL.TBL_NAME, "gameId", gameId);
        }
    }
}
