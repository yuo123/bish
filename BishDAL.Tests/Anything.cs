﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BishDAL.Tests
{
#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()

    /// <summary>
    /// Provides an object that compares equal with any other object, for testing
    /// </summary>
    class Anything
    {
        /// <summary>
        /// Provides the single instance of the <see cref="Anything"/> class
        /// </summary>
        public static readonly Anything Instance = new Anything();
        private Anything() { }
        public override bool Equals(object obj) { return true; }
    }

#pragma warning restore CS0659 
}
