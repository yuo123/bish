﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BishDAL.Tests
{
    static class DataAssert
    {
        public static void IsDBNull(object value)
        {
            Assert.AreEqual(DBNull.Value, value, "Value was not DBNull");
        }

        public static void ColumnContains(DataTable dt, string col, object value)
        {
            Assert.IsTrue(
                ColumnContainsInternal(dt, col, value),
                "Column {0} does not contain value {1}", col, value);
        }

        public static void ColumnDoesNotContain(DataTable dt, string col, object value)
        {
            Assert.IsFalse(
                ColumnContainsInternal(dt, col, value),
                "Column {0} does contain value {1}", col, value);
        }

        private static bool ColumnContainsInternal(DataTable dt, string col, object value)
        {
            DataColumn dc = dt.Columns[col];
            foreach (DataRow dr in dt.Rows)
                if (dr[dc].Equals(value))
                    return true;

            return false;
        }
    }
}
