﻿using System;
using System.Globalization;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BishDAL.Tests
{
    static class DateAssert
    {
        public static void AreWithin(DateTime date1, DateTime date2, TimeSpan range)
        {
            Assert.IsTrue((date2 - date1).Duration() <= range, 
                string.Format(CultureInfo.InvariantCulture, 
                "Dates are not within expected range. First: {0} Second: {1} Range: {2}",
                date1, date2, range));
        }

        public static void AreWithin(DateTime date1, DateTime date2, double daysRange)
        {
            Assert.IsTrue((date2 - date1).Duration().TotalDays <= daysRange,
                string.Format(CultureInfo.InvariantCulture,
                "Dates are not within expected range. First: {0} Second: {1} Range: {2} days",
                date1, date2, daysRange));
        }

        /// <summary>
        /// Asserts that the given DateTime is within a day of DateTime.Now
        /// </summary>
        public static void IsNewlyCreated(DateTime date)
        {
            AreWithin(DateTime.Now, date, 1);
        }

        /// <summary>
        /// Asserts that the given DateTime is within a day of DateTime.Now
        /// </summary>
        public static void IsNewlyCreated(object date)
        {
            Assert.IsInstanceOfType(date, typeof(DateTime), "given object is not a DateTime");
            IsNewlyCreated((DateTime)date);
        }
    }
}
