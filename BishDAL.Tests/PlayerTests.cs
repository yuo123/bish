﻿using System;
using System.Data;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BishDAL.Tests
{
    [TestClass]
    public class PlayerTests
    {
        private const string USERNAME = "PlayerTests User";
        private const string EMAIL = "TEST@TEST.COM";
        private const string COUNTRY = "TEST COUNTRY";
        private static readonly DateTime DOB = new DateTime(2000, 1, 2);

        private const int NONEXISTENT_ROOM = int.MaxValue - 1;

        [ClassInitialize]
        public static void CreateTestPlayer(TestContext c)
        {
            try
            {
                PlayersDAL.CreatePlayer(USERNAME, "test password", EMAIL, COUNTRY, DOB);
            }
            catch (DuplicateUsernameException)
            {
                Assert.Inconclusive("The test username '{0}' already exists. Please delete it and re-run the test.", USERNAME);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(DuplicateUsernameException), AllowDerivedTypes = false)]
        public void Creating_duplicate_player_throws()
        {
            PlayersDAL.CreatePlayer(USERNAME, "", EMAIL, COUNTRY, DOB);
        }

        [TestMethod]
        public void New_player_has_correct_initial_values()
        {
            PlayersDAL.SetStartingStats(USERNAME);
            object[] vals =
            {
                USERNAME,
                PlayersDAL.STARTING_LEVEL,
                DBNull.Value, //roomId
                Anything.Instance, Anything.Instance, //x and y, not tested here
                PlayersDAL.STARTING_HEALTH,
                PlayersDAL.STARTING_ATTACK,
                PlayersDAL.STARTING_DEFENSE,
                PlayersDAL.STARTING_XP,
                false, //isBanned
                EMAIL,
                COUNTRY,
                DOB
            };

            DataRow playerData = PlayersDAL.GetPlayerDetails(USERNAME);

            CollectionAssert.AreEqual(vals, playerData.ItemArray);
        }

        [TestMethod]
        public void Player_can_be_moved_between_rooms()
        {
            int roomId = RoomsDAL.CreateRoom(5, 6, 4);
            PlayersDAL.ChangePlayerRoom(USERNAME, roomId, 1, 2);

            DataRow playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            Assert.AreEqual(roomId, playerData["roomId"]);
            Assert.AreEqual(1, playerData["xPos"]);
            Assert.AreEqual(2, playerData["yPos"]);

            int secondRoomId = RoomsDAL.CreateRoom(5, 6, 7);
            PlayersDAL.ChangePlayerRoom(USERNAME, secondRoomId, 3, 4);

            playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            Assert.AreEqual(secondRoomId, playerData["roomId"]);
            Assert.AreEqual(3, playerData["xPos"]);
            Assert.AreEqual(4, playerData["yPos"]);

            //cleanup
            PlayersDAL.RemovePlayerFromRoom(USERNAME);
            RoomsDAL.DeleteRoom(roomId);
            RoomsDAL.DeleteRoom(secondRoomId);
        }

        [TestMethod]
        [ExpectedException(typeof(NonexistentRoomException), AllowDerivedTypes = false)]
        public void Moving_player_to_nonexistent_room_throws()
        {
            if (RoomsDAL.DoesRoomExist(NONEXISTENT_ROOM))
                Assert.Inconclusive("Cannot test with nonexistent room when it exists. Delete room {0} and try again", NONEXISTENT_ROOM);

            PlayersDAL.ChangePlayerRoom(USERNAME, NONEXISTENT_ROOM, 1, 2);
        }

        [TestMethod]
        public void Moving_player_leaves_them_in_expected_coordinates()
        {
            PlayersDAL.MovePlayer(USERNAME, 4, 3);

            DataRow playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            Assert.AreEqual(4, playerData["xPos"]);
            Assert.AreEqual(3, playerData["yPos"]);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(-1)]
        [DataRow(0)]
        [DataRow(9)]
        [DataRow(short.MaxValue)]
        public void Changed_health_has_expected_value(int amount)
        {
            DataRow playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            int before = (int)playerData["health"];

            PlayersDAL.ChangeHealth(USERNAME, amount);

            playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            Assert.AreEqual(before + amount, (int)playerData["health"]);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(-1)]
        [DataRow(0)]
        [DataRow(9)]
        [DataRow(short.MaxValue)]
        public void Changed_attack_has_expected_value(int amount)
        {
            DataRow playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            int before = (int)playerData["attack"];

            PlayersDAL.ChangeAttack(USERNAME, amount);

            playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            Assert.AreEqual(before + amount, (int)playerData["attack"]);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(-1)]
        [DataRow(0)]
        [DataRow(9)]
        [DataRow(short.MaxValue)]
        public void Changed_defense_has_expected_value(int amount)
        {
            DataRow playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            int before = (int)playerData["defense"];

            PlayersDAL.ChangeDefense(USERNAME, amount);

            playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            Assert.AreEqual(before + amount, (int)playerData["defense"]);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(-1)]
        [DataRow(0)]
        [DataRow(9)]
        [DataRow(short.MaxValue)]
        public void Changed_xp_has_expected_value(int amount)
        {
            DataRow playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            int before = (int)playerData["xp"];

            PlayersDAL.ChangeXp(USERNAME, amount);

            playerData = PlayersDAL.GetPlayerDetails(USERNAME);
            Assert.AreEqual(before + amount, (int)playerData["xp"]);
        }

        [DataTestMethod]
        [DataRow(10, 11, 12)]
        [DataRow(9, 8, 0)]
        public void LevelUp_changes_level_attack_defense_and_health(int attack, int defense, int health)
        {
            int levelBefore = (int)PlayersDAL.GetPlayerDetails(USERNAME)["level"];

            PlayersDAL.LevelUp(USERNAME, attack, defense, health);

            DataRow playerDetails = PlayersDAL.GetPlayerDetails(USERNAME);
            Assert.AreEqual(levelBefore + 1, playerDetails["level"]);
            Assert.AreEqual(attack, playerDetails["attack"]);
            Assert.AreEqual(defense, playerDetails["defense"]);
            Assert.AreEqual(health, playerDetails["health"]);
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(true)]
        [DataRow(false)]
        [DataRow(false)]
        [DataRow(true)]
        public void SetBanStatus_reflected_in_IsBanned(bool banned)
        {
            PlayersDAL.SetBanStatus(USERNAME, banned);
            Assert.AreEqual(banned, PlayersDAL.IsBanned(USERNAME));
        }

        [ClassCleanup]
        public static void DeleteTestPlayer()
        {
            OleDbHelper.Delete(PlayersDAL.TBL_NAME, "username", USERNAME);
        }
    }
}
